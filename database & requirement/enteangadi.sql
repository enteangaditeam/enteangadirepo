-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Nov 02, 2016 at 01:05 AM
-- Server version: 5.5.49-cll-lve
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `enteangadi`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ad`
--

CREATE TABLE IF NOT EXISTS `tbl_ad` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `image_url2` text NOT NULL,
  `description` text NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`ID`, `username`, `password`, `type`) VALUES
(1, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_association`
--

CREATE TABLE IF NOT EXISTS `tbl_association` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `photo_url` text NOT NULL,
  `concern_person_name` text NOT NULL,
  `contact_number` text NOT NULL,
  `policy` text NOT NULL,
  `description` text NOT NULL,
  `association_type_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`ID`),
  KEY `association_type_id` (`association_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_association_location`
--

CREATE TABLE IF NOT EXISTS `tbl_association_location` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `association_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `association_id` (`association_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_association_type`
--

CREATE TABLE IF NOT EXISTS `tbl_association_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `association_type` text NOT NULL,
  `img` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blood_bank`
--

CREATE TABLE IF NOT EXISTS `tbl_blood_bank` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `place` varchar(500) NOT NULL,
  `blood_group` varchar(20) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bus_time`
--

CREATE TABLE IF NOT EXISTS `tbl_bus_time` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `bus_name` varchar(100) NOT NULL,
  `bus_time` varchar(20) NOT NULL,
  `location_from` varchar(100) NOT NULL,
  `location_to` varchar(100) NOT NULL,
  `location_via` varchar(500) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comments`
--

CREATE TABLE IF NOT EXISTS `tbl_comments` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `comments` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_comments`
--

INSERT INTO `tbl_comments` (`ID`, `user_id`, `comments`) VALUES
(1, 11, 'tt');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_doctor`
--

CREATE TABLE IF NOT EXISTS `tbl_doctor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hospital_id` int(11) NOT NULL,
  `doctor_name` text NOT NULL,
  `department` int(11) NOT NULL,
  `booking_number` text NOT NULL,
  `worked_before` varchar(500) NOT NULL,
  `sun` varchar(100) NOT NULL,
  `mon` varchar(100) NOT NULL,
  `tue` varchar(100) NOT NULL,
  `wed` varchar(100) NOT NULL,
  `thur` varchar(100) NOT NULL,
  `fri` varchar(100) NOT NULL,
  `sat` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_educational_institution`
--

CREATE TABLE IF NOT EXISTS `tbl_educational_institution` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `institution_type_id` int(11) NOT NULL,
  `institution_name` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_educational_institution_category`
--

CREATE TABLE IF NOT EXISTS `tbl_educational_institution_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `category` text NOT NULL,
  `img` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_educational_institution_course`
--

CREATE TABLE IF NOT EXISTS `tbl_educational_institution_course` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `educational_institution_id` int(11) NOT NULL,
  `txt_course_name` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_educational_institution_image`
--

CREATE TABLE IF NOT EXISTS `tbl_educational_institution_image` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `institution_id` int(11) NOT NULL,
  `image_url` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_educational_institution_location`
--

CREATE TABLE IF NOT EXISTS `tbl_educational_institution_location` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `institute_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `institute_id` (`institute_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_emergency`
--

CREATE TABLE IF NOT EXISTS `tbl_emergency` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `emergency_name` varchar(500) NOT NULL,
  `contact_number` varchar(100) NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` varchar(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_emergency_category`
--

CREATE TABLE IF NOT EXISTS `tbl_emergency_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) NOT NULL,
  `img` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_event`
--

CREATE TABLE IF NOT EXISTS `tbl_event` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(500) NOT NULL,
  `image_url` text NOT NULL,
  `description` text NOT NULL,
  `venu` text NOT NULL,
  `time_and_date` datetime NOT NULL,
  `guest` text NOT NULL,
  `organizer` varchar(100) NOT NULL,
  `sponsor` varchar(100) NOT NULL,
  `coordinator` varchar(100) NOT NULL,
  `contact_no` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `isViewable` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_event`
--

INSERT INTO `tbl_event` (`ID`, `user_id`, `name`, `image_url`, `description`, `venu`, `time_and_date`, `guest`, `organizer`, `sponsor`, `coordinator`, `contact_no`, `status`, `isViewable`) VALUES
(1, NULL, 'Thaikkudam Bridge', 'Images/events/20161022101629.jpg', 'The band literally was born near Thaikkudam bridge. Thaikkudam is a place in Cochin, Kerala. All the hunky, funky, lean, mean, fat, brat members of the band got themselves jam packed in a room near there and kick started the journey!\r\n\r\nIn spite of the diverse cultural backgrounds, the members hit it off really well and that''s the real reason we do what we do!!!\r\n\r\nThis is Just the Beginning!', 'Calicut', '2016-10-23 13:30:00', 'Sindhuja', 'Bodhi', 'Binfas', 'Rashid', '0495-2563214', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_event_location`
--

CREATE TABLE IF NOT EXISTS `tbl_event_location` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_event_location`
--

INSERT INTO `tbl_event_location` (`ID`, `event_id`, `location_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_government_office`
--

CREATE TABLE IF NOT EXISTS `tbl_government_office` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `office_name` varchar(500) NOT NULL,
  `working_hours` varchar(20) NOT NULL,
  `contact_number` varchar(50) NOT NULL,
  `services` text NOT NULL,
  `location_id` int(11) NOT NULL,
  `address` text NOT NULL,
  `status` varchar(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hospital`
--

CREATE TABLE IF NOT EXISTS `tbl_hospital` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hospital_name` varchar(500) NOT NULL,
  `address` text NOT NULL,
  `contact_number` varchar(100) NOT NULL,
  `image_url` text NOT NULL,
  `status` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`ID`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hospital_category`
--

CREATE TABLE IF NOT EXISTS `tbl_hospital_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `category` text NOT NULL,
  `img` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hospital_department`
--

CREATE TABLE IF NOT EXISTS `tbl_hospital_department` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `department` text NOT NULL,
  `hospital_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `hospital_id` (`hospital_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hotspot`
--

CREATE TABLE IF NOT EXISTS `tbl_hotspot` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotspot_name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `place` varchar(500) NOT NULL,
  `image_url` text NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `isViewable` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_hotspot`
--

INSERT INTO `tbl_hotspot` (`ID`, `hotspot_name`, `description`, `place`, `image_url`, `user_id`, `status`, `isViewable`) VALUES
(1, 'Kalad', '0495 272 6161', 'Waynad', '', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hotspot_image`
--

CREATE TABLE IF NOT EXISTS `tbl_hotspot_image` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotspot_id` int(11) NOT NULL,
  `image_url` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_hotspot_image`
--

INSERT INTO `tbl_hotspot_image` (`ID`, `hotspot_id`, `image_url`) VALUES
(1, 1, 'Images/hotspot/20161022110612.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job`
--

CREATE TABLE IF NOT EXISTS `tbl_job` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(500) NOT NULL,
  `job` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `contact_number` varchar(50) NOT NULL,
  `email` varchar(500) NOT NULL,
  `date` date NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `image_url` text NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `isViewable` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_job`
--

INSERT INTO `tbl_job` (`ID`, `company`, `job`, `description`, `contact_number`, `email`, `date`, `category_id`, `status`, `image_url`, `user_id`, `isViewable`) VALUES
(1, 'Bodhi info solutions', '.Net Developer', 'Development and maintenance of applications aimed at a range of iOS devices including mobile phones and tablet.\r\nDevelopment of iOS applications and their integration with back-end services.\r\nDesign and build applications for the iOS platform.\r\nEnsure the performance, quality, and responsiveness of applications.\r\nCollaborate with a team to define, design, and ship new features.\r\nIdentify and correct bottlenecks and fix bugs. Help maintain code quality, organization, and automatization.', '25012365', 'Bodhiinfo@gmail.com', '2016-10-22', 1, 1, 'Images/job/20161022102010.jpg', NULL, 1),
(2, 'TCS', 'Associate consultant', 'Associate consultants (ACs) usually have advanced degrees in business or engineering and contribute to a varied mix of intellectually challenging projects. With experience, an AC has the potential to develop into a fully autonomous consultant on our staff.', '2545854588', 'tcs@tcs.com', '2016-10-22', 1, 1, 'Images/job/20161022103957.jpg', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_category`
--

CREATE TABLE IF NOT EXISTS `tbl_job_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) NOT NULL,
  `img` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_job_category`
--

INSERT INTO `tbl_job_category` (`ID`, `category`, `img`) VALUES
(1, 'IT', 'Images/job_cat/20161022101758.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_location`
--

CREATE TABLE IF NOT EXISTS `tbl_job_location` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `job_id` (`job_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_job_location`
--

INSERT INTO `tbl_job_location` (`ID`, `job_id`, `location_id`) VALUES
(2, 2, 1),
(4, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_location`
--

CREATE TABLE IF NOT EXISTS `tbl_location` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_location`
--

INSERT INTO `tbl_location` (`ID`, `location`) VALUES
(1, 'General');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE IF NOT EXISTS `tbl_news` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `heading` text CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `date` date NOT NULL,
  `photo_url` text NOT NULL,
  `breaking` tinyint(1) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_news`
--

INSERT INTO `tbl_news` (`ID`, `user_id`, `heading`, `content`, `date`, `photo_url`, `breaking`, `status`) VALUES
(1, NULL, 'à´«àµ‹à´£àµà´‚ à´®àµ†à´¯à´¿à´²àµà´‚ à´šàµ‹à´°àµâ€à´¤àµà´¤à´¿à´¯à´¤à´¾à´¯à´¿ à´œàµ‡à´•àµà´•à´¬àµ à´¤àµ‹à´®à´¸àµ..', 'à´•àµŠà´šàµà´šà´¿: à´¤à´¨àµà´±àµ† à´”à´¦àµà´¯àµ‡à´¾à´—à´¿à´• à´«àµ‹à´£àµà´‚ à´‡ à´®àµ†à´¯à´¿à´²àµà´‚ à´šàµ‹à´°àµâ€à´¤àµà´¤à´¿à´¯à´¤à´¾à´¯à´¿ à´†à´°àµ‹à´ªà´¿à´šàµà´šàµ à´µà´¿à´œà´¿à´²à´¨àµâ€à´¸àµ à´¡à´¯à´±à´•àµà´Ÿà´°àµâ€ à´œàµ‡à´•àµà´•à´¬àµ à´¤àµ‹à´®à´¸àµ à´¡à´¿.à´œà´¿.à´ªà´¿. à´²àµ‹à´•àµâ€Œà´¨à´¾à´¥àµ à´¬àµ†à´¹à´±à´¯àµà´•àµà´•àµ à´ªà´°à´¾à´¤à´¿ à´¨à´²àµâ€à´•à´¿. à´ªàµà´°à´¤àµ...à´¦àµ‚à´¤à´¨àµâ€ à´¨à´´à´¿ à´‡à´¨àµà´¨à´²àµ† à´°à´¾à´¤àµà´°à´¿à´¯à´¾à´£àµ à´ªà´°à´¾à´¤à´¿ à´¨à´²àµâ€à´•à´¿à´¯à´¤àµ. à´‡ à´®àµ†à´¯à´¿à´²àµâ€ à´¹à´¾à´•àµà´•àµ à´šàµ†à´¯àµà´¤à´¤à´¾à´¯à´¿ à´¸à´‚à´¶à´¯à´¿à´•àµà´•àµà´¨àµà´¨à´¤à´¾à´¯àµà´‚ à´®àµŠà´¬àµˆà´²àµâ€ à´«àµ‹à´£àµâ€ à´…à´Ÿà´•àµà´•à´®àµà´³àµà´³à´µ......\r\nà´ªà´°à´¾à´¤à´¿ à´¤à´¨à´¿à´•àµà´•àµ à´²à´­à´¿à´šàµà´šà´¿à´Ÿàµà´Ÿà´¿à´²àµà´²àµ†à´¨àµà´¨àµà´‚ à´•à´¿à´Ÿàµà´Ÿà´¿à´¯à´¾à´²àµà´Ÿà´¨àµ† à´¨à´Ÿà´ªà´Ÿà´¿ à´¸àµà´µàµ€à´•à´°à´¿à´•àµà´•àµà´®àµ†à´¨àµà´¨àµà´‚ à´¬àµ†à´¹àµâ€Œà´± à´…à´±à´¿à´¯à´¿à´šàµà´šàµ.......', '2016-10-22', 'Images/news/20161022101317.jpg', 1, 1),
(2, 13, '', '', '2016-10-27', 'Images/news/20161027105554.jpg', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news_location`
--

CREATE TABLE IF NOT EXISTS `tbl_news_location` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `news_id` (`news_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_news_location`
--

INSERT INTO `tbl_news_location` (`ID`, `news_id`, `location_id`) VALUES
(2, 1, 1),
(3, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_offer`
--

CREATE TABLE IF NOT EXISTS `tbl_offer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `image_url` text NOT NULL,
  `status` int(11) NOT NULL,
  `isViewable` tinyint(1) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_panikal`
--

CREATE TABLE IF NOT EXISTS `tbl_panikal` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `address` text NOT NULL,
  `contact_no` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cDate` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_panikkar`
--

CREATE TABLE IF NOT EXISTS `tbl_panikkar` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `panikkar_name` varchar(500) NOT NULL,
  `working_time` varchar(20) NOT NULL,
  `contact_number` varchar(100) NOT NULL,
  `panikkar_type_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `panikkar_type_id` (`panikkar_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_panikkar_location`
--

CREATE TABLE IF NOT EXISTS `tbl_panikkar_location` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `panikar_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `panikar_id` (`panikar_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_panikkar_type`
--

CREATE TABLE IF NOT EXISTS `tbl_panikkar_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `panikkar_type` text NOT NULL,
  `img` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_panikkar_type`
--

INSERT INTO `tbl_panikkar_type` (`ID`, `panikkar_type`, `img`) VALUES
(1, 'aa', 'Images/panikar_cat/20161021171149.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quiz_option`
--

CREATE TABLE IF NOT EXISTS `tbl_quiz_option` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `options` varchar(500) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `question_id` (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quiz_question`
--

CREATE TABLE IF NOT EXISTS `tbl_quiz_question` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_name` varchar(500) NOT NULL,
  `question` varchar(500) NOT NULL,
  `exp_date` date NOT NULL,
  `pub_date` date NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quiz_question_answer`
--

CREATE TABLE IF NOT EXISTS `tbl_quiz_question_answer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `question_id` (`question_id`),
  KEY `option_id` (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quiz_user_answer`
--

CREATE TABLE IF NOT EXISTS `tbl_quiz_user_answer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `question_id` (`question_id`),
  KEY `option_id` (`option_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sell`
--

CREATE TABLE IF NOT EXISTS `tbl_sell` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(500) NOT NULL,
  `category_id` int(11) NOT NULL,
  `image_url` text NOT NULL,
  `contact_number` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `price` float NOT NULL,
  `status` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `exp_date` date NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sell_category`
--

CREATE TABLE IF NOT EXISTS `tbl_sell_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(200) NOT NULL,
  `img` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sell_location`
--

CREATE TABLE IF NOT EXISTS `tbl_sell_location` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `sell_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `sell_id` (`sell_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shop`
--

CREATE TABLE IF NOT EXISTS `tbl_shop` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `shop_category_id` int(11) NOT NULL,
  `shop_name` varchar(500) NOT NULL,
  `working_hours` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `contact_number` varchar(100) NOT NULL,
  `contact_numbner_alternative` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `sub_Name` varchar(500) NOT NULL,
  `isViewable` tinyint(1) NOT NULL,
  `admin_rate` int(11) NOT NULL DEFAULT '-1',
  `priority` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`ID`),
  KEY `shop_category_id` (`shop_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_shop`
--

INSERT INTO `tbl_shop` (`ID`, `shop_category_id`, `shop_name`, `working_hours`, `address`, `contact_number`, `contact_numbner_alternative`, `status`, `sub_Name`, `isViewable`, `admin_rate`, `priority`) VALUES
(1, 1, 'Bhima', '10 am to 5 m', '5/3075 B & C, Ground Floor, Intercity Arcade, JafferKhan Colony Rd, Kozhikode, Kerala 673004', '0495 272 6161', '0495 272 6163', 1, 'Jewellery', 1, -1, 100);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shop_admin_rating`
--

CREATE TABLE IF NOT EXISTS `tbl_shop_admin_rating` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `rating` float NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shop_category`
--

CREATE TABLE IF NOT EXISTS `tbl_shop_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `category` text NOT NULL,
  `img` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_shop_category`
--

INSERT INTO `tbl_shop_category` (`ID`, `category`, `img`) VALUES
(1, 'Jewellery', 'Images/Shop_cat/20161022105001.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shop_image`
--

CREATE TABLE IF NOT EXISTS `tbl_shop_image` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `image_url` text NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `shop_id` (`shop_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_shop_image`
--

INSERT INTO `tbl_shop_image` (`ID`, `shop_id`, `image_url`) VALUES
(1, 1, 'Images/shop/20161022110014.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shop_location`
--

CREATE TABLE IF NOT EXISTS `tbl_shop_location` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `shop_id` (`shop_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_shop_location`
--

INSERT INTO `tbl_shop_location` (`ID`, `shop_id`, `location_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shop_rating`
--

CREATE TABLE IF NOT EXISTS `tbl_shop_rating` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` float NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `shop_id` (`shop_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_shop_rating`
--

INSERT INTO `tbl_shop_rating` (`ID`, `shop_id`, `user_id`, `rating`) VALUES
(1, 1, 11, 4),
(2, 1, 13, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_taxi`
--

CREATE TABLE IF NOT EXISTS `tbl_taxi` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `driver_name` varchar(500) NOT NULL,
  `taxi_name` varchar(500) NOT NULL,
  `registration_number` varchar(100) NOT NULL,
  `license_number` varchar(50) NOT NULL,
  `contact_number` varchar(100) NOT NULL,
  `image_url` text NOT NULL,
  `badge_number` varchar(100) NOT NULL,
  `place` varchar(500) NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `priority` int(11) DEFAULT '100',
  PRIMARY KEY (`ID`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_taxi_category`
--

CREATE TABLE IF NOT EXISTS `tbl_taxi_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `category` text NOT NULL,
  `min_charge` float NOT NULL,
  `kilometer_charge` float NOT NULL,
  `img` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_todays_special`
--

CREATE TABLE IF NOT EXISTS `tbl_todays_special` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(500) NOT NULL,
  `posted_by` varchar(100) NOT NULL,
  `place` varchar(500) NOT NULL,
  `image_url` text NOT NULL,
  `description` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `posted_date` date NOT NULL,
  `isViewable` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_todays_special`
--

INSERT INTO `tbl_todays_special` (`ID`, `topic`, `posted_by`, `place`, `image_url`, `description`, `category_id`, `status`, `posted_date`, `isViewable`) VALUES
(1, 'Biriyani', 'Admin', 'calicut', 'Images/todayspecial/20161022111711.jpg', 'Biryani, also known as biriyani or biriani, is an Indian mixed rice dish with its origins among the Muslims of the Indian subcontinent. It is popular throughout the subcontinent and among the diaspora from the region.', 1, 1, '2016-10-22', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_todays_special_category`
--

CREATE TABLE IF NOT EXISTS `tbl_todays_special_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `category` text NOT NULL,
  `img` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_todays_special_category`
--

INSERT INTO `tbl_todays_special_category` (`ID`, `category`, `img`) VALUES
(1, 'Food', 'Images/today_special_cat/20161022111357.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `email` text NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `user_referal_id` text NOT NULL,
  `suggested_referal_id` text NOT NULL,
  `status` int(11) NOT NULL,
  `image_url` text NOT NULL,
  `auth_key` varchar(100) NOT NULL,
  `auth_key2` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`ID`, `first_name`, `last_name`, `mobile`, `email`, `username`, `password`, `user_referal_id`, `suggested_referal_id`, `status`, `image_url`, `auth_key`, `auth_key2`) VALUES
(9, 'hjss', 'gjns', '+9649995624354', 'bkaja@gsks.com', '', '', 'EN0009', '', 0, '', '97dabcf4679b33d2fc554317c9c8a296', '9a724c2e570dec5d2787b02eb4f2ae0a'),
(10, 'sfbdge', 'dggssh', '+35356596688', 'sgsgshw@csbdj.com', '', '', 'EN0010', '', 0, '', '11231d165304d231a221fbc245beb86e', '75c9c985337258cd2ece5e77cb0d9221'),
(11, 'sindhuja', 'ck', '+918281938033', 'sindhu@gmail.com', '', '', 'EN0011', '', 0, 'Images/user/20161022104727.jpg', 'd83ab2041ddad3d78846aa87e7afd8f4', '1bebf1ef165ffef28c86269ca904d26f'),
(12, 'Sujina', 'E', '+919605616573', 'sujinabodhi@gmail.com', '', '', 'EN0012', '', 0, '', '5a60bbeee7381ffd318370d365ee5749', '3f7cd869687b8cc319ed35d5ba383468'),
(13, 'rameez', 'khan', '+919995646254', 'rameez@gmail.com', '', '', 'EN0013', '', 0, '', '77da10e7e41dd43b025d4acd1d9d5f49', 'd7cbe2607d00d4d46185dd1ae37d8759'),
(14, 'Vishnu', 'KR', '+919400976133', 'kr.vishnu401@gmail.com', '', '', 'EN0014', '', 0, 'Images/user/20161025204130.jpg', '3f7bb398bd7bcb3d3c5fb5e838307f9f', '5393fa4f378b09f930bf819404b1b6db'),
(15, 'sujina', 'E', '+919605616572', 'sujinabodhi@gmail.com', '', '', 'EN0015', '', 0, '', 'feb2b292f1bcfd8d043c3f1d0bc77c5a', 'ebdc5a173530157327d7f05e5779fd80'),
(16, 'maqbool', 'pp', '+62868666888', 'tudj@gus.com', '', '', 'EN0016', '', 0, '', 'ab8bce04bbe6e10e2473c68f80099aa1', '69d0c77f3a93cc048c6eea96a2d6bba4');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_association`
--
ALTER TABLE `tbl_association`
  ADD CONSTRAINT `tbl_association_ibfk_1` FOREIGN KEY (`association_type_id`) REFERENCES `tbl_association_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_educational_institution_location`
--
ALTER TABLE `tbl_educational_institution_location`
  ADD CONSTRAINT `tbl_educational_institution_location_ibfk_1` FOREIGN KEY (`institute_id`) REFERENCES `tbl_educational_institution` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_educational_institution_location_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `tbl_location` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_hospital`
--
ALTER TABLE `tbl_hospital`
  ADD CONSTRAINT `tbl_hospital_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `tbl_hospital_category` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_hospital_department`
--
ALTER TABLE `tbl_hospital_department`
  ADD CONSTRAINT `tbl_hospital_department_ibfk_1` FOREIGN KEY (`hospital_id`) REFERENCES `tbl_hospital` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_job`
--
ALTER TABLE `tbl_job`
  ADD CONSTRAINT `tbl_job_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `tbl_job_category` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_panikkar`
--
ALTER TABLE `tbl_panikkar`
  ADD CONSTRAINT `tbl_panikkar_ibfk_3` FOREIGN KEY (`panikkar_type_id`) REFERENCES `tbl_panikkar_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_panikkar_location`
--
ALTER TABLE `tbl_panikkar_location`
  ADD CONSTRAINT `tbl_panikkar_location_ibfk_1` FOREIGN KEY (`panikar_id`) REFERENCES `tbl_panikkar` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_panikkar_location_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `tbl_location` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_sell_location`
--
ALTER TABLE `tbl_sell_location`
  ADD CONSTRAINT `tbl_sell_location_ibfk_1` FOREIGN KEY (`sell_id`) REFERENCES `tbl_sell` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_sell_location_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `tbl_location` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_shop`
--
ALTER TABLE `tbl_shop`
  ADD CONSTRAINT `tbl_shop_ibfk_1` FOREIGN KEY (`shop_category_id`) REFERENCES `tbl_shop_category` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_shop_image`
--
ALTER TABLE `tbl_shop_image`
  ADD CONSTRAINT `tbl_shop_image_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `tbl_shop` (`ID`);

--
-- Constraints for table `tbl_shop_location`
--
ALTER TABLE `tbl_shop_location`
  ADD CONSTRAINT `tbl_shop_location_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `tbl_shop` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_shop_location_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `tbl_location` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_shop_rating`
--
ALTER TABLE `tbl_shop_rating`
  ADD CONSTRAINT `tbl_shop_rating_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `tbl_shop` (`ID`),
  ADD CONSTRAINT `tbl_shop_rating_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`ID`);

--
-- Constraints for table `tbl_todays_special`
--
ALTER TABLE `tbl_todays_special`
  ADD CONSTRAINT `tbl_todays_special_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `tbl_todays_special_category` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
