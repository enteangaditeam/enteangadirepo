<?php
	require("../../config/config.inc.php"); 
	require("../../config/Database.class.php");
	require("../../config/Application.class.php");

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();

	$tableId 	= 	$_REQUEST['hospitalId'];	
	$hosName 	=	$_REQUEST['hospital'];

?>
	<div role="tabpanel" class="tab-pane active" id="photo<?php echo $tableId; ?>">
		<form action="do.php?op=department" class="form1" method="post" onsubmit="return valid()" >
			<input type="hidden" name="hospitalId" id="hospitalId" value="<?php echo $tableId ?>">               								
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="shopName"><?= $hosName." "; ?></span></label>		                                  
					</div>
					<div class="form-group">		                      						
						<label for="emergency_name">Course Name<span class="star">*</span></label>
						<input type="text" id="course" name="course" class="form-control2" required>				
					  	<span id="user-result"></span>                    	                     
					</div>					
				</div> 	                  
			</div>              
			<div class="modal-footer">
				<input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
			</div>			          
		</form>
		
		<div class="tablearea table-responsive">
			<table class="table table_pop" id="headerTable2" >
			<thead style="background-color:#CCC;">
				<tr>
					<th>Sl No</th>
					<th>Course</th> 				                           
				</tr>
			</thead>
			<tbody >                             
				<?php							
				$selAllQuery3="select *																	 										
				from `".TABLE_EDUCATIONAL_INSTITUTION_COURSE."`
				where `".TABLE_EDUCATIONAL_INSTITUTION_COURSE."`.educational_institution_id='$tableId'																																			   
				order by `".TABLE_EDUCATIONAL_INSTITUTION_COURSE."`.ID ";
				//echo $selAllQuery;
				$selectAll3= $db->query($selAllQuery3);
				$num3=mysql_num_rows($selectAll3);							

				if($num3==0)
				{
					?>
					<tr>
						<td colspan="3" align="center">No data found</td>
					</tr>
					<?php
				}
				else
				{	
					$j=0;														
					while($row3=mysql_fetch_array($selectAll3))
					{
						$tabphotoId=$row3['ID']; 								
						?>
						<tr>
							<td><?php echo ++$j;?>									
								<div class="adno-dtls"><a href="do.php?deleteId=<?php echo $tabphotoId; ?>&op=delCource" class="delete" onclick="return delete_type();">DELETE</a> </div>
							</td> 
							<td>
								<?php echo $row3['txt_course_name']; ?>
							</td> 							                                                             
						</tr>
						<?php					
					}								
				}
				?>

			</tbody>

			</table>
		</div>
	</div>