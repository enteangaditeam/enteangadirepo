<?php
	require("../../config/config.inc.php"); 
	require("../../config/Database.class.php");
	require("../../config/Application.class.php");

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();

	$tableId 	= 	$_REQUEST['shopId'];
	$numPics	=	$_REQUEST['pics'];	
	$shopName 	=	$_REQUEST['shop'];

?>
	<div role="tabpanel" class="tab-pane active" id="photo<?php echo $tableId; ?>">
		<form action="do.php?op=photo" class="form1" method="post" onsubmit="return valid()" enctype="multipart/form-data">
			<input type="hidden" name="shopId" id="shopId" value="<?php echo $tableId ?>">               								
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						
						<label for="shopName"><?= $shopName; ?></span></label>		                                  
					</div>
					<div class="form-group">		                      
						<label for="photo">Photo<span class="valid">*</span> (500*329)</label>
						<input type="file" name="photo" id="photo"  required="">		                     
					</div>					
				</div> 	                  
			</div>              
			<div class="modal-footer">
				<input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
			</div>			          
		</form>
		
		<div class="tablearea table-responsive">
			<table class="table table_pop" id="headerTable2" >
			<thead style="background-color:#CCC;">
				<tr>
					<th>SlNo</th>
					<th>Photo</th> 				                           
				</tr>
			</thead>
			<tbody >                             
				<?php							
				$selAllQuery3="select *																	 										
				from `".TABLE_EDUCATIONAL_INSTITUTION_PHOTO."`
				where `".TABLE_EDUCATIONAL_INSTITUTION_PHOTO."`.institution_id='$tableId'																																			   
				order by `".TABLE_EDUCATIONAL_INSTITUTION_PHOTO."`.ID ";
				//echo $selAllQuery;
				$selectAll3= $db->query($selAllQuery3);
				$num3=mysql_num_rows($selectAll3);							

				if($num3==0)
				{
					?>
					<tr>
						<td colspan="3" align="center">No data found</td>
					</tr>
					<?php
				}
				else
				{	
					$j=0;														
					while($row3=mysql_fetch_array($selectAll3))
					{
						$tabphotoId=$row3['ID']; 								
						?>
						<tr>
							<td><?php echo ++$j;?>									
								<div class="adno-dtls"><a href="do.php?deleteId=<?php echo $tabphotoId; ?>&op=delPhoto" class="delete" onclick="return delete_type();">DELETE</a> </div>
							</td> 
							<td>
								<?php if($row3['image_url'])
								{?>
									<div class="view_photos" ><img style="width: 100%; max-width: 200px;" src="../../<?php echo $row3['image_url']; ?>" alt="No Image"  class="img-responsive" /></div>
									<?php
								}

								?>
							</td> 							                                                             
						</tr>
						<?php					
					}								
				}
				?>

			</tbody>

			</table>
		</div>
	</div>