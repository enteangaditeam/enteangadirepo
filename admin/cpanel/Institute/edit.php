<?php 
	include("../adminHeader.php"); 
	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>


<?php
	if(isset($_SESSION['msg']))
	{?>
		<font color="red"><?php echo $_SESSION['msg']; ?></font><?php 
	}	
	$_SESSION['msg']='';
	$editId=$_REQUEST['id'];
	$tableEdit="SELECT * FROM ".TABLE_EDUCATIONAL_INSTITUTION." WHERE ID='$editId'";	
	$editField=mysql_query($tableEdit);
	$editRow=mysql_fetch_array($editField);

		//Location query 
	$qry = "SELECT location_id FROM ".TABLE_EDUCATIONAL_INSTITUTION_LOCATION."   WHERE institute_id =$editId";
								
	$locResult = $db->query($qry);
	$sLocation = array();
	while($locFetch = mysql_fetch_array($locResult))
	{	 		
	 		array_push($sLocation,$locFetch['location_id']);
	}	
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="index.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">Institution </h4>
            </div>
            <div class="modal-body clearfix">
				<form method="post" action="do.php?op=edit" class="form1" enctype="multipart/form-data" onsubmit="return valid()">
							<input type="hidden" name="fid" id="fid" value="<?php echo $editId ?>">
			             
                <div class="row">
                 	<div class="col-sm-6">

											<div class="form-group">
                      <label for="category">Institution Type <span class="star">*</span></label>
                      <?php 
											?>
											<select name="category" id="group" class="form-control2" required >
											<?php
												$categoryTypes="select ID,category from ".TABLE_EDUCATIONAL_INSTITUTION_CATEGORY."";
												$res2=mysql_query($categoryTypes);													
												while($row=mysql_fetch_array($res2))
												{?>	
													<option value="<?php echo $row['ID']?>" <?php if($editRow['institution_type_id']== $row['ID']){?> selected="selected"<?php }?>><?php echo $row['category']?></option>
												<?php 									
												}?>	            																						
											</select>				
					  					<span id="user-result"></span>
                    </div>	
										<div class="form-group">
                      <label for="emergency_name">Institution Name<span class="star">*</span></label>
                      <input type="text" id="name" value="<?php echo $editRow['institution_name'];?>" placeholder="Name" title="Name" name="name" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
                      <label for="emergency_name">Description</label>
                      <textarea name="description" placeholder="Description" id="description" class="form-control2" ><?php echo $editRow['description'];?></textarea>				
					  					<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="contact_number">Contact Number<span class="star">*</span></label>
                      <input type="text" id="contact_number" value="<?php echo $editRow['phone'];?>" placeholder="Contact Number" title="Contact Number"  name="contact_number" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>
									  <div class="form-group">
                      <label for="Description">Address<span class="star"></span></label>
                      <textarea id="address" name="address" placeholder="Address" class="form-control2" placeholder="Description" ><?php echo $editRow['address'];?></textarea>				
					  					<span id="user-result"></span>
                    </div>	
										<div class="form-group">
                      <label for="Location">Location</label>
											<ul class="category_combo_list list-unstyled" style="display: block;">
												<?php 
														$selAllQuery="SELECT * FROM ".TABLE_LOCATION." ORDER BY location ASC";
														$selectAll= $db->query($selAllQuery);
														while($row=mysql_fetch_array($selectAll))
														{ ?>
															<li><input type="checkbox" name="locations[]" id="locations" value="<?= $row['ID'];?>" <?php if(in_array( $row['ID'],$sLocation)){ echo "checked" ;}?>>  
                        			<label><?= $row['location']; ?></label></li>
													<?php	}	
												?>
												</ul>					  																                     
                    </div>										

								</div>					 																								
            	</div>			
	 			<div>
            	</div>
            	<div class="modal-footer">
              		<input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            	</div>
				</form>
          	</div>
        </div>
      </div>
      <!-- Modal1 cls -->            
  </div>
<?php include("../adminFooter.php") ?>
