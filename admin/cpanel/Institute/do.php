<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_POST['name'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");
			}
		else
			{							
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;	
						
				$data['institution_type_id']	=	$App->convert($_REQUEST['category']);
				$data['institution_name']		=	$App->convert($_REQUEST['name']);
				$data['address']				=	$App->convert($_REQUEST['address']);
				$data['phone']					=	$App->convert($_REQUEST['contact_number']);
				$data['status']					= 1;
				$data['description']			= $App->convert($_REQUEST['description']); 				

				$success=$db->query_insert(TABLE_EDUCATIONAL_INSTITUTION, $data);
				$catArray 	=	$_REQUEST['locations'];				
				if(!empty($catArray)) 
				{
					for($i=0;$i<count($catArray);$i++)
					{
						$data1['institute_id']	=	$success;
						$data1['location_id'] 	=	$catArray[$i];						
						$db->query_insert(TABLE_EDUCATIONAL_INSTITUTION_LOCATION,$data1);
					}										
				}	
				$db->close();							
		
				if($success)
				{
					$_SESSION['msg']="Institute added Successfully";										
				}
				else
				{
					$_SESSION['msg']="Failed";							
				}	
				header("location:index.php");		
			}
		break;
		
	// EDIT SECTION
	//-
	case 'edit':
		
		$fid	=	$_REQUEST['fid'];	       
		if(!$_POST['name'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
					
								
				$data['institution_type_id']	=	$App->convert($_REQUEST['category']);
				$data['institution_name']		=	$App->convert($_REQUEST['name']);
				$data['address']				=	$App->convert($_REQUEST['address']);
				$data['phone']					=	$App->convert($_REQUEST['contact_number']);
				$data['description']			= 	$App->convert($_REQUEST['description']);
				$data['status']					= 1; 	

				$success=$db->query_update(TABLE_EDUCATIONAL_INSTITUTION, $data, "ID='{$fid}'");	

				$qry = "DELETE FROM ".TABLE_EDUCATIONAL_INSTITUTION_LOCATION." where institute_id=$fid";
				$resultQ = $db->query($qry);

				$catArray 	=	$_REQUEST['locations'];				
				if(!empty($catArray)) 
				{
					for($i=0;$i<count($catArray);$i++)
					{
						$data1['institute_id']	=	$fid;
						$data1['location_id'] 	=	$catArray[$i];						
						$db->query_insert(TABLE_EDUCATIONAL_INSTITUTION_LOCATION,$data1);
					}										
				}									 
				$db->close(); 				
				
				if($success)
				{
					$_SESSION['msg']="Institute updated successfully";																	
				}
				else
				{
					$_SESSION['msg']="Failed";									
				}	
				header("location:index.php");	
				
									
			}
		
		break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				try
				{
					$success= @mysql_query("DELETE FROM `".TABLE_EDUCATIONAL_INSTITUTION_LOCATION."` WHERE institute_id='{$id}'");		
					$success= @mysql_query("DELETE FROM `".TABLE_EDUCATIONAL_INSTITUTION_COURSE."` WHERE institute_id='{$id}'");
					$success= @mysql_query("DELETE FROM `".TABLE_EDUCATIONAL_INSTITUTION."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}				
				$db->close(); 
				
				
				if($success)
				{
					$_SESSION['msg']="Data deleted successfully";									
				}
				else
				{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
				}	
				header("location:index.php");	
		break;
	// Add cource
	case 'department':				
			    	
			if(!$_REQUEST['hospitalId'])
				{
					$_SESSION['msg']="Error, Invalid Details!";					
					header("location:index.php");	
				}
			else
				{				
					$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
					$db->connect();
					$success=0;
													
					$shopId		=	$App->convert($_REQUEST['hospitalId']);				
										
					$data['txt_course_name']			=	$App->convert($_REQUEST['course']);		
					$data['educational_institution_id']	=	$shopId;						
					
					$success1=$db->query_insert(TABLE_EDUCATIONAL_INSTITUTION_COURSE,$data);											
					$db->close();												
					if($success1)
					{							
						$_SESSION['msg']="Course Added Successfully";										
					}
					else
					{						
						$_SESSION['msg']="Failed";											
					}
					header("location:index.php");
				}
																					
			break;
// PHOTO UPLOAD
	case 'photo':				
			    	
			if(!$_REQUEST['shopId'])
				{
					$_SESSION['msg']="Error, Invalid Details!";					
					header("location:index.php");	
				}
			else
				{				
					$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
					$db->connect();
					$success=0;
													
					$shopId		=	$App->convert($_REQUEST['shopId']);				
					
					// upload file
					$date		= 	date("YmdHis");
					$path_info  = 	pathinfo($_FILES["photo"]["name"]);
					$ext	 	=	$path_info["extension"];	
					$allowed 	=  	array('bmp','jpg');		// allowed image extensions					
					$newName 	= 	$date.".".$ext;

					if(in_array($ext,$allowed) ) 	
					{
						$img='';
						if (!file_exists('../../Images/institute')) {
							mkdir('../../Images/institute', 0777, true);
						}	
						if(move_uploaded_file($_FILES["photo"]["tmp_name"],"../../Images/institute/".basename($newName)))
						{
							$img="Images/institute/" . $newName;		
						}
						$data['institution_id']			=	$shopId;		
						$data['image_url']			=	$App->convert($img);						
						
						$success1=$db->query_insert(TABLE_EDUCATIONAL_INSTITUTION_PHOTO,$data);											
						$db->close();								
					}
					
					if($success1)
					{							
						$_SESSION['msg']="Shop Photo Added Successfully";										
					}
					else
					{
						if(!in_array($ext,$allowed) ) 
						{
							$_SESSION['msg']="You have uploaded an invalid image file";
						}
						else
						{
							$_SESSION['msg']="Failed";	
						}						
					}
					header("location:index.php");
				}
																					
			break;
			// DELETE SHOP PHOTO
	case 'delPhoto':		
				$deleteId	=	$_REQUEST['deleteId'];
				$shopId		=	$_REQUEST['sid'];
				$success1	=	0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
								
				try
				{ 										
					$res=mysql_query("select image_url from ".TABLE_EDUCATIONAL_INSTITUTION_PHOTO." where ID='{$deleteId}'");
					$row=mysql_fetch_array($res);
					$photo="../../".$row['image_url'];	
					$success1= @mysql_query("DELETE FROM `".TABLE_EDUCATIONAL_INSTITUTION_PHOTO."` WHERE ID='{$deleteId}'");
					if($success1)					
					{																																	
						if (file_exists($photo)) 	
						{
							unlink($photo);					
						} 						
					}														     
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete this";				            
				}											
				$db->close(); 
				if($success1)
				{
					$_SESSION['msg']="Shop Photo deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete this";										
				}	
				header("location:index.php");					
		break;					
// DELETE SHOP PHOTO
	case 'delCource':		
				$deleteId	=	$_REQUEST['deleteId'];
				//$shopId		=	$_REQUEST['sid'];
				$success1	=	0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
								
				try
				{ 										
					$success1= @mysql_query("DELETE FROM `".TABLE_EDUCATIONAL_INSTITUTION_COURSE."` WHERE ID='{$deleteId}'");																	     
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete this";				            
				}											
				$db->close(); 
				if($success1)
				{
					$_SESSION['msg']="Course deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete this";										
				}	
				header("location:index.php");					
		break;	

					// Priority
	case 'priority':		
		$editId	=	$_REQUEST['shopId']; 
			    	
		if(!$_REQUEST['shopId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();

				$data['priority']	=   $App->convert($_REQUEST['priority']);
				
				$success1=$db->query_update(TABLE_EDUCATIONAL_INSTITUTION,$data," ID='{$editId}'");

				$db->close();
					
				if($success1)
				{							
					$_SESSION['msg']="Institute Priority Updated Successfully";										
				}
				else{
					$_SESSION['msg']="Failed";
				}
				
				header("location:index.php");																
			}	
			break;								
}
?>