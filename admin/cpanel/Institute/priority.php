<?php
	require("../../config/config.inc.php"); 
	require("../../config/Database.class.php");
	require("../../config/Application.class.php");

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();

	$tableId 	= 	$_REQUEST['shopId'];
	$priority	=	$_REQUEST['priority'];	
	$shopName 	=	$_REQUEST['shop'];

?>
	<div role="tabpanel" class="tab-pane active" id="photo<?php echo $tableId; ?>">
		<form action="do.php?op=priority" class="form1" method="post" onsubmit="return valid()" enctype="multipart/form-data">
			<input type="hidden" name="shopId" id="shopId" value="<?php echo $tableId ?>">               								
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="shopName"><?= $shopName; ?></span></label>		                                  
					</div>
					<div class="form-group">		                      
						<label for="photo">Priority<span class="valid">*</span></label>
						<input type="number" min="1" step="1" name="priority"  id="priority" class="form-control2" required value="<?=$priority?>" >		                     
					</div>					
				</div> 	                  
			</div>              
			<div class="modal-footer">
				<input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
			</div>			          
		</form>
				
	</div>