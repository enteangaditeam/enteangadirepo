<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_POST['category'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");
			}
		else
			{							
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;		
				$data['category_id']	    =	$App->convert($_REQUEST['category']);
				$data['emergency_name']	=	$App->convert($_REQUEST['emergency_name']);
				$data['contact_number']	=	$App->convert($_REQUEST['contact_number']);
				
				$data['status']	= "Available"; //$App->convert($_REQUEST['status']);
				
				$ifExist = 0;
				$ifExist = $db->existValuesId(TABLE_EMERGENCY, "emergency_name = '".$data['emergency_name']."' AND contact_number = '".$data['contact_number']."' AND category_id = ".$data['category_id']);	
				//echo $ifExist;
				if ($ifExist > 0) {
					$_SESSION['msg']="The details are already added before.";
				} else {
					$success=$db->query_insert(TABLE_EMERGENCY, $data);	
					if($success)
					{
						$_SESSION['msg']="Category added Successfully";		
					}
					else
					{
						$_SESSION['msg']="Failed";							
					}
				}	
				$db->close();			
				header("location:index.php");		
			}
		break;
		
	// EDIT SECTION
	//-
	case 'edit':
		
		$fid	=	$_REQUEST['fid'];	       
		if(!$_POST['category'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
					
								
				$data['category_id']	    =	$App->convert($_REQUEST['category']);
				$data['emergency_name']	=	$App->convert($_REQUEST['emergency_name']);
				$data['contact_number']	=	$App->convert($_REQUEST['contact_number']);
				$data['status']	= "Available"; //$App->convert($_REQUEST['status']);
					

				$success=$db->query_update(TABLE_EMERGENCY, $data, "ID='{$fid}'");					 
				$db->close(); 				
				
				if($success)
				{
					$_SESSION['msg']="Data updated successfully";																	
				}
				else
				{
					$_SESSION['msg']="Failed";									
				}	
				header("location:index.php");	
				
									
			}
		
		break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				try
				{			
					$success= @mysql_query("DELETE FROM `".TABLE_EMERGENCY."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}				
				$db->close(); 
				
				
				if($success)
				{
					$_SESSION['msg']="Data deleted successfully";									
				}
				else
				{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
				}	
				header("location:index.php");	
		break;	
}
?>