<?php 
	include("../adminHeader.php"); 
	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>


<?php
	if(isset($_SESSION['msg']))
	{?>
		<font color="red"><?php echo $_SESSION['msg']; ?></font><?php 
	}	
	$_SESSION['msg']='';
	$editId=$_REQUEST['id'];
	$tableEdit="SELECT * FROM ".TABLE_TODAYS_SPECIAL." WHERE ID='$editId'";
	$editField=mysql_query($tableEdit);
	$editRow=mysql_fetch_array($editField);
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="index.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">Today's Special </h4>
            </div>
            <div class="modal-body clearfix">
				<form method="post" action="do.php?op=edit" class="form1" enctype="multipart/form-data" onsubmit="return valid()">
				<input type="hidden" name="fid" id="fid" value="<?php echo $editId ?>">
			             
                <div class="row">
                 	<div class="col-sm-6">
                    	<div class="form-group">
                      		<label for="group">Category <span class="star">*</span></label>
                      		<select name="group" id="group" class="form-control2" required >
													<?php	
													$categoryTypes="select ID,category from ".TABLE_TODAYS_SPECIAL_CATEGORY."";
													$res2=mysql_query($categoryTypes);	
											
													while($row=mysql_fetch_array($res2))
													{?>	
														<option value="<?php echo $row['ID']?>" <?php if($editRow['category_id']== $row['ID']){?> selected="selected"<?php }?>><?php echo $row['category']?></option>
													<?php 									
													}?>										
													</select>				
					  							<span id="user-result"></span>
                    	</div>

																<div class="form-group">
                      <label for="driver_name">Posted By</label>
                      <input type="text" id="posted_by" value="<?php echo $editRow['posted_by'];?>" placeholder="Posted By" name="posted_by" class="form-control2">				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
                      <label for="name">Posted Date <span class="star">*</span></label>
                      <input type="text" id="newsDate" name="posted_date" require value="<?php echo $App->dbformat_date_db( $editRow['posted_date']);?>" class="form-control2 datepicker" required readonly>
						<span id="user-result"></span>
                    </div>
										<div class="form-group">
                      <label for="name">Topic<span class="star">*</span></label>
                      <input type="text" id="topic" value="<?php echo $editRow['topic'];?>" name="topic" placeholder="Topic" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
                      <label for="name">Description</label>
                      <textarea placeholder="Description" class="form-control2" name="description" id="description" cols="30" rows="10"><?php echo $editRow['description'];?></textarea>
						<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="place">Place</label>
                      <input type="place" id="Place" value="<?php echo $editRow['place'];?>"  placeholder="Place" name="place" class="form-control2">				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
                                <input type="checkbox" <?php if($editRow['isViewable']){ echo "checked" ;}?> name="isViewable" id="isViewable">
                                <label for="contact_no">Show in Home</label>
                            </div>
										<div class="form-group">
                      <label for="image_url">Image</label>
                      <input type="file" id="image_url" name="image_url" class="form-control2">				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
											<?php 
											if($editRow['image_url'])
											{
												$path="../../".$editRow['image_url']; ?>
												<div style="max-width: 100px;"><img id="thumb" src="<?php echo $path ?>" alt="No Image" class="img-responsive" /></div>
											<?php }else{?>
											<div style="max-width: 100px;"><img src="../../img/student.png" alt="No Image" class="img-responsive" /></div>
											<?php } ?>
										</div>

					</div>					 																								
            	</div>			
	 			<div>
            	</div>
            	<div class="modal-footer">
              		<input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            	</div>
				</form>
          	</div>
        </div>
      </div>
      <!-- Modal1 cls -->            
  </div>
<?php include("../adminFooter.php") ?>
