<?php 
	include("../adminHeader.php");
	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}
	$type		=	$_SESSION['LogType'];

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>
<script>
	function delete_type()
	{
		var del=confirm("Do you Want to Delete ?");
		if(del==true)
		{
			window.submit();
		}
		else
		{
			return false;
		}
	}
</script>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-4"> 
          		<div class="clearfix">
					<h2 class="q-title">Today's Special</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD NEW</a> 
				</div>
          </div>
          <div class="col-sm-8">
            <form method="post">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="form-control"  name="topic" placeholder="Topic" value="<?php echo @$_REQUEST['posted_by'] ?>">
									</div>
								</div>
								<div class="col-md-6">
              		<div class="input-group">
									<?php
										//Select all category names
										$categoryTypes="select ID,category from ".TABLE_TODAYS_SPECIAL_CATEGORY."";
										$res2=mysql_query($categoryTypes);
									?>
									<select name="group" id="group" class="form-control" required style="height:48px" >							
										<option value="All">All</option>
										<?php
												while($row=mysql_fetch_array($res2))
												{?>	
													<option value="<?php echo $row['ID']?>"><?php echo $row['category']?></option>
												<?php 									
												}?>	            			
											</select>           						
									</select>             
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> 
								</div>
								</div>
							</row>
            </form>
          </div>
        </div>
        </div>
 <?php	
$cond="1";
if(@$_REQUEST['group'])
{
	if($_POST['group']=="All")
	{
		$cond=$cond." and ".TABLE_TODAYS_SPECIAL.".topic like'%".$_POST['topic']."%'";
	}
	else
	{
		$cond=$cond." and ".TABLE_TODAYS_SPECIAL.".topic like'%".$_POST['topic']."%' AND ".TABLE_TODAYS_SPECIAL.".category_id=".$_POST['group']."";
	}
	
}

?>
        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table id="rowspan" class="tablesorter table view_limitter pagination_table" >
                <thead>
                  <tr >
										<th>Sl No</th>               
										<th>Category</th>
										<th>Topic</th>
										<th>Posted Date</th>																								
                  </tr>
                </thead>
                <tbody>
								<?php 
										$rowsPerPage=0;															
										$selAllQuery="SELECT * FROM ".TABLE_TODAYS_SPECIAL_CATEGORY.", ".TABLE_TODAYS_SPECIAL." WHERE $cond AND ".TABLE_TODAYS_SPECIAL.".category_id=".TABLE_TODAYS_SPECIAL_CATEGORY.".ID ORDER BY ".TABLE_TODAYS_SPECIAL.".ID ASC";
																
										$selectAll= $db->query($selAllQuery);
										$number=mysql_num_rows($selectAll);
										if($number==0)
										{
										?>
											<tr>
												<td align="center" colspan="5">
													There is no details in list.
												</td>
											</tr>
										<?php
										}
										else
										{
											/*********************** for pagination ******************************/
												$rowsPerPage = ROWS_PER_PAGE;
												if(isset($_GET['page']))
												{
													$pageNum = $_GET['page'];
												}
												else
												{
													$pageNum =1;
												}
												$offset = ($pageNum - 1) * $rowsPerPage;
												$select1=$db->query($selAllQuery." limit $offset, $rowsPerPage");
												$i=$offset+1;
												//use '$select1' for fetching
												/*************************** for pagination **************************/			


											//$i=1;
											while($row=mysql_fetch_array($select1))
											{	
												$tableId=$row['ID'];	?>
												<tr>
												<td><?php echo $i++;?>
															<div class="adno-dtls"> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> |
																		<a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a> |
																		<a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>"> VIEW</a>
															</div>
															<!-- Modal3 -->
																	<div  class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
																	<div class="modal-dialog">
																		<div class="modal-content"> 																																					
																		<div role="tabpanel" class="tabarea2"> 
																			
																			<div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Toady's Special</h4>
            </div>
																			
																			<!-- Tab panes -->
																			<div class="tab-content">
																			<div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
																				<table class="table nobg">
																				<tbody style="background-color:#FFFFFF">
																				
																					<tr>
																						<td>Category</td>
																						<td> : </td>
																						<td><?php  echo $row['category']; ?></td>
																					</tr>

																					<tr>
																						<td>Posted By</td>
																						<td> : </td>
																						<td><?php  echo $row['posted_by']; ?></td>
																					</tr>
																					<tr>
																						<td>Posted date</td>
																						<td> : </td>
																						<td><?php  echo $row['posted_date']; ?></td>
																					</tr>
																					<tr>
																						<td>Topic</td>
																						<td> : </td>
																						<td><?php  echo $row['topic']; ?></td>
																					</tr>
																					<tr>
																						<td>Description</td>
																						<td> : </td>
																						<td><?php  echo $row['description']; ?></td>
																					</tr>
																					
																					<tr>
																						<td>Place</td>
																						<td> : </td>
																						<td><?php  echo $row['place']; ?></td>
																					</tr>
																																										
																					<tr>
																						<td>Photo</td>
																						<td> : </td>
																						<td><?php if($row['image_url']){
																									$path="../../".$row['image_url']; ?>
																									<div style="max-width: 100px;"><img src="<?php echo $path ?>" alt="No Image" class="img-responsive" /></div>
																									<?php }else{?>
																									<div style="max-width: 100px;"><img src="../../img/student.png" alt="No Image" class="img-responsive" /></div>
																									<?php } ?>
																						</td>
																					</tr>																					
																				</tbody>
																				</table>
																			</div>
																													
																			</div>
																		</div>
																		</div>
																	</div>
																	</div>
																	<!-- Modal3 cls --> 																					  						
													</td>
													<td><?php echo $row['category']; ?></td>
													<td><?php echo $row['topic']; ?></td>
													<td><?php echo $row['posted_date']; ?></td>				                   	 	
												</tr>
										<?php }
									}?>                  
                </tbody>
              </table>
			  	
            </div>
						<?php 
										if($number>$rowsPerPage)
										{
										?>	
										<br />	
										<div style="text-align: center";>
											<div class="pagerSC" align="center" style="display: inline-block;">
										<?php
										
										$query   =  $db->query($selAllQuery);
										$numrows = mysql_num_rows($query);
										$maxPage = ceil($numrows/$rowsPerPage);
										$self = $_SERVER['PHP_SELF'];
										$nav  = '';
										if ($pageNum - 5 < 1) {
										$pagemin = 1;
										} else {
										$pagemin = $pageNum - 5;
										};
										if ($pageNum + 5 > $maxPage) {
										$pagemax = $maxPage;
										} else {
										$pagemax = $pageNum + 5;
										};
										
										$search=@$_REQUEST['search'];
										for($page = $pagemin; $page <= $pagemax; $page++)
										{
											if ($page == $pageNum)
											{
												$nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
											}
											else
											{
												if(@$search)
													{
													$nav .= " <a href=\"$self?page=$page&search=$search\">$page</a> ";
												}
												else
												{
													$nav .= " <a href=\"$self?page=$page\">$page</a> ";
												}
											}
										}
										?>
										<?php
										if ($pageNum > 1)
										{
											$page  = $pageNum - 1;
											if(@$search)
											{
												$prev  = " <a href=\"$self?page=$page&search=$search\">Prev</a> ";
												$first = " <a href=\"$self?page=1&search=$search\">First</a> ";
											}
											else
											{
												$prev  = " <a href=\"$self?page=$page\">Prev</a> ";
												$first = " <a href=\"$self?page=1\">First</a> ";
											}
										}
										else
										{
											$prev  = '&nbsp;';
											$first = '&nbsp;';
										}
										
										if ($pageNum < $maxPage)
										{
											$page = $pageNum + 1;
											if(@$search)
											{
													$next = " <a href=\"$self?page=$page&search=$search\">Next</a> ";
													$last = " <a href=\"$self?page=$maxPage&search=$search\">Last</a> ";
											}
											else
											{
													$next = " <a href=\"$self?page=$page\">Next</a> ";
													$last = " <a href=\"$self?page=$maxPage\">Last</a> ";
											}
										}
										else
										{
											$next = '&nbsp;';
											$last = '&nbsp;';
										}
										echo $first . $prev . $nav . $next . $last;
										?>
										<div style="clear: left;"></div>
										</div>	 
									</div>
									<?php
									}
									?>

			<!-- paging -->		
				<div style="clear:both;"></div>
				<div class="text-center">
					<div class="btn-group pager_selector"></div>
				</div>        
				<!-- paging end-->
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Today's Special </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" enctype = "multipart/form-data" method="post" onsubmit="return valid()">
                <div class="row">
                  <div class="col-sm-6">

                    <div class="form-group">
                      <label for="courseName">Category <span class="star">*</span></label>
                      <select name="group" id="group" class="form-control2" required >	
												<?php
												$res2=mysql_query($categoryTypes);
												while($row=mysql_fetch_array($res2))
												{?>	
													<option value="<?php echo $row['ID']?>"><?php echo $row['category']?></option>
												<?php 									
												}?>	 									
												</select>				
					  					<span id="user-result"></span>
                    </div>	

										

										<div class="form-group">
                      <label for="name">Topic<span class="star">*</span></label>
                      <input type="text" id="topic" name="topic" placeholder="Topic" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
                      <label for="name">Description</label>
                      <textarea placeholder="Description" class="form-control2" name="description" id="description" cols="30" rows="10"></textarea>
						<span id="user-result"></span>
                    </div>
					<div class="form-group">
                      <label for="driver_name">Posted Date <span class="star">*</span></label>
                      <input type="text" id="newsDate" name="posted_date" class="form-control2 datepicker" required readonly value="<?php echo date("d/m/Y");?>"  >				
					  					<span id="user-result"></span>
                    </div>
						<div class="form-group">
                      <label for="driver_name">Posted By</label>
                      <input type="text" id="posted_by" placeholder="Posted By" name="posted_by" Value="Admin" class="form-control2">				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
                      <label for="place">Place</label>
                      <input type="place" id="registration_number" placeholder="Place" name="place" class="form-control2">				
					  					<span id="user-result"></span>
                    </div>
					<div class="form-group">
											<input type="checkbox" name="isViewable" id="isViewable">
											<label for="contact_no">Show in Home</label>
										</div>
										<div class="form-group">
                      <label for="place">Image</label>
                      <input type="file" id="image_url" name="image_url" class="form-control2">				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
											<img id="thumb" width="100" height="100" src="../../img/student.png" alt="your image" />
										</div>
									</div>
								</div>
			  				<div>
            		</div>
            		<div class="modal-footer">
              		<input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            		</div>
							</form>
          	</div>
        	</div>
      	</div>
	  	</div>
    </div>
<?php include("../adminFooter.php") ?>
