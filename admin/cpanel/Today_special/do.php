<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_POST['group'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");
			}
		else
			{							
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$isViewable                     = 0;
				if (isset($_POST['isViewable'])) {
					$isViewable                 =   1;
				}
				$img                            =   "";

				$data['category_id']			=	$App->convert($_REQUEST['group']);
				$data['description']			=	$App->convert($_REQUEST['description']);
				$data['place']					=	$App->convert($_REQUEST['place']);
				$data['posted_by']				=	$App->convert($_REQUEST['posted_by']);
				$data['isViewable']					=	$App->convert($isViewable);
				$data['topic']					=	$App->convert($_REQUEST['topic']);
				$data['posted_date']			=	$App->dbFormat_date($_REQUEST['posted_date']);
				
				//Status is hard coded,Coz feature is not implimented now
				$data['status']					=	1;//$App->convert($_REQUEST['status']);
					
				if($_FILES["image_url"]["name"])
				{
					$file_name 		 				= 	pathinfo($_FILES["image_url"]["name"]);
					$file_ext	 					=	$file_name["extension"];
					$allowd_ext 					= 	array("jpeg","jpg","png");
					$rename 						= 	date("YmdHis");
					$new_file_name 					= 	$rename.".".$file_ext;
					if(in_array($file_ext,$allowd_ext) ) 	
					{
						if(move_uploaded_file($_FILES["image_url"]["tmp_name"],"../../Images/todayspecial/".basename($new_file_name)))
						{
							$img="Images/todayspecial/" . $new_file_name;
							$data['image_url']		=	$App->convert($img);
						}
					}
					else
					{	
						$db->close();
						$_SESSION['msg']="Invalid image";
						header("location:index.php");
						return;	
					}
				}
				else
				{
					$data['image_url']				=	$App->convert($img);
				}
				$success 							=	$db->query_insert(TABLE_TODAYS_SPECIAL, $data);	
				$db->close();				
				if($success)
				{
					$_SESSION['msg'] 				=	"Special Added Successfully";										
				}
				else
				{
					$_SESSION['msg'] 				=	"Failed";							
				}	
				header("location:index.php");		
				
		}
		break;
		
	// EDIT SECTION
	//-
	case 'edit':
		
		$fid	=	$_REQUEST['fid'];	       
		if(!$_POST['group'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$isViewable                     = 0;
				if (isset($_POST['isViewable'])) {
					$isViewable                 =   1;
				}
				$img                            =   "";

				$data['category_id']			=	$App->convert($_REQUEST['group']);
				$data['description']			=	$App->convert($_REQUEST['description']);
				$data['place']					=	$App->convert($_REQUEST['place']);
				$data['posted_by']				=	$App->convert($_REQUEST['posted_by']);
				$data['topic']					=	$App->convert($_REQUEST['topic']);
				$data['posted_date']			=	$App->dbFormat_date($_REQUEST['posted_date']);
				$data['isViewable']				=	$App->convert($isViewable);
				
				//Status is hard coded,Coz feature is not implimented now
				$data['status']					=	1;//$App->convert($_REQUEST['status']);

				if($_FILES["image_url"]["name"])
				{
					$file_name 		 				= 	pathinfo($_FILES["image_url"]["name"]);
					$file_ext	 					=	$file_name["extension"];
					$allowd_ext 					= 	array("jpeg","jpg","png");
					$rename 						= 	date("YmdHis");
					$new_file_name 					= 	$rename.".".$file_ext;
					if(in_array($file_ext,$allowd_ext) ) 	
					{
						if(move_uploaded_file($_FILES["image_url"]["tmp_name"],"../../Images/todayspecial/".basename($new_file_name)))
						{
							$img="Images/todayspecial/" . $new_file_name;
							$data['image_url']		=	$App->convert($img);

							$res=mysql_query("select image_url from ".TABLE_TODAYS_SPECIAL." where ID='{$fid}'");
        					$row=mysql_fetch_array($res);
        					if ($row['image_url']) {
								$image = '../../'.$row['image_url'];
								if (file_exists($image))
								{
									unlink($image);
								}
					        }
						}
					}
					else
					{	
						$db->close();
						$_SESSION['msg']="Invalid image";
						header("location:index.php");
						return;	
					}
				}		 
				$success=$db->query_update(TABLE_TODAYS_SPECIAL, $data,"ID='{$fid}'");
				$db->close();
				
				if($success)
				{
					$_SESSION['msg']="Details updated successfully";																	
				}
				else
				{
					$_SESSION['msg']="Failed";									
				}	
				header("location:index.php");						
		}
		
		break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				try
				{	
					$res=mysql_query("select image_url from ".TABLE_TODAYS_SPECIAL." where ID='{$id}'");
        			$row=mysql_fetch_array($res);
					$old_image = $row['image_url'];

					$success= @mysql_query("DELETE FROM `".TABLE_TODAYS_SPECIAL."` WHERE ID='{$id}'");	      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}				
				$db->close(); 
				
				
				if($success)
				{
					if ($old_image) {
						$image = '../../'.$old_image;
						if (file_exists($image))
						{
							unlink($image);
						}
					}
					$_SESSION['msg']="Data deleted successfully";	
														
				}
				else
				{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
				}	
				header("location:index.php");	
		break;	
}
?>