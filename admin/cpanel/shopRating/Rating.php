<?php
	require("../../config/config.inc.php"); 
	require("../../config/Database.class.php");
	require("../../config/Application.class.php");

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();

	$tableId 	= 	$_REQUEST['shopId'];
	$numPics	=	$_REQUEST['Rating'];	
	$shopName 	=	$_REQUEST['shop'];
	$admin_rate 	=	round($_REQUEST['admin_rate'],2);

	$selAllQuery3="select `".TABLE_SHOP_RATING."`.rating, `".TABLE_USER."`.first_name,
				`".TABLE_USER."`.last_name																 										
				from `".TABLE_SHOP_RATING."`,`".TABLE_USER."`
				where `".TABLE_SHOP_RATING."`.shop_id='$tableId' and `".TABLE_SHOP_RATING."`.user_id=`".TABLE_USER."`.ID																																			   
				order by `".TABLE_SHOP_RATING."`.ID ";
	//echo $selAllQuery3;
?>
<div role="tabpanel" class="tab-pane active" id="photo<?php echo $tableId; ?>">
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label for="shopName"><?= $shopName; ?></span></label>		                                  
			</div>
			<div class="form-group">
				<label for="shopName">Avg Ratings : <?php if($admin_rate==-1){ echo $numPics;}else{ echo $admin_rate;} ?></span></label>		                                  
			</div>									
		</div>
		<div class="col-sm-6">
			<form method="post" action="do.php?op=edit" class="form1" enctype="multipart/form-data" onsubmit="return valid()">
				<div class="row">
					<input type="hidden" name="fid" id="fid" value="<?php echo $tableId ?>">
					<div class="col-sm-3">
						<label for="shopName">Update as :</span></label>
					</div>
					<div class="col-sm-5">
						<input type="number" step="0.01" max="5" min="-1" id="admin_rate" title="enter -1 for reset" name="admin_rate" required class="form-control2 check_float" placeholder="Maximum 5" />
					</div>
					<div class="col-sm-4">
						<input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
					</div>
				</div>
			</form>
		</div>
	</div>              		          		
	<div class="tablearea table-responsive">
		<table class="table table_pop" id="headerTable2" >
			<thead style="background-color:#CCC;">
				<tr>
					<th>Sl No</th>
					<th>User</th>
					<th>Ratings</th> 				                           
				</tr>
			</thead>
			<tbody>                             
				<?php			
					$selectAll3= $db->query($selAllQuery3);
					$num3=mysql_num_rows($selectAll3);							

					if($num3==0)
					{
						?>
						<tr>
							<td colspan="3" align="center">No data found</td>
						</tr>
						<?php
					}
					else
					{	
						$j=0;														
						while($row3=mysql_fetch_array($selectAll3))
						{						 								
							?>
							<tr>
								<td><?php echo ++$j;?></td>
								<td><?php echo $row3['first_name']." " .$row3['last_name'];?></td>
								<td><?php echo $row3['rating'];?></td> 														                                                             
							</tr>
							<?php					
						}								
					}
				?>
			</tbody>
		</table>
	</div>
</div>