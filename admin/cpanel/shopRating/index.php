<?php 
	include("../adminHeader.php");

	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}
	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>

<script>
	// ajax for loading photo

	function loadRating(sid,rates,shop,admin_rate)
	{
		var xmlhttp;
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("photoDiv"+sid).innerHTML=xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","Rating.php?shopId="+sid+"&Rating="+rates+"&shop="+shop+"&admin_rate="+admin_rate,true);
		xmlhttp.send();
	}
</script>

<?php
	if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
	$_SESSION['msg']='';
 ?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">SHOP RATINGS</h2> 					 
				</div>
          </div>
          <div class="col-sm-4" >
            <form method="post">
              <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="Shop Name / Category" value="<?php echo @$_REQUEST['search'] ?>">
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> </div>
            </form>
          </div>
        </div>
		 <?php	
		$cond="1";
		if(@$_REQUEST['search'])
		{						
			$search		=	$_REQUEST['search'];
			$cond=$cond." and ((".TABLE_SHOP.".shop_name like'%".$search."%') or (".TABLE_SHOP_CATEGORY.".category like'%".$search."%'))";
		}
		
		?>
        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table id="rowspan" class="tablesorter table view_limitter pagination_table" >
                <thead>
                  <tr>
								<th>Sl No</th>
								<th>Shop Name</th>
								<th>Phone</th>
								<th>Address</th>
								<th>Category</th>
								<th>Avg Ratings</th>					
								<th></th>															
						</tr>
                </thead>
                <tbody>
						<?php 																					
						$selAllQuery	=  "select ".TABLE_SHOP.".ID,".TABLE_SHOP.".admin_rate,".TABLE_SHOP.".shop_name,
												   ".TABLE_SHOP.".working_hours,  ".TABLE_SHOP.".address,
												   ".TABLE_SHOP.".contact_number, ".TABLE_SHOP.".contact_numbner_alternative,
													 ".TABLE_SHOP.".status, ".TABLE_SHOP_CATEGORY.".category,													   
												   (select COALESCE(AVG(".TABLE_SHOP_RATING.".rating),0) as numPic	FROM ".TABLE_SHOP_RATING." where ".TABLE_SHOP_RATING.".shop_id=".TABLE_SHOP.".ID )  as PhotoCount																		 
											 from `".TABLE_SHOP."`,`".TABLE_SHOP_CATEGORY."`
										    where  `".TABLE_SHOP."`.shop_category_id	=	`".TABLE_SHOP_CATEGORY."`.ID										      
										      and ".TABLE_SHOP.".status=1
										      and $cond	 order by `".TABLE_SHOP."`.ID desc";
										// echo $selAllQuery;
				  	 	$rowsPerPage = ROWS_PER_PAGE;
						$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="9">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
						/*********************** for pagination ******************************/
						
						if(isset($_GET['page']))
						{
							$pageNum = $_GET['page'];
						}
						else
						{
							$pageNum =1;
						}
					//	echo $rowsPerPage;
					
						$offset = ($pageNum - 1) * $rowsPerPage;
						$select1=$db->query($selAllQuery." limit $offset, $rowsPerPage");
						$i=$offset+1;
			//echo $selAllQuery." limit $offset, $rowsPerPage";
						//use '$select1' for fetching
						/*************************** for pagination **************************/
						//$i=1;
						while($row=mysql_fetch_array($select1))
						{	
							$tableId 	= $row['ID'];
							$name 		= $row['shop_name'];
							$numPics 	= round($row['PhotoCount'],2);
							$admin_rate 	= round($row['admin_rate'],2);
							
							$qry = "SELECT ".TABLE_LOCATION.".location
													FROM ".TABLE_SHOP_LOCATION."	LEFT JOIN ".TABLE_LOCATION." ON 	
												".TABLE_LOCATION.".ID = ".TABLE_SHOP_LOCATION.".location_id 
													WHERE ".TABLE_SHOP_LOCATION.".shop_id =$tableId";
								
							$catResult = $db->query($qry);
							$sCategory = array();
							while($catFetch = mysql_fetch_array($catResult))
							{														
									array_push($sCategory,$catFetch['location']);
							}														
							$categoryNw	=	implode(",",$sCategory); 
						?>
                  <tr>
                    <td><?php echo $i;$i++;?>
                      <div class="adno-dtls">  <a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>"> VIEW</a> </div>
												<!-- Modal3 -->
												<div class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
												<div class="modal-dialog">
													<div class="modal-content"> 								
														<div role="tabpanel" class="tabarea2"> 
															
														<!-- Nav tabs -->
														<ul class="nav nav-tabs" role="tablist">									  
															<li role="presentation" class="active"> <a href="#personal<?php echo $tableId; ?>" aria-controls="personal" role="tab" data-toggle="tab">Shop Details</a> </li>										
														</ul>
																											
														<!-- Tab panes -->
														<div class="tab-content">
														<div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
															<table class="table nobg" >
															<tbody style="background-color:#FFFFFF">
																<tr>
																	<td>Shop name</td>
																	<td>:</td>
																	<td><?php echo $row['shop_name']; ?></td>
																</tr>											
																<tr>
																	<td>Category</td>
																	<td>:</td>
																	<td><?php echo $row['category']; ?></td>
																</tr>																
																<tr>
																	<td>Address</td>
																	<td>:</td>
																	<td><?php echo $row['address']; ?></td>
																</tr>																	
																<tr>
																	<td>Phone</td>
																	<td>:</td>
																	<td><?php echo $row['contact_number']; ?></td>
																</tr>	
																<tr>
																	<td>Alternative Phone</td>
																	<td>:</td>
																	<td><?php echo $row['contact_numbner_alternative']; ?></td>
																</tr>																															
																<tr>
																	<td>Work Hour</td>
																	<td>:</td>
																	<td><?php echo $row['working_hours']; ?></td>
																</tr>	
																<tr>
																	<td>Locations</td>
																	<td> : </td>
																	<td> <?php  echo $categoryNw ?></td>
																</tr>								  																													  										 
															</tbody>
															</table>
														</div>
																												
														</div>
													</div>

													</div>
												</div>
												</div>
												<!-- Modal3 cls --> 					  
													</td>                    					
												<td><?php echo $row['shop_name']; ?></td>				
												<td><?php echo $row['contact_number']; ?></td>
												<td><?php echo $row['address']; ?></td>
												<td><?php echo $row['category']; ?></td>
												<td><?php if($admin_rate==-1){ echo $numPics;}else{echo $admin_rate;} ?></td>																							
												<td style="width: 108px;" id="Rating<?php echo $tableId; ?>" ><a href="#" onclick="loadRating('<?php echo $tableId;?>','<?php echo $numPics;?>','<?php echo $name;?>','<?php echo $admin_rate;?>')" data-toggle="modal" data-target="#myModal5<?php echo $tableId; ?>" class="viewbtn">Rating</a>													
													<!-- Modal3 photo -->
													<div class="modal fade" id="myModal5<?php echo $tableId; ?>" tabindex="-1" role="dialog">
														<div class="modal-dialog modal-lg">
															<div class="modal-content">
															<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ratings</h4>
            </div>
																<div role="tabpanel" class="tabarea2">
																	<div class="tab-content" id="photoDiv<?php echo $tableId; ?>">
																		
																</div>
															</div>
														</div>
													</div>
												</div>
														<!-- Modal3 photo cls -->
													</td>												
												</tr>
											<?php }
									}
									?>                  
                </tbody>
              </table>			  			  
            </div>          
            <!--*****************************************************************-->
            
            	 <?php 
                  if($number>$rowsPerPage)
										{
										?>	
										<br />	
											<div class="pagerSC" align="center">
										<?php
										
										$query   =  $db->query($selAllQuery);
										$numrows = mysql_num_rows($query);
										$maxPage = ceil($numrows/$rowsPerPage);
										$self = $_SERVER['PHP_SELF'];
										$nav  = '';
										if ($pageNum - 5 < 1) {
										$pagemin = 1;
										} else {
										$pagemin = $pageNum - 5;
										};
										if ($pageNum + 5 > $maxPage) {
										$pagemax = $maxPage;
										} else {
										$pagemax = $pageNum + 5;
										};
										
										for($page = $pagemin; $page <= $pagemax; $page++)
										{
											if ($page == $pageNum)
											{
												$nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
											}
											else
											{
												if(@$search)
													{
													$nav .= " <a href=\"$self?page=$page&sname=$search\">$page</a> ";
												}
												else
												{
													$nav .= " <a href=\"$self?page=$page\">$page</a> ";
												}
											}
										}
										?>
										<?php
										if ($pageNum > 1)
										{
											$page  = $pageNum - 1;
											if(@$search)
											{
												$prev  = " <a href=\"$self?page=$page&sname=$search\">Prev</a> ";
												$first = " <a href=\"$self?page=1&sname=$search\">First Page</a> ";
											}
											else
											{
												$prev  = " <a href=\"$self?page=$page\">Prev</a> ";
												$first = " <a href=\"$self?page=1\">First Page</a> ";
											}
										}
										else
										{
											$prev  = '&nbsp;';
											$first = '&nbsp;';
										}
										
										if ($pageNum < $maxPage)
										{
											$page = $pageNum + 1;
											if(@$search)
											{
													$next = " <a href=\"$self?page=$page&sname=$search\">Next</a> ";
													$last = " <a href=\"$self?page=$maxPage&sname=$search\">Last Page</a> ";
											}
											else
											{
													$next = " <a href=\"$self?page=$page\">Next</a> ";
													$last = " <a href=\"$self?page=$maxPage\">Last Page</a> ";
											}
										}
										else
										{
											$next = '&nbsp;';
											$last = '&nbsp;';
										}
										echo $first . $prev . $nav . $next . $last;
										?>
										<div style="clear: left;"></div>
										</div>	 
									<?php
									}
                ?>
            
           <!-- ******************************************************************-->
            
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">SHOP REGISTRATION</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()">
                <div class="row">
                  <div class="col-sm-6">
                                     
                    
                    <div class="form-group">
                      <label for="shopName">Shop Name:<span class="valid">*</span></label>
                      <input type="text" class="form-control2" name="shopName" id="shopName" required>
                    </div>	
										<div class="form-group">
                      <label for="appTypeId">Shop Category:<span class="valid">*</span></label>
                      <select class="form-control2" name="categoryId" id="categoryId" required>
                      	<option value="">Select</option>
                      	<?php
													$select2 = mysql_query("select * from ".TABLE_SHOP_CATEGORY."");
													while($row2=mysql_fetch_array($select2))
													{
														?>
														<option value="<?php echo $row2['ID'];?>"><?php echo $row2['category'];?></option>
														<?php
														}
													?>
                      </select>
                    </div>								
										<div class="form-group">
											<label for="uAddress">Address:</label>
											<textarea name="shopAddress" id="shopAddress" class="form-control2"  style="height:80%"></textarea>
										</div>
										<div class="form-group">
											<label for="uPhone">Contact No:<span class="valid">*</span></label>	
											<input type="text" name="shopPhone" id="shopPhone" class="form-control2" required >
											<div id="uPhonediv" style="color:#FF6600; font-family:'Times New Roman', Times, serif"></div>
										</div>
										<div class="form-group">
											<label for="uPhone">Alternative No:<span class="valid">*</span></label>	
											<input type="text" name="shopPhone2" id="shopPhone2" class="form-control2" >
											<div id="uPhonediv" style="color:#FF6600; font-family:'Times New Roman', Times, serif"></div>
										</div>										                                      
                    <div class="form-group">
											<label for="workHour">Working Hour:</label>	
											<input type="text" name="workHour" id="workHour" class="form-control2" >
										</div>
								</div>
								<div class="col-sm-6">			
										<div class="form-group">
											<label for="Location">Location<span class="star">*</span></label>
												<ul class="category_combo_list list-unstyled" style="display: block;">
													<?php 
															$selAllQuery="SELECT * FROM ".TABLE_LOCATION." ORDER BY location ASC";
															$selectAll= $db->query($selAllQuery);
															while($row=mysql_fetch_array($selectAll))
															{ ?>
																<li><input type="checkbox" name="locations[]" id="breaking" value="<?= $row['ID'];?>">  
																	<label><?= $row['location']; ?></label></li>
													<?php	}	
												?>
												</ul>					  																                     
                    </div>

									</div>				
              </div>                                          
							<div>
							</div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
		</div>
	</div>
</div>
      <!-- Modal1 cls --> 
</div>
<?php include("../adminFooter.php") ?>