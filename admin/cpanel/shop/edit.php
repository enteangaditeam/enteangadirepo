<?php 
	include("../adminHeader.php"); 
	if($_SESSION['LogID']=="")
	{
	header("location:../../logout.php");
	}

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>
<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	$editId=$_REQUEST['id'];
	$tableEditQry	=  "select ".TABLE_SHOP.".ID, ".TABLE_SHOP.".shop_name,".TABLE_SHOP.".sub_name, ".TABLE_SHOP.".shop_category_id,".TABLE_SHOP.".isViewable,
												   ".TABLE_SHOP.".working_hours,  ".TABLE_SHOP.".address,
												   ".TABLE_SHOP.".contact_number, ".TABLE_SHOP.".contact_numbner_alternative,
													 ".TABLE_SHOP.".status,				  ".TABLE_SHOP_CATEGORY.".category,													   
												   (select count(".TABLE_SHOP_IMAGE.".ID) as numPic	FROM ".TABLE_SHOP_IMAGE." where ".TABLE_SHOP_IMAGE.".shop_id=".TABLE_SHOP.".ID )  as PhotoCount																		 
											 from `".TABLE_SHOP."`,`".TABLE_SHOP_CATEGORY."`
										    where  `".TABLE_SHOP."`.shop_category_id	=	`".TABLE_SHOP_CATEGORY."`.ID										      
										      and ".TABLE_SHOP.".status=1 and ".TABLE_SHOP.".ID=$editId										      										      
										 order by `".TABLE_SHOP."`.ID desc";
	
	$tableEdit 	=	mysql_query($tableEditQry);
	$editRow	=	mysql_fetch_array($tableEdit);

	//Location query 
	$qry = "SELECT location_id FROM ".TABLE_SHOP_LOCATION."  WHERE shop_id =$editId";								
	$locResult = $db->query($qry);
	$sLocation = array();
	while($locFetch = mysql_fetch_array($locResult))
	{	 		
	 		array_push($sLocation,$locFetch['location_id']);
	}	
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="index.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">SHOP DETAILS </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=edit" class="form1" method="post" onsubmit="return valid()">
			  <input type="hidden" name="id" id="id" value="<?php echo $editId ?>">			  
                <div class="row">
                  <div class="col-sm-6">                                                                        

					<div class="form-group">
                      <label for="shopName">Shop Name:<span class="valid">*</span></label>
                      <input type="text" class="form-control2" name="shopName" id="shopName" required value="<?php echo $editRow['shop_name'];?>" >
                    </div>	
										<div class="form-group">
                      <label for="shopName">Sub Name:<span class="valid">*</span></label>
                      <input type="text" class="form-control2" value="<?php echo $editRow['sub_name'];?>" name="subName" id="shopName" required>
                    </div>	
					<div class="form-group">
                      <label for="appTypeId">Shop Category:<span class="valid">*</span></label>
                      <select class="form-control2" name="categoryId" id="categoryId" required>
                      	<option value="">Select</option>
                      	<?php
							$select2 = mysql_query("select * from ".TABLE_SHOP_CATEGORY."");
							while($row2=mysql_fetch_array($select2))
							{
								?>
								<option value="<?php echo $row2['ID'];?>" <?php if($row2['ID']==$editRow['shop_category_id']){ echo 'selected' ;}?>><?php echo $row2['category'];?></option>
								<?php
								}
							?>
                      </select>
                    </div>								
					<div class="form-group">
						<label for="uAddress">Address:</label>
						<textarea name="shopAddress" id="shopAddress" class="form-control2"  style="height:80%"><?php echo $editRow['address'];?></textarea>
					</div>
					<div class="form-group">
						<label for="uPhone">Contact No:<span class="valid">*</span></label>	
						<input type="text" name="shopPhone" id="shopPhone" class="form-control2" required value="<?php echo $editRow['contact_number'];?>" >
						<div id="uPhonediv" style="color:#FF6600; font-family:'Times New Roman', Times, serif"></div>
					</div>
					<div class="form-group">
						<label for="uPhone">Alternative No:</label>	
						<input type="text" name="shopPhone2" id="shopPhone2" class="form-control2" value="<?php echo $editRow['contact_numbner_alternative'];?>" >
						<div id="uPhonediv" style="color:#FF6600; font-family:'Times New Roman', Times, serif"></div>
					</div>										                                      
                    <div class="form-group">
						<label for="workHour">Working Hour:</label>	
						<input type="text" name="workHour" id="workHour" class="form-control2" value="<?php echo $editRow['working_hours'];?>" >
					</div>
					<div class="form-group">
                                <input type="checkbox" <?php if($editRow['isViewable']){ echo "checked" ;}?> name="isViewable" id="isViewable">
                                <label for="contact_no">Show in Home</label>
                            </div>
				</div>
				<div class="col-sm-6">			
						<div class="form-group">
							<label for="Location">Location</label>
								<ul class="category_combo_list list-unstyled" style="display: block;">
									<?php 
										$selAllQuery="SELECT * FROM ".TABLE_LOCATION." ORDER BY location ASC";
										$selectAll= $db->query($selAllQuery);
										while($row=mysql_fetch_array($selectAll))
										{ ?>
											<li><input type="checkbox" name="locations[]" id="locations" value="<?= $row['ID'];?>" <?php if(in_array( $row['ID'],$sLocation)){ echo "checked" ;}?>>  
											<label><?= $row['location']; ?></label></li>
										<?php	}	
								?>
								</ul>					  																                     
                    	</div>
					</div>									
															
             </div>                 
             </div>              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>
