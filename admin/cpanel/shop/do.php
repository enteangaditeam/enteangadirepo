<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$loginType	=	$_SESSION['LogType'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['shopName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{	
				$isViewable                     = 0;
				if (isset($_POST['isViewable'])) {
					$isViewable                 =   1;
				}			
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success1=$success2=0;												
				
				$shopName	=	$App->convert($_REQUEST['shopName']);
				$subName	=	$App->convert($_REQUEST['subName']);				
				$phone		=	$App->convert($_REQUEST['shopPhone']);
				$catagory	=   $App->convert($_REQUEST['categoryId']);
				
				$existId=$db->existValuesId(TABLE_SHOP,"shop_name='$shopName' and contact_number='$phone' and shop_category_id='$catagory'");
				if($existId>0)
				{
					$_SESSION['msg']="Shop Is Already Exist";													
				}
				else
				{
					$data1['shop_name']						=	$App->convert($shopName);
					$data1['isViewable']					=	$App->convert($isViewable);
					$data1['sub_name']						=	$App->convert($subName);
					$data1['shop_category_id']				=	$catagory;
					$data1['working_hours']					=	$App->convert($_REQUEST['workHour']);
					$data1['address']						=	$App->convert($_REQUEST['shopAddress']);
					$data1['contact_number']				=	$phone;
					$data1['contact_numbner_alternative']	=	$App->convert($_REQUEST['shopPhone2']);
					$data1['status']						=	1;
					
					$success	=	$db->query_insert(TABLE_SHOP,$data1);	

					$catArray 	=	$_REQUEST['locations'];				
					if(!empty($catArray)) 
					{
						for($i=0;$i<count($catArray);$i++)
						{
							$data['shop_id']	=	$success;
							$data['location_id']=	$catArray[$i];						
							$db->query_insert(TABLE_SHOP_LOCATION,$data);
						}										
					}																		
					$db->close();
					
					if($success)
					{							
						$_SESSION['msg']="Shop Details Added Successfully";											
					}
					else{
						$_SESSION['msg']="Failed";
					}										
				}	
				header("location:index.php");											
			}		
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id']; 
			    	
		if(!$_REQUEST['shopName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success1=0;
				$isViewable                     = 0;
				if (isset($_POST['isViewable'])) {
					$isViewable                 =   1;
				}	
				$shopName	=	$App->convert($_REQUEST['shopName']);
				$subName	=	$App->convert($_REQUEST['subName']);				
				$phone		=	$App->convert($_REQUEST['shopPhone']);
				$catagory	=   $App->convert($_REQUEST['categoryId']);
				
				$existId=$db->existValuesId(TABLE_SHOP,"shop_name='$shopName' and contact_number='$phone' and shop_category_id='$catagory' and ID!='$editId'");			
				if($existId>0)
				{
					$_SESSION['msg']="Shop Is Already Exist";													
				}
				else
				{
					$data1['shop_name']						=	$App->convert($shopName);
					$data1['sub_name']						=	$App->convert($subName);
					$data1['shop_category_id']				=	$catagory;
					$data1['working_hours']					=	$App->convert($_REQUEST['workHour']);
					$data1['address']						=	$App->convert($_REQUEST['shopAddress']);
					$data1['contact_number']				=	$phone;
					$data1['isViewable']					=	$App->convert($isViewable);
					$data1['contact_numbner_alternative']	=	$App->convert($_REQUEST['shopPhone2']);
					$data1['status']						=	1;
					
					$success1=$db->query_update(TABLE_SHOP,$data1," ID='{$editId}'");
					
					$qry = "DELETE FROM ".TABLE_SHOP_LOCATION." where shop_id=$editId";
					$resultQ = $db->query($qry);
					$catArray 	=	$_REQUEST['locations'];				
					if(!empty($catArray)) 
					{
						for($i=0;$i<count($catArray);$i++)
						{
							$data['shop_id']	=	$editId;
							$data['location_id']=	$catArray[$i];						
							$db->query_insert(TABLE_SHOP_LOCATION,$data);
						}										
					}					
					$db->close();
					
					if($success1)
					{							
						$_SESSION['msg']="Shop Details Updated Successfully";										
					}
					else{
						$_SESSION['msg']="Failed";
					}
				}
				header("location:index.php");																
			}	
			break;	
				
	// DELETE SECTION
	case 'delete':		
				$deleteId	=	$_REQUEST['id'];
				$success1	=	$success2	=	0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
								
				try
				{ 	
					$res=mysql_query("select image_url from ".TABLE_SHOP_IMAGE." where shop_id='{$deleteId}'");
					while($row=mysql_fetch_array($res))											
					{		
						$photo="../../".$row['image_url'];
						if (file_exists($photo)) 	
						{
							unlink($photo);					
						} 									
					}	

					$success1	= @mysql_query("DELETE FROM `".TABLE_SHOP_IMAGE."` WHERE shop_id='{$deleteId}'");
					$success1	= @mysql_query("DELETE FROM `".TABLE_SHOP_LOCATION."` WHERE shop_id='{$deleteId}'");
					$success1	= @mysql_query("DELETE FROM `".TABLE_SHOP_RATING."` WHERE shop_id='{$deleteId}'");
					$success2	= @mysql_query("DELETE FROM `".TABLE_SHOP."` WHERE ID='{$deleteId}'");	
								      
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete this";								            
				}											
				$db->close(); 
				if($success2)
				{
					$_SESSION['msg']="Shop details deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete this";										
				}	
				
				header("location:index.php");					
		break;

	// PHOTO UPLOAD
	case 'photo':				
			    	
			if(!$_REQUEST['shopId'])
				{
					$_SESSION['msg']="Error, Invalid Details!";					
					header("location:index.php");	
				}
			else
				{				
					$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
					$db->connect();
					$success=0;
													
					$shopId		=	$App->convert($_REQUEST['shopId']);				
					
					// upload file
					$date		= 	date("YmdHis");
					$path_info  = 	pathinfo($_FILES["photo"]["name"]);
					$ext	 	=	$path_info["extension"];	
					$allowed 	=  	array('bmp','jpg');		// allowed image extensions					
					$newName 	= 	$date.".".$ext;

					if(in_array($ext,$allowed) ) 	
					{
						$img='';	
						if(move_uploaded_file($_FILES["photo"]["tmp_name"],"../../Images/shop/".basename($newName)))
						{
							$img="Images/shop/" . $newName;		
						}
						$data['shop_id']			=	$shopId;		
						$data['image_url']			=	$App->convert($img);						
						
						$success1=$db->query_insert(TABLE_SHOP_IMAGE,$data);											
						$db->close();								
					}
					
					if($success1)
					{							
						$_SESSION['msg']="Shop Photo Added Successfully";										
					}
					else
					{
						if(!in_array($ext,$allowed) ) 
						{
							$_SESSION['msg']="You have uploaded an invalid image file";
						}
						else
						{
							$_SESSION['msg']="Failed";	
						}						
					}
					header("location:index.php");
				}
																					
			break;	
// DELETE SHOP PHOTO
	case 'delPhoto':		
				$deleteId	=	$_REQUEST['deleteId'];
				$shopId		=	$_REQUEST['sid'];
				$success1	=	0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
								
				try
				{ 										
					$res=mysql_query("select image_url from ".TABLE_SHOP_IMAGE." where ID='{$deleteId}'");
					$row=mysql_fetch_array($res);
					$photo="../../".$row['image_url'];	
					$success1= @mysql_query("DELETE FROM `".TABLE_SHOP_IMAGE."` WHERE ID='{$deleteId}'");
					if($success1)					
					{																																	
						if (file_exists($photo)) 	
						{
						unlink($photo);					
						} 						
					}														     
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete this";				            
				}											
				$db->close(); 
				if($success1)
				{
					$_SESSION['msg']="Shop Photo deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete this";										
				}	
				header("location:index.php");					
		break;	
// Priority
	case 'priority':		
		$editId	=	$_REQUEST['shopId']; 
			    	
		if(!$_REQUEST['shopId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();

				$data['priority']	=   $App->convert($_REQUEST['priority']);
				
				$success1=$db->query_update(TABLE_SHOP,$data," ID='{$editId}'");

				$db->close();
					
				if($success1)
				{							
					$_SESSION['msg']="Shop Priority Updated Successfully";										
				}
				else{
					$_SESSION['msg']="Failed";
				}
				
				header("location:index.php");																
			}	
			break;	
			
}
?>