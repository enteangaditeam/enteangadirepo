<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	
		
	// EDIT SECTION
	//-
	case 'edit':
		
			$fid	=	$_REQUEST['fid'];	       
			if(!$_POST['hotspot_name'])
				{				
					$_SESSION['msg']="Error, Invalid Details!";					
					header("location:edit.php?id=$fid");	
				}
			else
				{				
					$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
					$db->connect();
					
					
				   $data['hotspot_name']			=	$App->convert($_REQUEST['hotspot_name']);
				   $data['description']			    =	$App->convert($_REQUEST['description']);
				   $data['place']			        =	$App->convert($_REQUEST['place']);
					
					$data['status']				=	1;				
					$image_name = $_FILES['photo']['name'];
					if($image_name)
                    {
							
						$ext = pathinfo($image_name, PATHINFO_EXTENSION);
						$next	=	date("ymdhis");
						
							if(move_uploaded_file($_FILES["photo"]["tmp_name"],"../../Images/hotspot/".basename($next.'.'.$ext)))
								{
									$img="Images/hotspot/" . $next.'.'.$ext;		
									$data['image_url']			=	$App->convert($img);
									$tableEditQry	=  "SELECT *						  
										FROM ".TABLE_HOTSPOT."						  
										WHERE ".TABLE_HOTSPOT.".ID='$hot_id'";
					
									$tableEdit 	=	mysql_query($tableEditQry);
									$editRow	=	mysql_fetch_array($tableEdit); 
									$delete_image = $editRow['image_url'];
									$file= ("../../".$delete_image);
 									unlink($file);
								}								
								
							
					
					}
					
					$success=$db->query_update(TABLE_HOTSPOT,$data," ID='{$fid}'");				 

					
					
					
					
					$db->close();	

					if($success)
					{
						$_SESSION['msg']="Hotspot approved successfully";																	
					}
					else
					{
						$_SESSION['msg']="Failed";									
					}	
					header("location:index.php");						
										
				}
			
			break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();		
						
				$res=mysql_query("select image_url from ".TABLE_HOTSPOT." where ID='{$id}'");
				$row=mysql_fetch_array($res);
				$photo="../../".$row['image_url'];	
				if (file_exists($photo)) 	
				{
					unlink($photo);					
				} 

				$success= @mysql_query("DELETE FROM `".TABLE_HOTSPOT."` WHERE ID='{$id}'");				      			  					
				$db->close(); 
				
				
				if($success)
				{
					$_SESSION['msg']="Hotspot deleted successfully";									
				}
				else
				{
					$_SESSION['msg']="You can't Delete. ";									
				}	
				header("location:index.php");	
		break;	
}
?>