<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_POST['location'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");
			}
		else
			{							
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
						
				$data['location_id']		=	$App->convert($_REQUEST['location']);
				$data['address']			=	$App->convert($_REQUEST['address']);
				$data['contact_number']		=	$App->convert($_REQUEST['contact_number']);
				$data['office_name']		=	$App->convert($_REQUEST['office_name']);
				$data['services']			=	$App->convert($_REQUEST['services']);
				$data['working_hours']		=	$App->convert($_REQUEST['working_hours']);
				
				//Status is hard coded,Coz feature is not implimented now
				$data['status']			=	"Available";//$App->convert($_REQUEST['status']);				
				
				$success=$db->query_insert(TABLE_GOVERNMENT_OFFICE, $data);
				$db->close();				
				
				if($success)
				{
					$_SESSION['msg']="Office Added Successfully";										
				}
				else
				{
					$_SESSION['msg']="Failed";							
				}	
				header("location:index.php");	
			}
		break;
		
	// EDIT SECTION
	//-
	case 'edit':
		
		$fid	=	$_REQUEST['fid'];	       
		if(!$_POST['location'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
					
									
				$data['location_id']		=	$App->convert($_REQUEST['location']);
				$data['address']			=	$App->convert($_REQUEST['address']);
				$data['contact_number']		=	$App->convert($_REQUEST['contact_number']);
				$data['office_name']		=	$App->convert($_REQUEST['office_name']);
				$data['services']			=	$App->convert($_REQUEST['services']);
				$data['working_hours']		=	$App->convert($_REQUEST['working_hours']);
				
				//Status is hard coded,Coz feature is not implimented now
				$data['status']			=	"Available";//$App->convert($_REQUEST['status']);		

					$success=$db->query_update(TABLE_GOVERNMENT_OFFICE, $data, "ID='{$fid}'");					 
					$db->close(); 				
				
					if($success)
					{
						$_SESSION['msg']="Details updated successfully";																	
					}
					else
					{
						$_SESSION['msg']="Failed";									
					}	
					header("location:index.php");	
				
									
			}
		
		break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				try
				{			
					$success= @mysql_query("DELETE FROM `".TABLE_GOVERNMENT_OFFICE."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}				
				$db->close(); 
				
				
				if($success)
				{
					$_SESSION['msg']=" Data deleted successfully";									
				}
				else
				{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
				}	
				header("location:index.php");	
		break;	
}
?>