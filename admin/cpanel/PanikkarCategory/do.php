<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_POST['category'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");
			}
		else
			{							
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$category=$App->convert($_REQUEST['category']);
				$existId=$db->existValuesId(TABLE_PANIKKAR_TYPE," panikkar_type='$category'");
				if($existId>0)
				{					
					$_SESSION['msg']="Details already Exist !!";					
					header("location:index.php");
					return;				
				}
				else
				{		
					$data['panikkar_type']	=	$App->convert($_REQUEST['category']);
									
					
					if($_FILES["img"]["name"])
					{
						$file_name 		 				= 	pathinfo($_FILES["img"]["name"]);
						$file_ext	 					=	$file_name["extension"];
						$rename 						= 	date("YmdHis");
						$new_file_name 					= 	$rename.".".$file_ext;
						
						if ($App->validateImages($file_ext)) {
                    		if (!file_exists('../../Images/panikar_cat')) {
								mkdir('../../Images/panikar_cat', 0777, true);
							}
							
							if(move_uploaded_file($_FILES["img"]["tmp_name"],"../../Images/panikar_cat/".basename($new_file_name))){
								$img="Images/panikar_cat/" . $new_file_name;
								$data['img']		=	$App->convert($img);
								$success=$db->query_insert(TABLE_PANIKKAR_TYPE, $data);
							}
							else{
								$_SESSION['msg']="Opps...! Category creation failed";
								header("location:index.php");		
								return;	
							}
                		}
						else{
							$_SESSION['msg']="Invalid Image";
							header("location:index.php");		
							return;	
							//Invalid image
						}
					}
					//echo $success;
					$db->close();				
				
					if($success)
					{
						$_SESSION['msg']="Category Added Successfully";										
					}
					else
					{
						$_SESSION['msg']="Failed";							
					}	
					header("location:index.php");		
				}
			}
	break;
			// EDIT SECTION
	//-
	case 'edit':
		
		$fid	=	$_REQUEST['fid'];	       
		if(!$_POST['category'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
					
				$category=$App->convert($_REQUEST['category']);
				$existId=$db->existValuesId(TABLE_PANIKKAR_TYPE," panikkar_type='$txt_category' AND ID!='$fid'");

				if($existId>0)
				{					
					$_SESSION['msg']="Details already Exist !!";					
					header("location:edit.php?id=$fid");						
					break ;
				}
				else
				{					
					$data['panikkar_type']	=	$App->convert($_REQUEST['category']);

					if($_FILES["img"]["name"]){
						$file_name 		 				= 	pathinfo($_FILES["img"]["name"]);
						$file_ext	 					=	$file_name["extension"];
						$rename 						= 	date("YmdHis");
						$new_file_name 					= 	$rename.".".$file_ext;

						if ($App->validateImages($file_ext)) {
							if (!file_exists('../../Images/panikar_cat')) {
								mkdir('../../Images/panikar_cat', 0777, true);
							}
							if(move_uploaded_file($_FILES["img"]["tmp_name"],"../../Images/panikar_cat/".basename($new_file_name))){
								$img="Images/panikar_cat/" . $new_file_name;
								$data['img']		=	$App->convert($img);

								//Delete Previous image
								$res=mysql_query("select img from ".TABLE_PANIKKAR_TYPE." where ID='{$fid}'");
        						$row=mysql_fetch_array($res);
								$image = '../../'.$row['img'];
							}
							else{
								$_SESSION['msg']="Opps...! Category creation failed";
								header("location:index.php");		
								return;	
							}
						}
						else{
							$_SESSION['msg']="Invalid Image";
							header("location:index.php");		
							return;	
						}
					}
					$success=$db->query_update(TABLE_PANIKKAR_TYPE, $data, "ID='{$fid}'");
					if($success){
        				if ($image) {
							if (file_exists($image)){
								unlink($image);
							}
						}
					}					 
					$db->close(); 				
				
					if($success)
					{
						$_SESSION['msg']="Details updated successfully";																	
					}
					else
					{
						$_SESSION['msg']="Failed";									
					}	
					header("location:index.php");	
				}
									
			}
		
		break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$res=mysql_query("select img from ".TABLE_PANIKKAR_TYPE." where ID='{$id}'");
        		$row=mysql_fetch_array($res);
				$image = '../../'.$row['img'];
				try
				{			
					$success= @mysql_query("DELETE FROM `".TABLE_PANIKKAR_TYPE."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}
				
				if($success){
        			if ($image) {
						if (file_exists($image)){
							unlink($image);
						}
					}
				}				
				
				$db->close(); 
				
				
				if($success)
				{
					$_SESSION['msg']="category deleted successfully";									
				}
				else
				{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
				}	
				header("location:index.php");	
		break;
		break;
		
	
}
?>