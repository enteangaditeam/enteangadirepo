<?php 
include("../adminHeader.php") ;

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
$type				=	$_SESSION['LogType'];
$labelList = array("label-success", "label-primary", "label-info", "label-warning", "label-danger");
?>
     


<div class="col-md-10 col-sm-8 rightarea">
	<h2>Dashboard</h2> 
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="dash_panel">
				<div class="dash_panel_header clearfix">
					<div><strong>Quick Look</strong></div>
				</div>
				<div class="dash_panel_body">
					<?php
					$usrCount = mysql_fetch_assoc(mysql_query("SELECT COUNT(*) AS total FROM `".TABLE_USER."`"));
					$newsAppCount = mysql_fetch_assoc(mysql_query("SELECT COUNT(*) AS total FROM `".TABLE_NEWS."` WHERE status = 0"));
					if ($newsAppCount['total'] > 20) {
						$newsLabel = 4;
					} else if ($newsAppCount['total'] > 10 && $newsAppCount['total'] <= 20) {
						$newsLabel = 3;
					} else if ($newsAppCount['total'] > 0 && $newsAppCount['total'] <= 10) {
						$newsLabel = 2;
					} else {
						$newsLabel = 0;
					}
					$eventAppCount = mysql_fetch_assoc(mysql_query("SELECT COUNT(*) AS total FROM `".TABLE_EVENT."` WHERE status = 0"));
					if ($eventAppCount['total'] > 20) {
						$eventLabel = 4;
					} else if ($eventAppCount['total'] > 10 && $eventAppCount['total'] <= 20) {
						$eventLabel = 3;
					} else if ($eventAppCount['total'] > 0 && $eventAppCount['total'] <= 10) {
						$eventLabel = 2;
					} else {
						$eventLabel = 0;
					}
					$panikkarAppCount = mysql_fetch_assoc(mysql_query("SELECT COUNT(*) AS total FROM `".TABLE_PANIKKAR."` WHERE status = 0"));
					if ($panikkarAppCount['total'] > 20) {
						$panikkarLabel = 4;
					} else if ($panikkarAppCount['total'] > 10 && $panikkarAppCount['total'] <= 20) {
						$panikkarLabel = 3;
					} else if ($panikkarAppCount['total'] > 0 && $panikkarAppCount['total'] <= 10) {
						$panikkarLabel = 2;
					} else {
						$panikkarLabel = 0;
					}
					$jobAppCount = mysql_fetch_assoc(mysql_query("SELECT COUNT(*) AS total FROM `".TABLE_JOB."` WHERE status = 0"));
					if ($jobAppCount['total'] > 20) {
						$jobLabel = 4;
					} else if ($jobAppCount['total'] > 10 && $jobAppCount['total'] <= 20) {
						$jobLabel = 3;
					} else if ($jobAppCount['total'] > 0 && $jobAppCount['total'] <= 10) {
						$jobLabel = 2;
					} else {
						$jobLabel = 0;
					}
					$hotspotAppCount = mysql_fetch_assoc(mysql_query("SELECT COUNT(*) AS total FROM `".TABLE_HOTSPOT."` WHERE status = 0"));
					if ($hotspotAppCount['total'] > 20) {
						$hotspotLabel = 4;
					} else if ($hotspotAppCount['total'] > 10 && $hotspotAppCount['total'] <= 20) {
						$hotspotLabel = 3;
					} else if ($hotspotAppCount['total'] > 0 && $hotspotAppCount['total'] <= 10) {
						$hotspotLabel = 2;
					} else {
						$hotspotLabel = 0;
					}
					$sellAppCount = mysql_fetch_assoc(mysql_query("SELECT COUNT(*) AS total FROM `".TABLE_SELL."` WHERE status = 0"));
					if ($sellAppCount['total'] > 20) {
						$sellLabel = 4;
					} else if ($sellAppCount['total'] > 10 && $sellAppCount['total'] <= 20) {
						$sellLabel = 3;
					} else if ($sellAppCount['total'] > 0 && $sellAppCount['total'] <= 10) {
						$sellLabel = 2;
					} else {
						$sellLabel = 0;
					}
					?>
					<ul class="dash_panel_quick_list">						
						<li>
							<a href="<?= ($usrCount['total'] > 0) ? "../users" : "javascript:void(0)" ?>" <?php if ($usrCount['total'] == 0) { ?>class="disabled" <?php } ?>>
								<span class="dash_panel_quick_list_ico"><i class="material-icons">perm_identity</i></span>
								<label>Total Users: </label>
								<span class="label pull-right <?= $labelList[1]; ?>"><?= $usrCount['total']; ?></span>
							</a>
						</li>
						<li>
							<a href="<?= ($newsAppCount['total'] > 0) ? "../NewsApprove" : "javascript:void(0)" ?>" <?php if ($newsAppCount['total'] == 0) { ?>class="disabled" <?php } ?>>
								<span class="dash_panel_quick_list_ico"><i class="material-icons">assignment</i></span>
								<label>News to be approved: </label>
								<span class="label pull-right <?= $labelList[$newsLabel]; ?>"><?= $newsAppCount['total']; ?></span>
							</a>
						</li>
						<li>
							<a href="<?= ($eventAppCount['total'] > 0) ? "../Events/approve.php" : "javascript:void(0)" ?>" <?php if ($eventAppCount['total'] == 0) { ?>class="disabled" <?php } ?>>
								<span class="dash_panel_quick_list_ico"><i class="material-icons">event_seat</i></span>
								<label>Events to be approved: </label>
								<span class="label pull-right <?= $labelList[$newsLabel]; ?>"><?= $eventAppCount['total']; ?></span>
							</a>
						</li>
						<li>
							<a href="<?= ($panikkarAppCount['total'] > 0) ? "../Panikkar/approve.php" : "javascript:void(0)" ?>" <?php if ($panikkarAppCount['total'] == 0) { ?>class="disabled" <?php } ?>>
								<span class="dash_panel_quick_list_ico"><i class="material-icons">people</i></span>
								<label>Panikkar to be approved: </label>
								<span class="label pull-right <?= $labelList[$panikkarLabel]; ?>"><?= $panikkarAppCount['total']; ?></span>
							</a>
						</li>
						<li>
							<a href="<?= ($jobAppCount['total'] > 0) ? "../Job/approve.php" : "javascript:void(0)" ?>" <?php if ($jobAppCount['total'] == 0) { ?>class="disabled" <?php } ?>>
								<span class="dash_panel_quick_list_ico"><i class="material-icons">people</i></span>
								<label>Jobs to be approved: </label>
								<span class="label pull-right <?= $labelList[$jobLabel]; ?>"><?= $jobAppCount['total']; ?></span>
							</a>
						</li>
						<li>
							<a href="<?= ($hotspotAppCount['total'] > 0) ? "../HotspotApprove" : "javascript:void(0)" ?>" <?php if ($hotspotAppCount['total'] == 0) { ?>class="disabled" <?php } ?>>
								<span class="dash_panel_quick_list_ico"><i class="material-icons">room</i></span>
								<label>Hotspots to be approved: </label>
								<span class="label pull-right <?= $labelList[$hotspotLabel]; ?>"><?= $hotspotAppCount['total']; ?></span>
							</a>
						</li>
						<li>
							<a href="<?= ($sellAppCount['total'] > 0) ? "../users" : "javascript:void(0)" ?>" <?php if ($sellAppCount['total'] == 0) { ?>class="disabled" <?php } ?>>
								<span class="dash_panel_quick_list_ico"><i class="material-icons">local_grocery_store</i></span>
								<label>Sell items to be approved: </label>
								<span class="label pull-right <?= $labelList[$sellLabel]; ?>"><?= $sellAppCount['total']; ?></span>
							</a>
						</li>
					</ul>
				</div>
			</div>	
			<div class="dash_panel table-responsive">
				<table class="table table-bordered dash_panel_shop_table">
					<thead>
						<tr>
							<th colspan="4">
								<div class="pull-left"><strong>Top Rated Shops</strong></div>
								<div class="pull-right"><a href="../shop"><strong>See All</strong><i class="fa fa-chevron-right"></i></a></div>
							</th>
						</tr>
						<tr>
							<th>#</th>
							<th>Shop Name</th>
							<th>Contact No</th>
							<th>Rating</th>
						</tr>
					</thead>
					<tbody>
						<?php							
							$shopRes = $db->query("SELECT COALESCE(AVG(SR.rating),0) as shopRate, S.shop_name, S.contact_number
										  FROM ".TABLE_SHOP." S
									 LEFT JOIN ".TABLE_SHOP_RATING." SR ON S.ID = SR.shop_id
								      GROUP BY S.ID ORDER BY shopRate DESC LIMIT 5");
							if (mysql_num_rows($shopRes) > 0) {
								$i = 0;
								while ($shopRow = mysql_fetch_array($shopRes)) {
									?>
									<tr>
										<td><?= $i + 1; ?></td>
										<td><?= $shopRow['shop_name']; ?></td>
										<td><?= $shopRow['contact_number']; ?></td>
										<td style="text-align: center;"><span class="label <?= ($shopRow['shopRate'] > 0) ? $labelList[$i] : $labelList[4] ?>"><?= round($shopRow['shopRate'], 2); ?>/5</span></td>
									</tr>
									<?php
									$i++;
								}
							} else {
								?>
								<tr>
									<td colspan="4">No shops are registered yet.</td>
								</tr>
								<?php
							}		  
							
						?>						
					</tbody>
				</table>
			</div>	
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="dash_panel">
				<div class="dash_panel_header clearfix">
					<div class="pull-left"><strong>Users</strong></div>
					<div class="pull-right"><a href="../users"><strong>See All</strong><i class="fa fa-chevron-right"></i></a></div>
				</div>
				<div class="dash_panel_body">
					<?php
					$userRes = mysql_query("SELECT * FROM `".TABLE_USER."` ORDER BY ID DESC LIMIT 5");
					if (mysql_num_rows($userRes) > 0) {
						while ($userRow = mysql_fetch_array($userRes)) {
							?>
							<div class="dash_panel_user">
								<img src="../../<?php if ($userRow['image_url']) { echo $userRow['image_url']; } else { echo "img/student.png"; } ?>" alt="" />
								<ul class="dash_panel_user_det">
									<li><?= $userRow['first_name']. ' '. $userRow['last_name']; ?></li>
									<li><?= $userRow['email']; ?></li>
									<li><?= $userRow['mobile']; ?></li>
								</ul>
								<div style="clear: both;"></div>
							</div>
							<?php
						}
					} else {
						?>
						<p style="padding: 5px;">No users registered yet.</p>
						<?php
					}
					?>					
				</div>
			</div>	
			<div class="dash_panel">
				<div class="dash_panel_header clearfix">
					<div class="pull-left"><strong>Feedbacks</strong></div>
					<div class="pull-right"><a href="../Comments"><strong>See All</strong><i class="fa fa-chevron-right"></i></a></div>
				</div>
				<div class="dash_panel_body">
					<ul class="dash_panel_list dash_panel_list_feedback">
						<?php
						$feedRes = mysql_query("SELECT comments FROM `".TABLE_COMMENTS."` ORDER BY ID DESC LIMIT 5");
						if (mysql_num_rows($feedRes) > 0) {
							while ($feedRow = mysql_fetch_array($feedRes)) {
								?>
								<li><?= $App->split_sentence($feedRow['comments'], 100) ?></li>
								<?php
							}
						} else {
							?>
							<li>No feedbacks recieved yet.</li>
							<?php	
						}
						?>
					</ul>
				</div>
			</div>	
		</div>
	</div>
</div>



<?php include("../adminFooter.php") ?>