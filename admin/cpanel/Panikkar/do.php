<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$loginType	=	$_SESSION['LogType'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['name'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success1=$success2=0;												
				
				$name		=	$App->convert($_REQUEST['name']);				
				$Phone		=	$App->convert($_REQUEST['Phone']);				
				
				$existId=$db->existValuesId(TABLE_PANIKKAR,"panikkar_name='$name' and contact_number='$Phone' ");
				if($existId>0)
				{
					$_SESSION['msg']="Details Is Already Exist";													
				}
				else
				{
					$data1['panikkar_name']		=	$name;
					$data1['contact_number']	=	$Phone;
					$data1['working_time']		=	$App->convert($_REQUEST['wotking']);				
					$data1['panikkar_type_id']	=	$App->convert($_REQUEST['categoryId']);												
					$data1['status']			=	1;
					
					$success	=	$db->query_insert(TABLE_PANIKKAR,$data1);	

					$catArray 	=	$_REQUEST['locations'];
					if(!empty($catArray)) 
					{
						for($i=0;$i<count($catArray);$i++)
						{
							$data['panikar_id']	=	$success;
							$data['location_id']=	$catArray[$i];						
							$db->query_insert(TABLE_PANIKKAR_LOCATION,$data);
						}										
					}																		
					$db->close();
					if($success)
					{							
						$_SESSION['msg']="Details Added Successfully";											
					}
					else{
						$_SESSION['msg']="Failed";
					}										
				}	
				header("location:index.php");											
			}		
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id']; 
			    	
		if(!$_REQUEST['name'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success1=0;
				
				$name		=	$App->convert($_REQUEST['name']);				
				$Phone		=	$App->convert($_REQUEST['Phone']);				
							
				$existId=$db->existValuesId(TABLE_PANIKKAR,"panikkar_name='$name' and contact_numbe='$Phone' and ID!='$editId'");			
				if($existId>0)
				{
					$_SESSION['msg']="Details Is Already Exist";													
				}
				else
				{
					$imgRes = mysql_query("SELECT status FROM `".TABLE_PANIKKAR."` WHERE ID = ".$editId);
            		$imgRow = mysql_fetch_array($imgRes);
            		$status = $imgRow['status'];
					
					$data1['panikkar_name']		=	$name;
					$data1['contact_number']	=	$Phone;
					$data1['working_time']		=	$App->convert($_REQUEST['wotking']);				
					$data1['panikkar_type_id']	=	$App->convert($_REQUEST['categoryId']);												
					$data1['status']			=	$status;
					
					$success1=$db->query_update(TABLE_PANIKKAR,$data1," ID='{$editId}'");
					
					$qry = "DELETE FROM ".TABLE_PANIKKAR_LOCATION." where panikar_id=$editId";
					$resultQ = $db->query($qry);
					$catArray 	=	$_REQUEST['locations'];				
					if(!empty($catArray)) 
					{
						for($i=0;$i<count($catArray);$i++)
						{
							$data['panikar_id']	=	$editId;
							$data['location_id']=	$catArray[$i];						
							$db->query_insert(TABLE_PANIKKAR_LOCATION,$data);
						}										
					}					
					$db->close();
					
					if($success1)
					{							
						$_SESSION['msg']="Details Updated Successfully";										
					}
					else{
						$_SESSION['msg']="Failed";
					}
				}
				header("location:index.php");
				if(!$status)
				{
					header("location:approve.php");
				}																
			}	
			break;	
				
	// DELETE SECTION
	case 'delete':		
				$deleteId	=	$_REQUEST['id'];
				$success1	=	$success2	=	0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				$imgRes = mysql_query("SELECT status FROM `".TABLE_PANIKKAR."` WHERE ID = ".$deleteId);
            	$imgRow = mysql_fetch_array($imgRes);
            	$status = $imgRow['status'];				
				try
				{ 	
					$res=mysql_query("select image_url from ".TABLE_PANIKKAR." where ID='{$deleteId}'");
					while($row=mysql_fetch_array($res))											
					{		
						$photo="../../".$row['image_url'];																																									
						if (file_exists($photo)) 	
						{
							unlink($photo);					
						} 									
					}	

					$success1	= @mysql_query("DELETE FROM `".TABLE_PANIKKAR_LOCATION."` WHERE panikar_id='{$deleteId}'");
					$success1	= @mysql_query("DELETE FROM `".TABLE_PANIKKAR."` WHERE ID='{$deleteId}'");	
								      
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete this";				            
				}											
				$db->close(); 
				if($success1)
				{
					$_SESSION['msg']="Details deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete this";										
				}	
				header("location:index.php");
				if(!$status)
				{
					header("location:approve.php");
				}					
		break;
		case 'approve':
			if(!$_REQUEST['id'])
			{
				$_SESSION['msg']="Error, Invalid Details!";
				header("location:approve.php");
			} 
			else 
			{
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
				$db->connect();
				$fid = $_REQUEST['id'];
				$success = 0;
				$data['status'] = 1;
				$success=$db->query_update(TABLE_PANIKKAR, $data, "ID='{$fid}'");
				if($success)
				{
					$_SESSION['msg']="Event approved successfully";
				}
				else
				{
					$_SESSION['msg']="Something went wrong!";
				}
				header("location:approve.php");
				break;
			}			
}
?>