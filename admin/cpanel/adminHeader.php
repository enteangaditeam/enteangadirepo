<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}

$uid 				=	$_SESSION['LogID'];
$type				=	$_SESSION['LogType'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Enthe Angadi</title>
<script type="text/javascript" src="../../js/modernizr-1.5.min.js"></script>
<script type="text/javascript" src="../../js/jquery.js"></script>


<!-- Bootstrap -->
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/style.css" rel="stylesheet">
<link href="../../css/newstyle.css" rel="stylesheet">
<link href="../../css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,900' rel='stylesheet' type='text/css'>
<link href="../../css/owl.carousel.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="../../font-awesome/css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="../../xdsoft_date/jquery.datetimepicker.min.css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<!--<link rel="stylesheet" type="text/css" href="../../css/bootstrap-theme.css">-->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
<script type="text/javascript" src="../../js/jquery.tablesorter.js"></script>	
<script type="text/javascript">
		jQuery(document).ready(function($) {
			$("#rowspan").tablesorter({debug: true});
		});
</script>

</head>
<body class="bgmain">
<div id="header">
  <div class="container-fuid"> <a href="#" class="logo" style="padding-left: 25px;">
        <img src="../../img/logo.png"/><span>Ente Angadi</span>
      </a>
    <div class="navigation">
      <div class="dropdown"> <a style="color:#565656; background:none;" id="dLabel" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"><i class="glyphicon glyphicon-user
"></i> Admin <span class="caret"></span> </a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
          <li><a href="../changePassword/">Change Password</a></li>
          <li><a href="../../logout.php">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="clearer"></div>
</div>


<div id="contentarea" style="padding-top:0;">
  <div class="container-fuid">
    <div class="row">
      <div class="col-md-2 col-sm-4 leftnav">
        <div class="leftmenu" style="background-color: #222d32;">
        	<div class="mobmenu"></div>
		 			<ul>
            <li> <a href="../adminDash/index.php" class="dropdown-toggle" ><span class="icon_nav"><i class="material-icons">menu</i></span>Home </a></li>
            <li> <a href="../Location" class="dropdown-toggle" ><span class="icon_nav"><i class="material-icons">location_on</i></span>Location</a></li>		
            <li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon_nav"><i class="material-icons">assignment</i></span>News</a>
				<ul class="dropdown-menu dropdown2" role="menu">
					<li><a href="../News">Add News</a></li>
					<li><a href="../NewsApprove">Approve News</a></li>              	
				</ul></li>
			
			<li>
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<span class="icon_nav"><i class="material-icons">event_seat</i></span>Events</a>
				<ul class="dropdown-menu dropdown2" role="menu">
					<li><a href="../Events/">Add Events</a></li>
					<li><a href="../Events/approve.php">Approve Events</a></li>
				</ul>
			</li>
            <li>
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon_nav"><i class="material-icons">store</i></span>Shops</a>
				<ul class="dropdown-menu dropdown2" role="menu">
					<li><a href="../Shop_cat">Category</a></li>
					<li><a href="../shop">Add Shops</a></li>
					<li><a href="../shopRating">Rating</a></li>			                            
				</ul></li>	            
            <li> <a href="../Blood" class="dropdown-toggle"><span class="icon_nav"><i class="material-icons">healing</i></span>Blood</a></li>
			<li> <a href="#"  class="dropdown-toggle" data-toggle="dropdown"><span class="icon_nav"><i class="material-icons">local_convenience_store</i></span>Emergency Numbers</a>
				<ul class="dropdown-menu dropdown2" role="menu">
					<li><a href="../Emergency_Category">Category</a></li>							
					<li><a href="../Emergency">Add Emergency</a></li>			                            
				</ul></li>
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon_nav"><i class="material-icons">local_hospital</i></span>Hospitals &amp; Doctors</a>
				<ul class="dropdown-menu dropdown2" role="menu">
					<li><a href="../HospitalCategory">Category</a></li>
					<li><a href="../Hospital">Add Hospital</a></li>
                </ul></li>	
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon_nav"><i class="material-icons">school</i></span>Educational Institution</a>
				<ul class="dropdown-menu dropdown2" role="menu">
					<li><a href="../InstituteCat">Institution Type</a></li>
					<li><a href="../Institute">Add Institution</a></li>			                            
				</ul></li>		
			<li> <a href="../Government_office" ><span class="icon_nav"><i class="material-icons">domain</i></span>Govt. Offices</a></li>	
			<li>
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon_nav"><i class="material-icons">people</i></span>Panikkar &amp; Panikal</a>
				<ul class="dropdown-menu dropdown2" role="menu">					
					<li><a href="../PanikkarCategory">Panikkar Category</a></li>
					<li><a href="../Panikkar">Add Panikkar</a></li>
					<li><a href="../Panikal/index.php">Panikal</a></li>
					<li><a href="../Panikal/approve.php">Approve Panikal</a></li>
					<li><a href="../Panikkar/approve.php">Approve Panikkar</a></li>
				</ul>
			</li>			
			<li>
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon_nav"><i class="material-icons">people</i></span>Job</a>
				<ul class="dropdown-menu dropdown2" role="menu">
					<li><a href="../JobCategory">Job Category</a></li>
					<li><a href="../Job">Add Job</a></li>
					<li><a href="../Job/approve.php">Approve Job</a></li>					
				</ul>
			</li>
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon_nav"><i class="material-icons">directions_car</i></span>Taxi</a>
				<ul class="dropdown-menu dropdown2" role="menu">
					<li><a href="../Taxi_Category">Category</a></li>
					<li><a href="../Taxi">Add Taxi</a></li>			                            
				</ul></li>			  
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon_nav"><i class="material-icons">flag</i></span>Association &amp; Political Party</a>
				<ul class="dropdown-menu dropdown2" role="menu">
					<li><a href="../Association_Category">Category</a></li>
					<li><a href="../Association">Add Association</a></li>			                            				
			</ul></li>
			<li> <a href="../Bus_time" class="dropdown-toggle"><span class="icon_nav"><i class="material-icons">directions_bus</i></span>Bus Time</a></li>
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon_nav"><i class="material-icons">room</i></span>Hot Spot</a>
				<ul class="dropdown-menu dropdown2" role="menu">
					<li><a href="../Hotspot">Add Hot Spot</a></li>
					<li><a href="../HotspotApprove">Approve Hot Spot</a></li>              	
				</ul>
			</li>
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon_nav"><i class="material-icons">local_grocery_store</i></span>Sell</a>
				<ul class="dropdown-menu dropdown2" role="menu">
					<li><a href="../Sell_Category">Category</a></li>
					<li><a href="../Sell">Sell Items</a></li>			                            
					<li><a href="../Sell/approve.php">Approve Selling Details</a></li>			                            
				</ul></li>
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon_nav"><i class="material-icons">pages</i></span>Today's Special</a>
				<ul class="dropdown-menu dropdown2" role="menu">
					<li><a href="../Today_special_Category">Category</a></li>
					<li><a href="../Today_special">Add Today's Special</a></li>			                            
				</ul></li>
			<li> <a href="../Offers" class="dropdown-toggle"><span class="icon_nav"><i class="material-icons">local_offer</i></span>Offers</a></li>
			<li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<span class="icon_nav"><i class="material-icons">live_help</i></span>Quiz</a>
                            <ul class="dropdown-menu dropdown2" role="menu">
                                <li><a href="../Quiz">Add Quiz</a></li>
                                <li><a href="../Quiz/userAnswers.php">User Answers</a></li>
                            </ul>
                        </li>
			<li> <a href="../Ad" class="dropdown-toggle"><span class="icon_nav"><i class="material-icons">font_download</i></span>Advertisements</a></li>
			<li> <a href="../users" class="dropdown-toggle"><span class="icon_nav"><i class="material-icons">perm_identity</i></span>Users</a></li>
			<li> <a href="../Comments" class="dropdown-toggle"><span class="icon_nav"><i class="material-icons">feedback</i></span>Feedbacks</a></li>
		</ul>
        </div>
      </div>