<?php 
	include("../adminHeader.php");
	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}
	$type		=	$_SESSION['LogType'];

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>
<script>
	function delete_type()
	{
		var del=confirm("Do you Want to Delete ?");
		if(del==true)
		{
			window.submit();
		}
		else
		{
			return false;
		}
	}
</script>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">News</h2> 
					 
				</div>
          </div>
          <div class="col-sm-4">
            <form method="post">
              <div class="input-group">
			  				<input type="text" class="form-control"  name="search" placeholder="Heading" value="<?php echo @$_REQUEST['search'] ?>">             
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> </div>
            </form>
          </div>
        </div>
 <?php	
$cond="1";
if(@$_REQUEST['search'])
{
	$cond=$cond." and ".TABLE_NEWS.".heading like'%".$_REQUEST['search']."%'";
}

?>
        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table id="rowspan" class="tablesorter table view_limitter pagination_table" >
                <thead>
                  <tr >
						<th>Sl No</th>               
						<th>Heading</th>
						<th>Date</th>	
						<th>User</th>	
						<th></th>																										
                  </tr>
                </thead>
                <tbody>
								<?php 															
										$selAllQuery="SELECT ".TABLE_NEWS.".ID,".TABLE_NEWS.".heading,".TABLE_NEWS.".content,".TABLE_NEWS.".date,".TABLE_NEWS.".photo_url,".TABLE_NEWS.".status,
										".TABLE_USER.".first_name,".TABLE_USER.".last_name,".TABLE_USER.".mobile,".TABLE_USER.".email
										 FROM ".TABLE_NEWS.", ".TABLE_USER." WHERE ".TABLE_NEWS.".status=0 and ".TABLE_NEWS.".user_id=".TABLE_USER.".ID and  $cond ORDER BY ".TABLE_NEWS.".ID ASC";
										$rowsPerPage = ROWS_PER_PAGE;						
										$selectAll= $db->query($selAllQuery);
										$number=mysql_num_rows($selectAll);
										if($number==0)
										{
										?>
											<tr>
												<td align="center" colspan="5">
													There is no News in list.
												</td>
											</tr>
										<?php
										}
										else
										{
											/*********************** for pagination ******************************/												
											if(isset($_GET['page']))
											{
												$pageNum = $_GET['page'];
											}
											else
											{
												$pageNum =1;
											}
											$offset = ($pageNum - 1) * $rowsPerPage;
											$select1=$db->query($selAllQuery." limit $offset, $rowsPerPage");
											$i=$offset+1;
											//use '$select1' for fetching
											/*************************** for pagination **************************/											
											while($row=mysql_fetch_array($select1))
											{
												$tableId=$row["ID"];	
																								
												?>
												<tr>
												<td><?php echo $i++;?>
															<div class="adno-dtls"> 
																		<a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a> |
																		<a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>"> VIEW</a>
															</div>	
																<!-- Modal3 -->
																	<div  class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
																	<div class="modal-dialog">
																		<div class="modal-content"> 																																					
																		<div role="tabpanel" class="tabarea2"> 
																			
																			<!-- Nav tabs -->
																		<ul class="nav nav-tabs" role="tablist">									  
																			<li role="presentation" class="active"> <a href="#personal<?php echo $tableId; ?>" aria-controls="personal" role="tab" data-toggle="tab">News Details</a> </li>										
																		</ul>
																			
																			<!-- Tab panes -->
																			<div class="tab-content">
																			<div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
																				<table class="table nobg">
																				<tbody style="background-color:#FFFFFF">
															
																					<tr>
																						<td>Heading</td>
																						<td> : </td>
																						<td><?php  echo $row['heading']; ?></td>
																					</tr>
																					<tr>
																						<td>Description</td>
																						<td> : </td>
																						<td><?php  echo $row['content']; ?></td>
																					</tr>
																					<tr>
																						<td>Date</td>
																						<td> : </td>
																						<td><?php  echo $App->dbformat_date_db($row['date']); ?></td>
																					</tr>
																					<tr>
																						<td>Photo</td>
																						<td> : </td>
																						<td><?php if($row['photo_url']){
																									$path="../../".$row['photo_url']; ?>
																									<div style="max-width: 100px;"><img src="<?php echo $path ?>" alt="No Image" class="img-responsive" /></div>
																									<?php } ?>
																						</td>
																					</tr>
																					<tr>
																						<td>User Name</td>
																						<td> : </td>
																						<td><?php echo $row['first_name']." ".$row['last_name']; ?></td>
																					</tr>
																					<tr>
																						<td>Mobile</td>
																						<td> : </td>
																						<td><?php echo $row['mobile']; ?></td>
																					</tr>
																					<tr>
																						<td>Email</td>
																						<td> : </td>
																						<td><?php echo $row['email']; ?></td>
																					</tr>																					
																																										
																				</tbody>
																				</table>
																			</div>
																													
																			</div>
																		</div>
																		</div>
																	</div>
																	</div>
																	<!-- Modal3 cls --> 																				  						
													</td>
													<td><?php echo $row['heading']; ?></td>
													<td><?php echo $App->dbformat_date_db($row['date']); ?></td>
													<td><?php  echo $row['first_name']." ".$row['last_name']; ?></td>														
													<td><a href="edit.php?id=<?php echo $tableId?>" class="viewbtn" >Approve</a>	</td>			                   	 	
												</tr>
										<?php }
									}?>                  
								</tbody>
							</table>
								
							</div>
								<!-- **********************************************************************  -->
									<?php 
										if($number>$rowsPerPage)
										{
										?>	
										<br />	
										<div style="text-align: center";>
											<div class="pagerSC" align="center" style="display: inline-block;">
										<?php
										
										$query   =  $db->query($selAllQuery);
										$numrows = mysql_num_rows($query);
										$maxPage = ceil($numrows/$rowsPerPage);
										$self = $_SERVER['PHP_SELF'];
										$nav  = '';
										if ($pageNum - 5 < 1) {
										$pagemin = 1;
										} else {
										$pagemin = $pageNum - 5;
										};
										if ($pageNum + 5 > $maxPage) {
										$pagemax = $maxPage;
										} else {
										$pagemax = $pageNum + 5;
										};
										
										$search=@$_REQUEST['search'];
										for($page = $pagemin; $page <= $pagemax; $page++)
										{
											if ($page == $pageNum)
											{
												$nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
											}
											else
											{
												if(@$search)
													{
													$nav .= " <a href=\"$self?page=$page&search=$search\">$page</a> ";
												}
												else
												{
													$nav .= " <a href=\"$self?page=$page\">$page</a> ";
												}
											}
										}
										?>
										<?php
										if ($pageNum > 1)
										{
											$page  = $pageNum - 1;
											if(@$search)
											{
												$prev  = " <a href=\"$self?page=$page&search=$search\">Prev</a> ";
												$first = " <a href=\"$self?page=1&search=$search\">First</a> ";
											}
											else
											{
												$prev  = " <a href=\"$self?page=$page\">Prev</a> ";
												$first = " <a href=\"$self?page=1\">First</a> ";
											}
										}
										else
										{
											$prev  = '&nbsp;';
											$first = '&nbsp;';
										}
										
										if ($pageNum < $maxPage)
										{
											$page = $pageNum + 1;
											if(@$search)
											{
													$next = " <a href=\"$self?page=$page&search=$search\">Next</a> ";
													$last = " <a href=\"$self?page=$maxPage&search=$search\">Last</a> ";
											}
											else
											{
													$next = " <a href=\"$self?page=$page\">Next</a> ";
													$last = " <a href=\"$self?page=$maxPage\">Last</a> ";
											}
										}
										else
										{
											$next = '&nbsp;';
											$last = '&nbsp;';
										}
										echo $first . $prev . $nav . $next . $last;
										?>
										<div style="clear: left;"></div>
										</div>	 
									</div>
									<?php
									}
													?>
											
										<!-- ******************************************************************-->
          </div>
        </div>
      </div>
      
     
    </div>
  </div>
<?php include("../adminFooter.php") ?>