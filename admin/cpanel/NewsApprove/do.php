<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	
		
	// EDIT SECTION
	//-
	case 'edit':
		
			$fid	=	$_REQUEST['fid'];	       
			if(!$_POST['heading'])
				{				
					$_SESSION['msg']="Error, Invalid Details!";					
					header("location:edit.php?id=$fid");	
				}
			else
				{				
					$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
					$db->connect();
					$success=0;
						
					$data['heading']			=	$App->convert($_REQUEST['heading']);
					$data['content']			=	$App->convert($_REQUEST['description']);
					$data['date']				=	$App->dbFormat_date($_REQUEST['newsDate']);
					if( isset($_REQUEST['breaking'])){
						$data['breaking']		=	1;
					}
					else{
						$data['breaking']		=	0;
					}
					$data['status']				=	1;	

					if($_FILES["photo"]["name"])
					{
						$file_name 		 				= 	pathinfo($_FILES["photo"]["name"]);
						$file_ext	 					=	$file_name["extension"];
						$rename 						= 	date("YmdHis");
						$new_file_name 					= 	$rename.".".$file_ext;
						
						if ($App->validateImages($file_ext)) {
							if (!file_exists('../../Images/news')) {
								mkdir('../../Images/news', 0777, true);
								copy('http://www.pickit3d.com/sites/default/files/default.jpg', '../../Images/news/default_image.jpg');
							}
							if(move_uploaded_file($_FILES["photo"]["tmp_name"],"../../Images/news/".basename($new_file_name))){
								$img="Images/news/" . $new_file_name;
								$data['photo_url']			=	$App->convert($img);

								//Delete Previous image
								$res=mysql_query("select photo_url from ".TABLE_NEWS." where ID='{$fid}'");
        						$row=mysql_fetch_array($res);
								$image = '../../'.$row['photo_url'];

							}
							else{
								$_SESSION['msg']="Opps...! Image upload failed";
								header("location:edit.php?id=$fid");	
								return;	
							} 
						}
						else{
							$_SESSION['msg']="Please upload a valid Image";
							header("location:edit.php?id=$fid");	
							return;	
						}
					}									
					$success=$db->query_update(TABLE_NEWS, $data, "ID='{$fid}'");					 
				//	$db->close(); 				
				    

					$qry = "DELETE FROM ".TABLE_NEWS_LOCATION." where news_id=$fid";
					$resultQ = $db->query($qry);

					$catArray 	=	$_REQUEST['locations'];	
								//echo $catArray ;
					if(!empty($catArray)) 
					{
						for($i=0;$i<count($catArray);$i++)
						{
							$data1['news_id']	=	$fid;
							$data1['location_id']=	$catArray[$i];						
							$db->query_insert(TABLE_NEWS_LOCATION,$data1);
						}										
					}	
					$db->close();	

					if($success)
					{
						if ($image) {
							if (file_exists($image)){
								unlink($image);
							}
						}
						$_SESSION['msg']="News approved successfully";																	
					}
					else
					{
						$_SESSION['msg']="Failed";									
					}	
					header("location:index.php");						
										
				}
			
			break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();		
						
				$res=mysql_query("select photo_url from ".TABLE_NEWS." where ID='{$id}'");
				$row=mysql_fetch_array($res);
				$photo="../../".$row['photo_url'];	
				if (file_exists($photo)) 	
				{
					unlink($photo);					
				} 


				$success= @mysql_query("DELETE FROM `".TABLE_NEWS_LOCATION."` WHERE news_id='{$id}'");
				$success= @mysql_query("DELETE FROM `".TABLE_NEWS."` WHERE ID='{$id}'");
				$db->close(); 
				//echo "DELETE FROM `".TABLE_NEWS."` WHERE ID='{$id}'";
				//die;
				if($success)
				{
					$_SESSION['msg']="News deleted successfully";									
				}
				else
				{
					$_SESSION['msg']="You can't Delete. ";									
				}	
				header("location:index.php");	
		break;	
}
?>