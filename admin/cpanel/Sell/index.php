<?php
include("../adminHeader.php");
if($_SESSION['LogID']=="")
{
    header("location:../../logout.php");
}
$type		=	$_SESSION['LogType'];

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
?>
<script>
    function delete_type()
    {
        var del=confirm("Do you Want to Delete ?");
        if(del==true)
        {
            window.submit();
        }
        else
        {
            return false;
        }
    }
</script>

<?php
if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }
$_SESSION['msg']='';
?>

<div class="col-md-10 col-sm-8 rightarea">
    <div class="row">
        <div class="col-sm-4">
            <div class="clearfix">
                <h2 class="q-title">Sell</h2>
                <a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD NEW</a>
            </div>
        </div>
        <div class="col-sm-8">
            <form method="GET">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="form-control"  name="product_name" placeholder="Product name" value="<?php echo @$_REQUEST['product_name'] ?>">
									</div>
								</div>
								<div class="col-md-6">
								<div class="input-group">
									<select name="category" id="category" class="form-control" required style="height:48px" >
										<option value="All">All</option>
									<?php
                                        $categoryTypes="select ID,category from ".TABLE_SELL_CATEGORY." order by ID desc";
	                                    $res2=mysql_query($categoryTypes);            
										while($row=mysql_fetch_array($res2))
										{?>	
											<option value="<?php echo $row['ID']?>"><?php echo $row['category']?></option>
										<?php 									
										}?>	            			
									</select>             
										<span class="input-group-btn">
										<button class="btn btn-default lens" type="submit"></button>
										</span> 
								</div>
								</div>
							</row>
            </form>
        </div>
    </div>
    <?php
    $cond="1";
    if(@$_REQUEST['category'])
    {
        if($_REQUEST['category']=="All")
        {
            $cond=$cond." and ".TABLE_SELL.".product_name like'%".$_REQUEST['product_name']."%'";
        }
        else
        {
            $cond=$cond." and ".TABLE_SELL.".product_name like'%".$_REQUEST['product_name']."%' AND ".TABLE_SELL.".category_id=".$_REQUEST['category']."";
        }
	
    }

    ?>
</div>
    <div class="row">
        <div class="col-sm-12">
            <div class="tablearea table-responsive">
                <table id="rowspan" class="tablesorter table view_limitter pagination_table" >
                    <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Category</th>
                        <th>Product Name</th>
                        <th>Prize</th>
                        <th>Contact</th>
                        <th>Image</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $selAllQuery="SELECT * FROM ".TABLE_SELL_CATEGORY.",".TABLE_SELL." WHERE ".TABLE_SELL_CATEGORY.".ID = ".TABLE_SELL.".category_id AND status = 1 and $cond ORDER BY ".TABLE_SELL.".ID ASC";

                    $selectAll= $db->query($selAllQuery);
                    $number=mysql_num_rows($selectAll);
                    $rowsPerPage = 0;
                    if($number==0)
                    {
                        ?>
                        <tr>
                            <td align="center" colspan="7">
                                There is no Item in the list.
                            </td>
                        </tr>
                        <?php
                    }
                    else
                    {
                        /*********************** for pagination ******************************/
                        $rowsPerPage = ROWS_PER_PAGE;
                        if(isset($_GET['page']))
                        {
                            $pageNum = $_GET['page'];
                        }
                        else
                        {
                            $pageNum =1;
                        }
                        $offset = ($pageNum - 1) * $rowsPerPage;
                        $select1=$db->query($selAllQuery." limit $offset, $rowsPerPage");
                        $i=$offset+1;
                        //use '$select1' for fetching
                        /*************************** for pagination **************************/
                        while($row=mysql_fetch_array($select1))
                        {
                            $tableId=$row['ID'];
                            $locQry = "SELECT ".TABLE_LOCATION.".location FROM ".TABLE_SELL_LOCATION."	LEFT JOIN ".TABLE_LOCATION." ON ".TABLE_LOCATION.".ID = ".TABLE_SELL_LOCATION.".location_id WHERE ".TABLE_SELL_LOCATION.".sell_id =$tableId";
                            $locResult = $db->query($locQry);
                            $locArray = array();
                            while($locRow = mysql_fetch_array($locResult))
                            {
                                array_push($locArray,$locRow['location']);
                            }
                            $locString	=	implode(",",$locArray);
                            $isUser = false;
                            if ($row['user_id']) {
                                $isUser = true;
                                $user_id = $row['user_id'];
                                $userRes = mysql_query("SELECT first_name, last_name, mobile, email FROM `".TABLE_USER."` WHERE ID = ".$user_id);
                                $userRow = mysql_fetch_array($userRes);
                            }
                            ?>
                            <tr>
                                <td><?php echo $i++;?>
                                    <div class="adno-dtls">
                                        <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> |
                                        <a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a> |
                                        <a href="#" data-toggle="modal" data-target="#event_view_<?= $tableId; ?>">VIEW</a>
                                    </div>

                                    <!-- Pop up -->
                                    <div  class="modal fade" id="event_view_<?= $tableId; ?>" tabindex="-1" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div role="tabpanel" class="tabarea2">

                                                    <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Item Details</h4>
            </div>

                                                    <!-- Tab panes -->
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
                                                            <table class="table nobg">
                                                                <tbody style="background-color:#FFFFFF">

                                                                <tr>
                                                                    <td>Category</td>
                                                                    <td> : </td>
                                                                    <td><?php  echo $row['category']; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Product name</td>
                                                                    <td> : </td>
                                                                    <td><?php  echo $row['product_name']; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Description</td>
                                                                    <td> : </td>
                                                                    <td><?php  echo $row['description']; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Expiry date</td>
                                                                    <td> : </td>
                                                                    <td><?php  echo $App->dbformat_date_db($row['exp_date']); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Contact</td>
                                                                    <td> : </td>
                                                                    <td><?php  echo $row['contact_number']; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>price</td>
                                                                    <td> : </td>
                                                                    <td> <?php  echo $row['price']; ?></td>
                                                                </tr>
                                                                <tr>
																    <td>Photo</td>
																    <td> : </td>
																    <td><?php if($row['image_url']){
																		$path="../../".$row['image_url']; ?>
																		<div style="max-width: 100px;"><img src="<?php echo $path ?>" alt="No Image" class="img-responsive" /></div>
																	<?php }else{?>
																	    <div style="max-width: 100px;"><img src="../../img/student.png" alt="No Image" class="img-responsive" /></div>
																		<?php } ?>
																	</td>
																</tr>
                                                                <?php
                                                                if (count($locArray) > 0) {
                                                                    ?>
                                                                    <tr>
                                                                        <td>Locations</td>
                                                                        <td> : </td>
                                                                        <td> <?= $locString; ?></td>
                                                                    </tr>
                                                                    <?php
                                                                }?>
                                                                </tbody>
                                                            </table>
                                                            <?php
                                                            if ($isUser) {
                                                                ?>
                                                                <h3>User Details</h3>
                                                                <table class="table nobg">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>Name</td>
                                                                        <td>:</td>
                                                                        <td><?= $userRow['first_name']. ' ' .$userRow['last_name']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Mobile</td>
                                                                        <td>:</td>
                                                                        <td><?= $userRow['mobile']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Email</td>
                                                                        <td>:</td>
                                                                        <td><?= $userRow['email']; ?></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Pop up -->
                                </td>
                                <td><?= $row['category']; ?></td>
                                <td><?= $row['product_name']; ?></td>
                                <td><?= $row['price']; ?></td>
                                <td><?= $row['contact_number']; ?></td>
                                <td><?php if($row['image_url']){
									$path="../../".$row['image_url']; ?>
									<div style="max-width: 100px;"><img src="<?php echo $path ?>" alt="No Image" class="img-responsive" /></div>
									<?php }else{?>
									<div style="max-width: 100px;"><img src="../../img/student.png" alt="No Image" class="img-responsive" /></div>
									<?php } ?>
								</td>
                                
                            </tr>
                        <?php }
                    }?>
                    </tbody>
                </table>

            </div>
            <!-- **********************************************************************  -->
            <?php
            if($number>$rowsPerPage)
            {
                ?>
                <br />
                <div style="text-align: center";>
                    <div class="pagerSC" align="center" style="display: inline-block;">
                        <?php

                        $query   =  $db->query($selAllQuery);
                        $numrows = mysql_num_rows($query);
                        $maxPage = ceil($numrows/$rowsPerPage);
                        $self = $_SERVER['PHP_SELF'];
                        $nav  = '';
                        if ($pageNum - 5 < 1) {
                            $pagemin = 1;
                        } else {
                            $pagemin = $pageNum - 5;
                        };
                        if ($pageNum + 5 > $maxPage) {
                            $pagemax = $maxPage;
                        } else {
                            $pagemax = $pageNum + 5;
                        };

                        $search=@$_REQUEST['search'];
                        for($page = $pagemin; $page <= $pagemax; $page++)
                        {
                            if ($page == $pageNum)
                            {
                                $nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
                            }
                            else
                            {
                                if(@$search)
                                {
                                    $nav .= " <a href=\"$self?page=$page&search=$search\">$page</a> ";
                                }
                                else
                                {
                                    $nav .= " <a href=\"$self?page=$page\">$page</a> ";
                                }
                            }
                        }
                        ?>
                        <?php
                        if ($pageNum > 1)
                        {
                            $page  = $pageNum - 1;
                            if(@$search)
                            {
                                $prev  = " <a href=\"$self?page=$page&search=$search\">Prev</a> ";
                                $first = " <a href=\"$self?page=1&search=$search\">First</a> ";
                            }
                            else
                            {
                                $prev  = " <a href=\"$self?page=$page\">Prev</a> ";
                                $first = " <a href=\"$self?page=1\">First</a> ";
                            }
                        }
                        else
                        {
                            $prev  = '&nbsp;';
                            $first = '&nbsp;';
                        }

                        if ($pageNum < $maxPage)
                        {
                            $page = $pageNum + 1;
                            if(@$search)
                            {
                                $next = " <a href=\"$self?page=$page&search=$search\">Next</a> ";
                                $last = " <a href=\"$self?page=$maxPage&search=$search\">Last</a> ";
                            }
                            else
                            {
                                $next = " <a href=\"$self?page=$page\">Next</a> ";
                                $last = " <a href=\"$self?page=$maxPage\">Last</a> ";
                            }
                        }
                        else
                        {
                            $next = '&nbsp;';
                            $last = '&nbsp;';
                        }
                        echo $first . $prev . $nav . $next . $last;
                        ?>
                        <div style="clear: left;"></div>
                    </div>
                </div>
                <?php
            }
            ?>

            <!-- ******************************************************************-->



        </div>
    </div>
</div>

<!-- Modal1 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Sell</h4>
            </div>
            <div class="modal-body clearfix">
                <form action="do.php?op=new"  class="form1" method="post" onsubmit="return valid()" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">Category:<span class="star">*</span></label>
                                <select name="category" id="category" class="form-control2" required >	
								<?php
								$res2=mysql_query($categoryTypes);            
								while($row=mysql_fetch_array($res2))
								{?>	
								    <option value="<?php echo $row['ID']?>"><?php echo $row['category']?></option>
							    <?php 									
								}
                                ?>	 									
								</select>	
                            </div>
                            
                            <div class="form-group">
                                <label for="venue">Product name:<span class="star">*</span></label>
                                <input type="text" id="product_name" name="product_name" class="form-control2" value=""  required placeholder="Product name" />
                            </div>
                            <div class="form-group">
                                <label for="description">Description:</label>
                                <textarea style="min-height: 100px;" id="description" name="description" placeholder="Description" class="form-control2"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="time_date">Contact:<span class="star">*</span></label>
                                <input type="text" id="contact_number" name="contact_number" class="form-control2" value=""  required placeholder="Contact number" />
                            </div>
                            <div class="form-group">
                                <label for="time_date">Expiry date:<span class="star">*</span></label>
                                <input type="text" id="exp_date" name="exp_date" class="form-control2 datepicker" value="<?php echo date("d/m/Y");?>"  required placeholder="Expiry date" />
                            </div>
                            <div class="form-group">
                                <label for="guest">Price:</label>
                                <input type="text" id="price" name="price" required class="form-control2 check_float" placeholder="Price" />
                            </div>
                            <div class="form-group">
                                <label for="place">Image</label>
                                <input type="file" id="image_url" name="image_url" class="form-control2">
                            </div>
							<div class="form-group">
							    <img id="thumb" width="100" height="100" src="../../img/student.png" alt="your image" />
							</div>
                            <div class="form-group">
                                <label for="Location">Location</label>
                                <ul class="category_combo_list list-unstyled" style="display: block;">
                                    <?php
                                    $selAllQuery="SELECT * FROM ".TABLE_LOCATION." ORDER BY location ASC";
                                    $selectAll= $db->query($selAllQuery);
                                    while($row=mysql_fetch_array($selectAll))
                                    { ?>
                                        <li><input type="checkbox" name="locations[]" <?php if($row['ID']==1){ ?>checked="true" <?php } ?> id="breaking" value="<?= $row['ID'];?>">
                                            <label><?= $row['location']; ?></label></li>
                                    <?php	}
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<!-- Detailed view for events -->
<!-- Modal1 -->
<div class="modal fade" id="event_view" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Events - Detailed View</h4>
            </div>
            <div class="modal-body clearfix">

            </div>
        </div>
    </div>
</div>
<?php include("../adminFooter.php") ?>
