<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
    header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
    // NEW SECTION
    case 'new':

        if(!$_POST['category'])
        {
            $_SESSION['msg']="Error, Invalid Details!";
            header("location:index.php");
        }
        else
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success=0;
            $imageValidity = 1;
            $data['category_id']			        =	$App->convert($_REQUEST['category']);
            $data['exp_date']                       =   $App->dbFormat_date($_REQUEST['exp_date']);
            $data['description']			        =	$App->convert($_REQUEST['description']);
            $data['contact_number']				    =	$App->convert($_REQUEST['contact_number']);
            $data['price']				            =	$App->convert($_REQUEST['price']);
            $data['product_name']				    =	$App->convert($_REQUEST['product_name']);
            $data['status']				            =	1;
            $data['user_id']			            =	0;

            $img                                    =   "";

            if ($_FILES['image_url']['name'] != "") 
            {
                $file_name 		 				    = 	pathinfo($_FILES["image_url"]["name"]);
				$file_ext	 					    =	$file_name["extension"];
				$allowd_ext 					    = 	array("jpeg","jpg","png");
				$rename 						    = 	date("YmdHis");
				$new_file_name 					    = 	$rename.".".$file_ext;
                if(in_array($file_ext,$allowd_ext) ) 	
				{
				    if(move_uploaded_file($_FILES["image_url"]["tmp_name"],"../../Images/sell/".basename($new_file_name)))
					{
					    $img="Images/sell/" . $new_file_name;
						$data['image_url']		    =	$App->convert($img);
					}
                    else
                    {
                        $_SESSION['msg']            =   "Error..!!";
                        header("location:index.php");
                        return;
                    }
				}
                else
                {
                     $_SESSION['msg']               =   "Please upload a valid image";
                     header("location:index.php");
                     return;
                }
            } 
            else 
            {
                $data['image_url']                  =    $img;
            }
            
            $success=$db->query_insert(TABLE_SELL, $data);
            $catArray 	                            =	$_REQUEST['locations'];
            if(!empty($catArray))
            {
                for($i=0;$i<count($catArray);$i++)
                {
                    $data1['sell_id']	            =	$success;
                    $data1['location_id']           =	$catArray[$i];
                    $db->query_insert(TABLE_SELL_LOCATION,$data1);
                }
            }
            $db->close();

            if($success)
            {
                $_SESSION['msg']="Item Added Successfully";
            }
            else
            {
                $_SESSION['msg']="Failed";
            }
            header("location:index.php");

        }
        break;

    // EDIT SECTION
    //-
    case 'edit':
        
        $fid	=	$_REQUEST['fid'];

        if(!$_POST['category'])
        {
            $_SESSION['msg']="Error, Invalid Details!";
            header("location:edit.php?id=".$fid);
        }
        else
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            
            $imgRes = mysql_query("SELECT image_url,status FROM `".TABLE_SELL."` WHERE ID = ".$fid);
            $imgRow = mysql_fetch_array($imgRes);
            $status = $imgRow['status'];
            $image_url = $imgRow['image_url'];
			
            $isImageUploaded =0;
            $success=0;

            $data['category_id']			        =	$App->convert($_REQUEST['category']);
            $data['description']			        =	$App->convert($_REQUEST['description']);
            $data['contact_number']				    =	$App->convert($_REQUEST['contact_number']);
            $data['exp_date']                       =   $App->dbFormat_date($_REQUEST['exp_date']);
            $data['price']				            =	$App->convert($_REQUEST['price']);
            $data['product_name']				    =	$App->convert($_REQUEST['product_name']);
            $data['status']				            =	$status;
            
            if ($_FILES['image_url']['name']) 
            {
                $file_name 		 				    = 	pathinfo($_FILES["image_url"]["name"]);
				$file_ext	 					    =	$file_name["extension"];
				$rename 						    = 	date("YmdHis");
				$new_file_name 					    = 	$rename.".".$file_ext;
                $isValidImage 						=	$App->validateImages($file_ext);
                
                if($isValidImage) 	
				{
				    if(move_uploaded_file($_FILES["image_url"]["tmp_name"],"../../Images/sell/".basename($new_file_name)))
					{
					    $img="Images/sell/" . $new_file_name;
						$data['image_url']		    =	$App->convert($img);
                        $isImageUploaded            =   1;
					}
                    else
                    {
                        $_SESSION['msg']            =   "Error..!!";
                        header("location:edit.php?id=".$fid);
                        return;
                    }
				}
                else
                {
                     $_SESSION['msg']               =   "Please upload a valid image";
                     header("location:edit.php?id=".$fid);
                     return;
                }
            }

            $success=$db->query_update(TABLE_SELL, $data, "ID='{$fid}'");
            $qry = "DELETE FROM ".TABLE_SELL_LOCATION." where sell_id=$fid";
            $resultQ = $db->query($qry);

            $catArray 	=	$_REQUEST['locations'];
                //echo $catArray ;
            if(!empty($catArray))
            {
                for($i=0;$i<count($catArray);$i++)
                {
                    $data1['sell_id']	=	$fid;
                    $data1['location_id']=	$catArray[$i];
                    $db->query_insert(TABLE_SELL_LOCATION,$data1);
                }
           }
           $db->close();

           if($success)
           {
                if ($image_url && $isImageUploaded) 
		        {
                	$image = '../../'.$image_url;
                    if (file_exists($image))
                    {
                       unlink($image);
                    }
                }
                $_SESSION['msg']="Details updated successfully";
           }
           else
           {
                $_SESSION['msg']="Failed";
           }
           if($status==0)
           {
                header("location:approve.php");
                return;    
           }
           header("location:index.php");

        }

        break;

    // DELETE SECTION
    //-
    case 'delete':

        $id	=	$_REQUEST['id'];
        $success=0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();

        $res=mysql_query("select image_url from ".TABLE_SELL." where ID='{$id}'");
        $row=mysql_fetch_array($res);
        $success= @mysql_query("DELETE FROM `".TABLE_SELL_LOCATION."` WHERE sell_id='{$id}'");
        $success= @mysql_query("DELETE FROM `".TABLE_SELL."` WHERE ID='{$id}'");

        $db->close();


        if($success)
        {
            $_SESSION['msg']="Item deleted successfully";
            if ($row['image_url']) {
                $image = '../../'.$row['image_url'];
                if (file_exists($image))
                {
                    unlink($image);
                }
        }
        }
        else
        {
            $_SESSION['msg']="You can't Delete. ";
        }
        header("location:index.php");
        break;
        
    case 'approve':
        if(!$_REQUEST['id'])
        {
            $_SESSION['msg']="Error, Invalid Details!";
            header("location:approve.php");
        } else {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $fid = $_REQUEST['id'];
            $success = 0;
            $data['status'] = 1;
            $success=$db->query_update(TABLE_SELL, $data, "ID='{$fid}'");
            if($success)
            {
                $_SESSION['msg']="Item approved successfully";
            }
            else
            {
                $_SESSION['msg']="Something went wrong!";
            }
            header("location:approve.php");
            break;
        }
}
?>