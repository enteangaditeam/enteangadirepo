<?php
include("../adminHeader.php");
if($_SESSION['LogID']=="")
{
    header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
?>


<?php
if(isset($_SESSION['msg']))
{?>
    <font color="red"><?php echo $_SESSION['msg']; ?></font><?php
}
$_SESSION['msg']='';
$editId=$_REQUEST['id'];
$tableEdit="SELECT * FROM ".TABLE_SELL." WHERE ID='$editId'";
$editField=mysql_query($tableEdit);
$editRow=mysql_fetch_array($editField);

//Location query
$qry = "SELECT location_id FROM ".TABLE_SELL_LOCATION."   WHERE sell_id =$editId";

$locResult = $db->query($qry);
$sLocation = array();
while($locFetch = mysql_fetch_array($locResult))
{
    array_push($sLocation,$locFetch['location_id']);
}
?>


<!-- Modal1 -->
<div >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a class="close" href="index.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title">Sell Item Details</h4>
            </div>
            <div class="modal-body clearfix">
                <form method="post" action="do.php?op=edit" class="form1" enctype="multipart/form-data" onsubmit="return valid()">
                    <input type="hidden" name="fid" id="fid" value="<?php echo $editId ?>">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                      		<label for="group">Category <span class="star">*</span></label>
                      		<select name="category" id="category" class="form-control2" required >
							<?php	
							    $categoryTypes="select ID,category from ".TABLE_SELL_CATEGORY."";
								$res2=mysql_query($categoryTypes);	
											
								while($row=mysql_fetch_array($res2))
								{?>	
								    <option value="<?php echo $row['ID']?>" <?php if($editRow['category_id']== $row['ID']){?> selected="selected"<?php }?>><?php echo $row['category']?></option>
								<?php 									
								}?>										
							</select>
                    	</div>

                            <div class="form-group">
                                <label for="venue">Product name:<span class="star">*</span></label>
                               <input type="text" id="product_name" name="product_name" value="<?= $editRow['product_name']; ?>" class="form-control2"  required placeholder="Product name" />
                            </div>
                            <div class="form-group">
                                <label for="description">Description:</label>
                                <textarea style="min-height: 100px;" id="description" name="description"   placeholder="Description" class="form-control2"><?= $editRow['description']; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="time_date">Contact:<span class="star">*</span></label>
                                <input type="text" id="contact_number" name="contact_number" class="form-control2" value="<?= $editRow['contact_number']; ?>"  required placeholder="Contact number" />
                            </div>
                            <div class="form-group">
                                <label for="time_date">Expiry Date:<span class="star">*</span></label>
                                <input type="text" id="exp_date" name="exp_date" class="form-control2 datepicker" value="<?= $App->dbFormat_date_db($editRow['exp_date']); ?>"  required placeholder="Expiry date" />
                            </div>
                            <div class="form-group">
                                <label for="guest">Price:<span class="star">*</span></label>
                                <input type="text" value="<?= $editRow['price']; ?>" id="price" required  name="price" class="form-control2 check_float" placeholder="Price" />
                            </div>
                            <div class="form-group">
                                <label for="place">Image</label>
                                <input type="file" id="image_url" name="image_url" class="form-control2">				
					  		    <span id="user-result"></span>
                            </div>
                            <div class="form-group">
							<?php 
							    if($editRow['image_url'])
								{
								    $path="../../".$editRow['image_url']; ?>
									<div style="max-width: 100px;"><img id="thumb" src="<?php echo $path ?>" alt="No Image" class="img-responsive" /></div>
								<?php }else{?>
								    <div style="max-width: 100px;"><img id="thumb" src="../../img/student.png" alt="No Image" class="img-responsive" /></div>
								<?php } ?>
							</div>
                            <div class="form-group">
                                <label for="Location">Location</label>
                                <ul class="category_combo_list list-unstyled" style="display: block;">
                                    <?php
                                    $selAllQuery="SELECT * FROM ".TABLE_LOCATION." ORDER BY location ASC";
                                    $selectAll= $db->query($selAllQuery);
                                    while($row=mysql_fetch_array($selectAll))
                                    { ?>
                                        <li><input type="checkbox" name="locations[]" value="<?= $row['ID'];?>" <?php if(in_array( $row['ID'],$sLocation)){ echo "checked" ;}?>>
                                            <label><?= $row['location']; ?></label></li>
                                    <?php	}
                                    ?>
                                </ul>
                            </div>

                        </div>

                        </div>
                    </div>
                    <div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal1 cls -->
</div>
<?php include("../adminFooter.php") ?>
