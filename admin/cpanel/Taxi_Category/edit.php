<?php 
	include("../adminHeader.php"); 
	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>


<?php
	if(isset($_SESSION['msg']))
	{?>
		<font color="red"><?php echo $_SESSION['msg']; ?></font><?php 
	}	
	$_SESSION['msg']='';
	$editId=$_REQUEST['id'];
	$tableEdit="SELECT * FROM ".TABLE_TAXI_CATEGORY." WHERE ID='$editId'";
	$editField=mysql_query($tableEdit);
	$editRow=mysql_fetch_array($editField);
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="index.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">Taxi Category </h4>
            </div>
            <div class="modal-body clearfix">
				<form method="post" action="do.php?op=edit" class="form1" enctype="multipart/form-data" onsubmit="return valid()">
				<input type="hidden" name="fid" id="fid" value="<?php echo $editId ?>">
			             
                <div class="row">
                 	<div class="col-sm-6">

											<div class="form-group">
                      <label for="Heading">Category<span class="star">*</span></label>
                      <input type="text" id="category" name="category" class="form-control2" value="<?php echo $editRow['category'];?>"  required placeholder="Category" title="Category" />				
					  					<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="Heading">Minimum charge<span class="star">*</span></label>
                      <input type="text" id="min_charge" name="min_charge" class="form-control2 check_float" value="<?php echo $editRow['min_charge'];?>"  required placeholder="0.00" title="Min Charge" />				
					  					<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="Heading">Kilometer Charge<span class="star">*</span></label>
                      <input type="text" id="kilometer_charge" name="kilometer_charge" class="form-control2 check_float" value="<?php echo $editRow['kilometer_charge'];?>"  required placeholder="0.00" title="kilometercharge" />				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
												<label for="image_url">Image </label>
												<input type="file" id="image_url" name="img" class="form-control2">
                    	</div>
											<div class="form-group">
											<?php 
											if($editRow['img'])
											{
												$path="../../".$editRow['img']; ?>
												<div style="max-width: 100px;"><img id="thumb" src="<?php echo $path ?>" alt="No Image" class="img-responsive" /></div>
											<?php }else{?>
											<div style="max-width: 100px;"><img src="../../img/student.png" alt="No Image" class="img-responsive" /></div>
											<?php } ?>
										</div>

					</div>					 																								
            	</div>			
	 			<div>
            	</div>
            	<div class="modal-footer">
              		<input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            	</div>
				</form>
          	</div>
        </div>
      </div>
      <!-- Modal1 cls -->            
  </div>
<?php include("../adminFooter.php") ?>
