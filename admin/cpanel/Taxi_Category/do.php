<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_POST['category'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");
			}
		else
			{							
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$category=$App->convert($_REQUEST['category']);
				$existId=$db->existValuesId(TABLE_TAXI_CATEGORY," category='$category'");
				if($existId>0)
				{					
					$_SESSION['msg']="Category already Exist !!";					
					header("location:index.php");				
				}
				else
				{		
					$data['category']			=	$App->convert($_REQUEST['category']);
					$data['min_charge']			=	$App->convert($_REQUEST['min_charge']);
					$data['kilometer_charge']	=	$App->convert($_REQUEST['kilometer_charge']);

					if($_FILES["img"]["name"])
					{
						$file_name 		 				= 	pathinfo($_FILES["img"]["name"]);
						$file_ext	 					=	$file_name["extension"];
						$rename 						= 	date("YmdHis");
						$new_file_name 					= 	$rename.".".$file_ext;
						
						if ($App->validateImages($file_ext)) {
                    		if (!file_exists('../../Images/taxi_cat')) {
								mkdir('../../Images/taxi_cat', 0777, true);
							}
							
							if(move_uploaded_file($_FILES["img"]["tmp_name"],"../../Images/taxi_cat/".basename($new_file_name))){
								$img="Images/taxi_cat/" . $new_file_name;
								$data['img']		=	$App->convert($img);
								$success=$db->query_insert(TABLE_TAXI_CATEGORY, $data);
							}
							else{
								$_SESSION['msg']="Opps...! Category creation failed";
								header("location:index.php");		
								return;	
							}
                		}
						else{
							$_SESSION['msg']="Invalid Image";
							header("location:index.php");		
							return;	
							//Invalid image
						}
					}
					//echo $success;
					$db->close();				
				
					if($success)
					{
						$_SESSION['msg']="Category added Successfully";										
					}
					else
					{
						$_SESSION['msg']="Failed";							
					}	
					header("location:index.php");		
				}
			}
		break;
		
	// EDIT SECTION
	//-
	case 'edit':
		
		$fid	=	$_REQUEST['fid'];	       
		if(!$_POST['category'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
					
				$category=$App->convert($_REQUEST['category']);
				$existId=$db->existValuesId(TABLE_TAXI_CATEGORY," category='$category' AND ID!='$fid'");

				if($existId>0)
				{					
					$_SESSION['msg']="Category already Exist !!";					
					header("location:edit.php?id=$fid");						
					break ;
				}
				else
				{					
					$data['category']			=	$App->convert($_REQUEST['category']);
					$data['min_charge']			=	$App->convert($_REQUEST['min_charge']);
					$data['kilometer_charge']	=	$App->convert($_REQUEST['kilometer_charge']);
					if($_FILES["img"]["name"]){
						$file_name 		 				= 	pathinfo($_FILES["img"]["name"]);
						$file_ext	 					=	$file_name["extension"];
						$rename 						= 	date("YmdHis");
						$new_file_name 					= 	$rename.".".$file_ext;

						if ($App->validateImages($file_ext)) {
							if (!file_exists('../../Images/taxi_cat')) {
								mkdir('../../Images/taxi_cat', 0777, true);
							}
							if(move_uploaded_file($_FILES["img"]["tmp_name"],"../../Images/taxi_cat/".basename($new_file_name))){
								$img="Images/taxi_cat/" . $new_file_name;
								$data['img']		=	$App->convert($img);

								//Delete Previous image
								$res=mysql_query("select img from ".TABLE_TAXI_CATEGORY." where ID='{$fid}'");
        						$row=mysql_fetch_array($res);
								$image = '../../'.$row['img'];
							}
							else{
								$_SESSION['msg']="Opps...! Category creation failed";
								header("location:index.php");		
								return;	
							}
						}
						else{
							$_SESSION['msg']="Invalid Image";
							header("location:index.php");		
							return;	
						}
					}

					$success=$db->query_update(TABLE_TAXI_CATEGORY, $data, "ID='{$fid}'");					 
					$db->close(); 				
				
					if($success)
					{
						if ($image) {
							if (file_exists($image)){
								unlink($image);
							}
						}
						$_SESSION['msg']="Category updated successfully";																	
					}
					else
					{
						$_SESSION['msg']="Failed";									
					}	
					header("location:index.php");	
				}
									
			}
		
		break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$res=mysql_query("select img from ".TABLE_TAXI_CATEGORY." where ID='{$id}'");
        		$row=mysql_fetch_array($res);
				$image = '../../'.$row['img'];
				try
				{			
					$success= @mysql_query("DELETE FROM `".TABLE_TAXI_CATEGORY."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}				
				$db->close(); 
				
				
				if($success)
				{
					if ($image) {
						if (file_exists($image)){
							unlink($image);
						}
					}
					$_SESSION['msg']="Category deleted successfully";									
				}
				else
				{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
				}	
				header("location:index.php");	
		break;	
}
?>