<?php 
	include("../adminHeader.php");

	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}
	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>
<script>
	function delete_type()
	{
		var del=confirm("Do you Want to Delete ?");
		if(del==true)
		{
		window.submit();
		}
		else
		{
		return false;
		}
	}
</script>

<?php
	if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
	$_SESSION['msg']='';
 ?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">APPROVE JOB</h2> 
				</div>
          </div>
          <div class="col-sm-4" >
            <form method="post">
              <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="Company Name / Job Title / Category" value="<?php echo @$_REQUEST['search'] ?>">
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> </div>
            </form>
          </div>
        </div>
		 <?php	
		$cond="1";
		if(@$_REQUEST['search'])
		{						
			$search		=	$_REQUEST['search'];
			$cond=$cond." and ((".TABLE_JOB.".company like'%".$search."%') or (".TABLE_JOB.".job like'%".$search."%') or (".TABLE_JOB_CATEGORY.".category like'%".$search."%'))";
		}
		
		?>
        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
<table id="rowspan" class="tablesorter table view_limitter pagination_table" >
                <thead>
                  	<tr>
						<th>Sl No</th>
						<th>Category</th>
						<th>company</th>
						<th>Job Title</th>
						<th>contact No</th>
                        <th>Action </th>																																							
					</tr>
                </thead>
                <tbody>
						<?php 																					
						$selAllQuery	=  "select ".TABLE_JOB.".ID,   ".TABLE_JOB.".company,
												   ".TABLE_JOB.".job,  ".TABLE_JOB.".description,
												   ".TABLE_JOB.".contact_number, ".TABLE_JOB.".email,
												   ".TABLE_JOB.".date,".TABLE_JOB.".image_url,".TABLE_JOB.".user_id,
												   ".TABLE_JOB_CATEGORY.".category																	 
											 from `".TABLE_JOB."`,`".TABLE_JOB_CATEGORY."`
										    where  `".TABLE_JOB."`.category_id	=	`".TABLE_JOB_CATEGORY."`.ID																				      
										      and ".TABLE_JOB.".status=0
										      and $cond										      
										 order by `".TABLE_JOB."`.ID desc";
										// echo $selAllQuery;

						


				  	 	$rowsPerPage = ROWS_PER_PAGE;
						$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="6">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
						/*********************** for pagination ******************************/
						
						if(isset($_GET['page']))
						{
							$pageNum = $_GET['page'];
						}
						else
						{
							$pageNum =1;
						}
					//	echo $rowsPerPage;
					
						$offset = ($pageNum - 1) * $rowsPerPage;
						$select1=$db->query($selAllQuery." limit $offset, $rowsPerPage");
						$i=$offset+1;
					//	echo $selAllQuery." limit $offset, $rowsPerPage";
						//use '$select1' for fetching
						/*************************** for pagination **************************/
						//$i=1;
						while($row=mysql_fetch_array($select1))
						{	
							$tableId=$row['ID'];								
							$qry = "SELECT ".TABLE_LOCATION.".location
													FROM ".TABLE_JOB_LOCATION."	LEFT JOIN ".TABLE_LOCATION." ON 	
												".TABLE_LOCATION.".ID = ".TABLE_JOB_LOCATION.".location_id 
													WHERE ".TABLE_JOB_LOCATION.".job_id =$tableId";
								
							$catResult = $db->query($qry);							
							$categoryNw="";
							while($catFetch = mysql_fetch_array($catResult))
							{																						
									$categoryNw=$categoryNw.", ".$catFetch['location'];
							}	
							$categoryNw = ltrim($categoryNw, ',');																			

							$user=$row['user_id'];	
							if($row['user_id'])		
							{				
								$resul= mysql_query("SELECT ".TABLE_USER.".first_name,".TABLE_USER.".last_name,".TABLE_USER.".mobile
														FROM ".TABLE_USER."	WHERE ".TABLE_USER.".ID =$user");
								$row10=mysql_fetch_array($resul);
							}									
						?>
                  <tr>
                    <td><?php echo $i;$i++;?>
                      <div class="adno-dtls"> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> |
					   <a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a>  |
					   <a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>"> VIEW</a> </div>
												<!-- Modal3 -->
												<div class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
												<div class="modal-dialog">
													<div class="modal-content"> 								
														<div role="tabpanel" class="tabarea2"> 
															
														<!-- Nav tabs -->
														<ul class="nav nav-tabs" role="tablist">									  
															<li role="presentation" class="active"> <a href="#personal<?php echo $tableId; ?>" aria-controls="personal" role="tab" data-toggle="tab">Job Details</a> </li>										
														</ul>
																											
														<!-- Tab panes -->
														<div class="tab-content">
														<div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
															<table class="table nobg" >
															<tbody style="background-color:#FFFFFF">
																<tr>
																	<td>Job Title</td>
																	<td>:</td>
																	<td><?php echo $row['job']; ?></td>
																</tr>											
																<tr>
																	<td>Category</td>
																	<td>:</td>
																	<td><?php echo $row['category']; ?></td>
																</tr>																
																<tr>
																	<td>Company</td>
																	<td>:</td>
																	<td><?php echo $row['company']; ?></td>
																</tr>																	
																<tr>
																	<td>Description</td>
																	<td>:</td>
																	<td><?php echo $row['description']; ?></td>
																</tr>	
																<tr>
																	<td>Contact No</td>
																	<td>:</td>
																	<td><?php echo $row['contact_number']; ?></td>
																</tr>																															
																<tr>
																	<td>Email</td>
																	<td>:</td>
																	<td><?php echo $row['email']; ?></td>
																</tr>
																<tr>
																	<td>Exp Date</td>
																	<td>:</td>
																	<td><?php echo $App->dbformat_date_db($row['date']); ?></td>
																</tr>
																<tr>
																	<td>Photo</td>
																	<td>:</td>
																	<td><?php if($row['image_url']){
																									$path="../../".$row['image_url']; ?>
																									<div style="max-width: 100px;"><img src="<?php echo $path ?>" alt="No Image" class="img-responsive" /></div>
																									<?php } ?>
																	</td>
																</tr>
																<?php if($row['user_id']!=null) { ?>
																					<tr>
																						<td>User Name</td>
																						<td> : </td>
																						<td><?php echo $row10['first_name']." ".$row10['last_name']; ?></td>
																					</tr>
																					<tr>
																						<td>Mobile</td>
																						<td> : </td>
																						<td><?php echo $row10['mobile']; ?></td>
																					</tr>	
																<?php } ?>
																<tr>
																	<td>Locations</td>
																	<td> : </td>
																	<td> <?php  echo $categoryNw ?></td>
																</tr>								  																													  										 
															</tbody>
															</table>
														</div>
																												
														</div>
													</div>

													</div>
												</div>
												</div>
												<!-- Modal3 cls --> 					  
													</td>                    					
												<td><?php echo $row['category']; ?></td>				
												<td><?php echo $row['company']; ?></td>
												<td><?php echo $row['job']; ?></td>
												<td><?php echo $row['contact_number']; ?></td>
                                                <td><a class="btn btn-success" href="do.php?id=<?php echo $tableId; ?>&op=approve">Approve</a></td>																							
											</tr>
											<?php }
									}
									?>                  
                </tbody>
              </table>			  			  
            </div>          
            <!--*****************************************************************-->
            
            	 <?php 
                  if($number>$rowsPerPage)
					{
					?>	
					<br />	
						<div class="pagerSC" align="center">
					<?php
					
					$query   =  $db->query($selAllQuery);
					$numrows = mysql_num_rows($query);
					$maxPage = ceil($numrows/$rowsPerPage);
					$self = $_SERVER['PHP_SELF'];
					$nav  = '';
					if ($pageNum - 5 < 1) {
					$pagemin = 1;
					} else {
					$pagemin = $pageNum - 5;
					};
					if ($pageNum + 5 > $maxPage) {
					$pagemax = $maxPage;
					} else {
					$pagemax = $pageNum + 5;
					};
					
					for($page = $pagemin; $page <= $pagemax; $page++)
					{
						if ($page == $pageNum)
						{
							$nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
						}
						else
						{
							if(@$search)
								{
								$nav .= " <a href=\"$self?page=$page&sname=$search\">$page</a> ";
							}
							else
							{
								$nav .= " <a href=\"$self?page=$page\">$page</a> ";
							}
						}
					}
					?>
					<?php
					if ($pageNum > 1)
					{
						$page  = $pageNum - 1;
						if(@$search)
						{
							$prev  = " <a href=\"$self?page=$page&sname=$search\">Prev</a> ";
							$first = " <a href=\"$self?page=1&sname=$search\">First Page</a> ";
						}
						else
						{
							$prev  = " <a href=\"$self?page=$page\">Prev</a> ";
							$first = " <a href=\"$self?page=1\">First Page</a> ";
						}
					}
					else
					{
						$prev  = '&nbsp;';
						$first = '&nbsp;';
					}
					
					if ($pageNum < $maxPage)
					{
						$page = $pageNum + 1;
						if(@$search)
						{
								$next = " <a href=\"$self?page=$page&sname=$search\">Next</a> ";
								$last = " <a href=\"$self?page=$maxPage&sname=$search\">Last Page</a> ";
						}
						else
						{
								$next = " <a href=\"$self?page=$page\">Next</a> ";
								$last = " <a href=\"$self?page=$maxPage\">Last Page</a> ";
						}
					}
					else
					{
						$next = '&nbsp;';
						$last = '&nbsp;';
					}
					echo $first . $prev . $nav . $next . $last;
					?>
					<div style="clear: left;"></div>
					</div>	 
				<?php
				}
                ?>
            
           <!-- ******************************************************************-->
            
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">JOB REGISTRATION</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-sm-6">
                                     
                    
                    <div class="form-group">
                      <label for="shopName">Job Title:<span class="valid">*</span></label>
                      <input type="text" class="form-control2" name="job" id="job" required>
                    </div>	
					<div class="form-group">
                      <label for="shopName">company:<span class="valid">*</span></label>
                      <input type="text" class="form-control2" name="company" id="company" required>
                    </div>
					<div class="form-group">
                      <label for="appTypeId">Job Category:<span class="valid">*</span></label>
                      <select class="form-control2" name="categoryId" id="categoryId" required>
                      	<option value="">Select</option>
                      	<?php
								$select2 = mysql_query("select * from ".TABLE_JOB_CATEGORY."");
								while($row2=mysql_fetch_array($select2))
								{
									?>
									<option value="<?php echo $row2['ID'];?>"><?php echo $row2['category'];?></option>
									<?php
									}
								?>
                      </select>
                    </div>								
					<div class="form-group">
						<label for="uAddress">Description:</label>
						<textarea name="description" id="description" class="form-control2"  style="height:80%"></textarea>
					</div>
					<div class="form-group">
						<label for="uPhone">Contact No:<span class="valid">*</span></label>	
						<input type="text" name="Phone" id="Phone" class="form-control2" required >
						<div id="uPhonediv" style="color:#FF6600; font-family:'Times New Roman', Times, serif"></div>
					</div>
					<div class="form-group">
						<label for="uPhone">Email:<span class="valid">*</span></label>	
						<input type="text" name="Email" id="Email" class="form-control2" required>
						<div id="uPhonediv" style="color:#FF6600; font-family:'Times New Roman', Times, serif"></div>
					</div>										                                      
					<div class="form-group">
						<label for="workHour">Exp. Date:</label>	
						<input type="text" name="expDate" id="expDate" class="form-control2" required readonly value="<?php echo date("d/m/Y");?>" >
					</div>
					<div class="form-group">
						<label for="workHour">Upload Image:</label>	
						<input type="file" name="photo" id="photo" >
					</div>
				</div>
				<div class="col-sm-6">			
							<div class="form-group">
								<label for="Location">Location<span class="star">*</span></label>
									<ul class="category_combo_list list-unstyled" style="display: block;">
										<?php 
												$selAllQuery="SELECT * FROM ".TABLE_LOCATION." ORDER BY location ASC";
												$selectAll= $db->query($selAllQuery);
												while($row=mysql_fetch_array($selectAll))
												{ ?>
													<li><input type="checkbox" name="locations[]" id="breaking" value="<?= $row['ID'];?>">  
														<label><?= $row['location']; ?></label></li>
										<?php	}	
									?>
									</ul>					  																                     
                    			</div>
							</div>				
              		</div>                                          
					<div>
					</div>
				<div class="modal-footer">
				<input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
				</div>
				</form>
		</div>
	</div>
</div>
      <!-- Modal1 cls --> 
</div>
<?php include("../adminFooter.php") ?>