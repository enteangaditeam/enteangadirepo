<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$loginType	=	$_SESSION['LogType'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['job'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success1=$success2=0;
				$isViewable                     = 0;
				if (isset($_POST['isViewable'])) {
					$isViewable                 =   1;
				}												
				
				$job		=	$App->convert($_REQUEST['job']);				
				$company	=	$App->convert($_REQUEST['company']);				
				
				$existId=$db->existValuesId(TABLE_JOB,"company='$company' and job='$job' ");
				if($existId>0)
				{
					$_SESSION['msg']="Job Is Already Exist";													
				}
				else
				{
					$data1['company']			=	$company;
					$data1['isViewable']					=	$App->convert($isViewable);
					$data1['job']				=	$job;
					$data1['description']		=	$App->convert($_REQUEST['description']);				
					$data1['contact_number']	=	$App->convert($_REQUEST['Phone']);
					$data1['email']				=	$App->convert($_REQUEST['Email']);
					$data1['date']				=	$App->dbFormat_date($_REQUEST['expDate']);
					$data1['category_id']		=	$App->convert($_REQUEST['categoryId']);

					$path_info1 = pathinfo($_FILES["photo"]["name"]);
					$ext1	 	=	$path_info1["extension"];
					$result 	= date("YmdHis");
					$newName 	= $result.".".$ext1;	
					$img 		="";
					
					
					if($_FILES["photo"]["name"]){
						$file_name 		 				= 	pathinfo($_FILES["photo"]["name"]);
						$file_ext	 					=	$file_name["extension"];
						$rename 						= 	date("YmdHis");
						$new_file_name 					= 	$rename.".".$file_ext;

						if ($App->validateImages($file_ext)) {
							if (!file_exists('../../Images/job')) {
								mkdir('../../Images/job', 0777, true);
								copy('http://www.pickit3d.com/sites/default/files/default.jpg', '../../Images/job/default_image.jpg');
							}
							if(move_uploaded_file($_FILES["photo"]["tmp_name"],"../../Images/job/".basename($new_file_name))){
								$img="Images/job/" . $new_file_name;		
							}
							else{
								$_SESSION['msg']="Opps...! Category creation failed";
								header("location:index.php");		
								return;	
							}
						}
						else{
							$_SESSION['msg']="Please upload a valid image";
							header("location:index.php");		
							return;
						}	
					}
					$data1['image_url']			=	$img;
					$data1['status']			=	1;
					
					$success	=	$db->query_insert(TABLE_JOB,$data1);	

					$catArray 	=	$_REQUEST['locations'];				
					if(!empty($catArray)) 
					{
						for($i=0;$i<count($catArray);$i++)
						{
							$data['job_id']	=	$success;
							$data['location_id']=	$catArray[$i];						
							$db->query_insert(TABLE_JOB_LOCATION,$data);
						}										
					}																		
					$db->close();
					
					if($success)
					{							
						$_SESSION['msg']="Job Details Added Successfully";											
					}
					else{
						$_SESSION['msg']="Failed";
					}										
				}	
				header("location:index.php");											
			}		
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id']; 
		$isImageUploaded=0;
		if(!$_REQUEST['job'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=".$editId);	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$imgRes = mysql_query("SELECT image_url,status FROM `".TABLE_JOB."` WHERE ID = ".$editId);
        		$imgRow = mysql_fetch_array($imgRes);
        		$image_url = $imgRow['image_url'];
				$status = $imgRow['status'];
				$success1=0;
				$isViewable                     = 0;
				if (isset($_POST['isViewable'])) {
					$isViewable                 =   1;
				}
				$job		=	$App->convert($_REQUEST['job']);				
				$company	=	$App->convert($_REQUEST['company']);
				
				$existId=$db->existValuesId(TABLE_JOB,"company='$company' and job='$job' and ID!='$editId'");
				if($existId>0)
				{
					$_SESSION['msg']="Job Is Already Exist";													
				}
				else
				{
					$data1['company']			=	$company;
					$data1['job']				=	$job;
					$data1['description']		=	$App->convert($_REQUEST['description']);				
					$data1['contact_number']	=	$App->convert($_REQUEST['Phone']);
					$data1['email']				=	$App->convert($_REQUEST['Email']);
					$data1['date']				=	$App->dbFormat_date($_REQUEST['expDate']);
					$data1['category_id']		=	$App->convert($_REQUEST['categoryId']);
					$data1['isViewable']		=	$App->convert($isViewable);

					//$path_info1 = pathinfo($_FILES["photo"]["name"]);	
					if($_FILES["photo"]["name"]){
						$file_name 		 				= 	pathinfo($_FILES["photo"]["name"]);
						$file_ext	 					=	$file_name["extension"];
						$rename 						= 	date("YmdHis");
						$new_file_name 					= 	$rename.".".$file_ext;

						if ($App->validateImages($file_ext)) {
							if (!file_exists('../../Images/job')) {
								mkdir('../../Images/job', 0777, true);
								copy('http://www.pickit3d.com/sites/default/files/default.jpg', '../../Images/job/default_image.jpg');
							}
							if(move_uploaded_file($_FILES["photo"]["tmp_name"],"../../Images/job/".basename($new_file_name))){
								$img="Images/job/" . $new_file_name;
								$data1['image_url']			=	$img;

								//Delete Previous image
								$res=mysql_query("select image_url from ".TABLE_JOB." where ID='{$editId}'");
								$row=mysql_fetch_array($res);
								$image = '../../'.$row['image_url'];		
							}
							else{
								$_SESSION['msg']="Opps...! Category updation failed";
								header("location:index.php");		
								return;	
							}
						}
						else{
							$_SESSION['msg']="Please upload a valid image";
							header("location:index.php");		
							return;
						}	
					}
					$data1['status']			=	$status;
					
					$success1=$db->query_update(TABLE_JOB,$data1," ID='{$editId}'");
					$qry = "DELETE FROM ".TABLE_JOB_LOCATION." where job_id=$editId";
					$resultQ = $db->query($qry);
					$catArray 	=	$_REQUEST['locations'];				
					
					if(!empty($catArray)) 
					{
						for($i=0;$i<count($catArray);$i++)
						{
							$data['job_id']	=	$editId;
							$data['location_id']=	$catArray[$i];						
							$db->query_insert(TABLE_JOB_LOCATION,$data);
						}										
					}					
					$db->close();
					
					if($success1)
					{
						if ($image) {
							if (file_exists($image)){
								unlink($image);
							}
						}						
						$_SESSION['msg']="Job Details Updated Successfully";				
					}
					else{
						$_SESSION['msg']="Failed";
					}
				}
				header("location:index.php");																
			}	
			break;	
				
	// DELETE SECTION
	case 'delete':		
				$deleteId	=	$_REQUEST['id'];
				$success1	=	$success2	=	0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
								
				try
				{ 	
					$res=mysql_query("select image_url from ".TABLE_JOB." where ID='{$deleteId}'");
					while($row=mysql_fetch_array($res))											
					{		
						$photo="../../".$row['image_url'];																																									
						if (file_exists($photo)) 	
						{
							unlink($photo);					
						} 									
					}	

					$success1	= @mysql_query("DELETE FROM `".TABLE_JOB_LOCATION."` WHERE job_id='{$deleteId}'");
					$success1	= @mysql_query("DELETE FROM `".TABLE_JOB."` WHERE ID='{$deleteId}'");	
								      
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete this";				            
				}											
				$db->close(); 
				if($success1)
				{
					$_SESSION['msg']="Job details deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete this";										
				}	
				header("location:index.php");					
		break;

	// PHOTO UPLOAD
	case 'photo':				
			    	
			if(!$_REQUEST['shopId'])
				{
					$_SESSION['msg']="Error, Invalid Details!";					
					header("location:index.php");	
				}
			else
				{				
					$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
					$db->connect();
					$success=0;
													
					$shopId		=	$App->convert($_REQUEST['shopId']);				
					
					// upload file
					$date		= 	date("YmdHis");
					$path_info  = 	pathinfo($_FILES["photo"]["name"]);
					$ext	 	=	$path_info["extension"];	
					$allowed 	=  	array('bmp','jpg');		// allowed image extensions					
					$newName 	= 	$date.".".$ext;

					if(in_array($ext,$allowed) ) 	
					{
						$img='';	
						if(move_uploaded_file($_FILES["photo"]["tmp_name"],"../../images/shop/".basename($newName)))
						{
							$img="images/shop/" . $newName;		
						}
						$data['shop_id']			=	$shopId;		
						$data['image_url']			=	$App->convert($img);						
						
						$success1=$db->query_insert(TABLE_SHOP_IMAGE,$data);											
						$db->close();								
					}
					
					if($success1)
					{							
						$_SESSION['msg']="Shop Photo Added Successfully";										
					}
					else
					{
						if(!in_array($ext,$allowed) ) 
						{
							$_SESSION['msg']="You have uploaded an invalid image file";
						}
						else
						{
							$_SESSION['msg']="Failed";	
						}						
					}
					header("location:index.php");
				}
																					
			break;	
// DELETE SHOP PHOTO
	case 'delPhoto':		
				$deleteId	=	$_REQUEST['deleteId'];
				$shopId		=	$_REQUEST['sid'];
				$success1	=	0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
								
				try
				{ 										
					$res=mysql_query("select image_url from ".TABLE_SHOP_IMAGE." where ID='{$deleteId}'");
					$row=mysql_fetch_array($res);
					$photo="../../".$row['image_url'];	
					$success1= @mysql_query("DELETE FROM `".TABLE_SHOP_IMAGE."` WHERE ID='{$deleteId}'");
					if($success1)					
					{																																	
						if (file_exists($photo)) 	
						{
						unlink($photo);					
						} 						
					}														     
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete this";				            
				}											
				$db->close(); 
				if($success1)
				{
					$_SESSION['msg']="Shop Photo deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete this";										
				}	
				header("location:index.php");					
		break;
		case 'approve':
        
		if(!$_REQUEST['id'])
        {
            $_SESSION['msg']="Error, Invalid Details!";
            header("location:approve.php");
        } 
		else 
		{
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $fid = $_REQUEST['id'];
            $success = 0;
            $data['status'] = 1;
            $success=$db->query_update(TABLE_JOB, $data, "ID='{$fid}'");
            if($success)
            {
                $_SESSION['msg']="Job approved successfully";
            }
            else
            {
                $_SESSION['msg']="Something went wrong!";
            }
            header("location:approve.php");
            break;
        }						
}
?>