<?php 
	include("../adminHeader.php"); 
	if($_SESSION['LogID']=="")
	{
	header("location:../../logout.php");
	}

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>
<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	$editId=$_REQUEST['id'];
	$tableEditQry	=   "select ".TABLE_JOB.".ID,   ".TABLE_JOB.".company,".TABLE_JOB.".isViewable,
												   ".TABLE_JOB.".job,  ".TABLE_JOB.".description,
												   ".TABLE_JOB.".contact_number, ".TABLE_JOB.".email,
												   ".TABLE_JOB.".date,".TABLE_JOB.".image_url,".TABLE_JOB.".category_id												   																 
											 from `".TABLE_JOB."`
										    where  ".TABLE_JOB.".ID=$editId	";			
//	echo $tableEditQry;
	$tableEdit 	=	mysql_query($tableEditQry);
	$editRow	=	mysql_fetch_array($tableEdit);

	//Location query 
	$qry = "SELECT location_id FROM ".TABLE_JOB_LOCATION."  WHERE job_id =$editId";								
	$locResult = $db->query($qry);
	$sLocation = array();
	while($locFetch = mysql_fetch_array($locResult))
	{	 		
	 		array_push($sLocation,$locFetch['location_id']);
	}	
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="index.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">JOB DETAILS </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=edit" class="form1" method="post" enctype="multipart/form-data" onsubmit="return valid()">
			  <input type="hidden" name="id" id="id" value="<?php echo $editId ?>">			  
                <div class="row">
                  <div class="col-sm-6">                                                                        


					 			<div class="form-group">
                      <label for="shopName">Job Title:<span class="valid">*</span></label>
                      <input type="text" class="form-control2" name="job" id="job" required value="<?php echo $editRow['job'];?>">
                    </div>	
								<div class="form-group">
                      <label for="shopName">company:<span class="valid">*</span></label>
                      <input type="text" class="form-control2" name="company" id="company" required value="<?php echo $editRow['company'];?>">
                    </div>
					
								<div class="form-group">
                      <label for="appTypeId">Category:<span class="valid">*</span></label>
                      <select class="form-control2" name="categoryId" id="categoryId" required>
                      	<option value="">Select</option>
                      	<?php
													$select2 = mysql_query("select * from ".TABLE_JOB_CATEGORY."");
													while($row2=mysql_fetch_array($select2))
													{
														?>
														<option value="<?php echo $row2['ID'];?>" <?php if($row2['ID']==$editRow['category_id']){ echo 'selected' ;}?>><?php echo $row2['category'];?></option>
														<?php
														}
													?>
                      </select>
                    </div>								
									<div class="form-group">
										<label for="uAddress">Description:</label>
										<textarea name="description" id="description" class="form-control2"  style="height:80%"><?php echo $editRow['description'];?></textarea>
									</div>
									<div class="form-group">
										<label for="uPhone">Contact No:<span class="valid">*</span></label>	
										<input type="text" name="Phone" id="mobile" class="form-control2" required value="<?php echo $editRow['contact_number'];?>">
										<div id="uPhonediv" style="color:#FF6600; font-family:'Times New Roman', Times, serif"></div>
									</div>
									<div class="form-group">
										<label for="uPhone">Email:<span class="valid">*</span></label>	
										<input type="email" name="Email" id="Email" class="form-control2" required value="<?php echo $editRow['email'];?>">
										<div id="uPhonediv" style="color:#FF6600; font-family:'Times New Roman', Times, serif"></div>
									</div>										                                      
									<div class="form-group">
										<label for="workHour">Exp. Date:</label>											
										<input type="text" id="expDate" name="expDate" class="form-control2 datepicker" value="<?php echo $App->dbformat_date_db($editRow['date']);?>" required readonly >	
									</div>
									<div class="form-group">
										<label for="workHour">Upload Image:</label>	
										<input type="file" name="photo" id="image_url" >
									</div>
									<div class="form-group">
                                <input type="checkbox" <?php if($editRow['isViewable']){ echo "checked" ;}?> name="isViewable" id="isViewable">
                                <label for="contact_no">Show in Home</label>
                            </div>
									<div class="form-group">
											<?php 
											if($editRow['image_url'])
											{
												$path="../../".$editRow['image_url']; ?>
												<div style="max-width: 255px;"><img src="<?php echo $path ?>" alt="No Image" id="thumb" class="img-responsive" /></div>
											<?php }else{?>
											<div style="max-width: 255px;"><img src="../../img/student.png" alt="No Image" id="thumb"  class="img-responsive" /></div>
											<?php } ?>
										</div>
				</div>
				<div class="col-sm-6">			
						<div class="form-group">
							<label for="Location">Location<span class="star"></span></label>
								<ul class="category_combo_list list-unstyled" style="display: block;">
									<?php 
										$selAllQuery="SELECT * FROM ".TABLE_LOCATION." ORDER BY location ASC";
										$selectAll= $db->query($selAllQuery);
										while($row=mysql_fetch_array($selectAll))
										{ ?>
											<li><input type="checkbox" name="locations[]" id="locations" value="<?= $row['ID'];?>" <?php if(in_array( $row['ID'],$sLocation)){ echo "checked" ;}?>>  
											<label><?= $row['location']; ?></label></li>
										<?php	}	
								?>
								</ul>					  																                     
                    	</div>
					</div>									
															
             </div>                 
             </div>              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>
