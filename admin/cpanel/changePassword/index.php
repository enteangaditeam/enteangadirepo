<?php
require('../adminHeader.php');

if(@isset($_SESSION['msg'])){
	echo '<font color="red">'.$_SESSION['msg'].'</font>';
}
unset($_SESSION['msg']);

?>
<script>
function valid()
{

flag=false;

jPassword=document.getElementById('newPassword').value;
jConfirmPassword=document.getElementById('conPassword').value;		
	if(jConfirmPassword=="" || jConfirmPassword!=jPassword)
	{																			///for password
	document.getElementById('pwddiv').innerHTML="Password And Confirm Password are Not Equal";
	flag=true;
	}

if(flag==true)
	{
		return false;
	}

}

function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}

</script>
<div class="row" style="margin: 0px;">
	<div class="col-lg-3 col-md-3 col-sm-3">
		<div class="bd_panel bd_panel_default bd_panel_shadow">
			<form method="post" action="do.php?op=index" class="form1" onsubmit=" return valid()">
				<div class="bd_panel_head">
					<h3>Change Password</h3>
				</div>
				<div class="bd_panel_body">
					<div class="form-group">
						<label>Current User name</label>
						<input type="text" autocomplete="off" name="oldUserName" id="oldUserName" required="" class="form-control2">
					</div>
					<div class="form-group">
						<label>Current Password</label>
						<input autocomplete="new-password" type="password" name="oldPassword" id="oldPassword" required="" class="form-control2">
					</div>
					<div class="form-group">
						<label>New Password</label>
						<input type="password" name="newPassword" id="newPassword" required="" class="form-control2" class="form-control2">
					</div>
					<div class="form-group">
						<label>Confirm Password</label>
						<input type="password" name="conPassword" id="conPassword" required="" onfocus="clearbox('pwddiv')" class="form-control2">
					</div>
				</div>
				<div class="bd_panel_footer">
					<div class="panel_row">
						<div class="panel_col_full">
							<input type="submit" class="btn btn-primary continuebtn" name="form" value="SAVE">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
require('../adminFooter.php');
?>
