<?php 
	include("../adminHeader.php");
	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}
	$type		=	$_SESSION['LogType'];

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>
<script>
	function delete_type()
	{
		var del=confirm("Do you Want to Delete ?");
		if(del==true)
		{
			window.submit();
		}
		else
		{
			return false;
		}
	}
</script>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">BLOOD</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD NEW</a> 
				</div>
          </div>
          <div class="col-sm-4">
            <form method="post">
              <div class="input-group">
							<select name="group1" id="group1" class="form-control" required style="height:48px" >							
							<option value="All" <?php if(@$_REQUEST['group1']== "All"){?> selected="selected"<?php }?>>All</option>
                            <option value="A+" <?php if(@$_REQUEST['group1']== "A+"){?> selected="selected"<?php }?>>A+</option>
                            <option value="A-" <?php if(@$_REQUEST['group1']== "A-"){?> selected="selected"<?php }?>>A-</option>
                            <option value="AB+" <?php if(@$_REQUEST['group1']== "AB+"){?> selected="selected"<?php }?>>AB+</option>
                            <option value="AB-" <?php if(@$_REQUEST['group1']== "AB-"){?> selected="selected"<?php }?>>AB-</option>
                            <option value="B+" <?php if(@$_REQUEST['group1']== "B+"){?> selected="selected"<?php }?>>B+</option>										
                            <option value="B-" <?php if(@$_REQUEST['group1']== "B-"){?> selected="selected"<?php }?>>B-</option>										
                            <option value="O+" <?php if(@$_REQUEST['group1']== "O+"){?> selected="selected"<?php }?>>O+</option>										
                            <option value="O-" <?php if(@$_REQUEST['group1']== "O-"){?> selected="selected"<?php }?>>O-</option>										
						</select>             
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> </div>
            </form>
          </div>
        </div>
 <?php	
$cond="1";
if(@$_REQUEST['group1'])
{
	if($_POST['group1']!="All")
	{
		$cond=$cond." and ".TABLE_BLOOD_BANK.".blood_group like'%".$_POST['group1']."%'";
	}
	
}

?>
        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
             <table id="rowspan" class="tablesorter table view_limitter pagination_table " >
                <thead>
                  <tr >
										<th>Sl No</th>               
										<th>Group</th>
										<th>Name</th>
										<th>Mobile</th>	
										<th>Place</th>																											
                  </tr>
                </thead>
                <tbody>
								<?php
										$rowsPerPage = 0; 															
										$selAllQuery="SELECT * FROM ".TABLE_BLOOD_BANK." WHERE $cond ORDER BY ID ASC";
																
										$selectAll= $db->query($selAllQuery);
										$number=mysql_num_rows($selectAll);
										if($number==0)
										{
										?>
											<tr>
												<td align="center" colspan="5">
													There is no details in list.
												</td>
											</tr>
										<?php
										}
										else
										{
											/*********************** for pagination ******************************/
												$rowsPerPage = ROWS_PER_PAGE;
												if(isset($_GET['page']))
												{
													$pageNum = $_GET['page'];
												}
												else
												{
													$pageNum =1;
												}
												$offset = ($pageNum - 1) * $rowsPerPage;
												$select1=$db->query($selAllQuery." limit $offset, $rowsPerPage");
												$i=$offset+1;
												//use '$select1' for fetching
												/*************************** for pagination **************************/			


											//$i=1;
											while($row=mysql_fetch_array($select1))
											{	
												$tableId=$row['ID'];	?>
												<tr>
												<td><?php echo $i++;?>
															<div class="adno-dtls"> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> |
																		<a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a>
															</div>																					  						
													</td>
													<td><?php echo $row['blood_group']; ?></td>
													<td><?php echo $row['name']; ?></td>
													<td><?php echo $row['mobile']; ?></td>
													<td><?php echo $row['place']; ?></td>					                   	 	
												</tr>
										<?php }
									}?>                  
                </tbody>
              </table>
			  	
            </div>
						<?php 
										if($number>$rowsPerPage)
										{
										?>	
										<br />	
										<div style="text-align: center";>
											<div class="pagerSC" align="center" style="display: inline-block;">
										<?php
										
										$query   =  $db->query($selAllQuery);
										$numrows = mysql_num_rows($query);
										$maxPage = ceil($numrows/$rowsPerPage);
										$self = $_SERVER['PHP_SELF'];
										$nav  = '';
										if ($pageNum - 5 < 1) {
										$pagemin = 1;
										} else {
										$pagemin = $pageNum - 5;
										};
										if ($pageNum + 5 > $maxPage) {
										$pagemax = $maxPage;
										} else {
										$pagemax = $pageNum + 5;
										};
										
										$search=@$_REQUEST['search'];
										for($page = $pagemin; $page <= $pagemax; $page++)
										{
											if ($page == $pageNum)
											{
												$nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
											}
											else
											{
												if(@$search)
													{
													$nav .= " <a href=\"$self?page=$page&search=$search\">$page</a> ";
												}
												else
												{
													$nav .= " <a href=\"$self?page=$page\">$page</a> ";
												}
											}
										}
										?>
										<?php
										if ($pageNum > 1)
										{
											$page  = $pageNum - 1;
											if(@$search)
											{
												$prev  = " <a href=\"$self?page=$page&search=$search\">Prev</a> ";
												$first = " <a href=\"$self?page=1&search=$search\">First</a> ";
											}
											else
											{
												$prev  = " <a href=\"$self?page=$page\">Prev</a> ";
												$first = " <a href=\"$self?page=1\">First</a> ";
											}
										}
										else
										{
											$prev  = '&nbsp;';
											$first = '&nbsp;';
										}
										
										if ($pageNum < $maxPage)
										{
											$page = $pageNum + 1;
											if(@$search)
											{
													$next = " <a href=\"$self?page=$page&search=$search\">Next</a> ";
													$last = " <a href=\"$self?page=$maxPage&search=$search\">Last</a> ";
											}
											else
											{
													$next = " <a href=\"$self?page=$page\">Next</a> ";
													$last = " <a href=\"$self?page=$maxPage\">Last</a> ";
											}
										}
										else
										{
											$next = '&nbsp;';
											$last = '&nbsp;';
										}
										echo $first . $prev . $nav . $next . $last;
										?>
										<div style="clear: left;"></div>
										</div>	 
									</div>
									<?php
									}
									?>

			<!-- paging -->		
				<div style="clear:both;"></div>
				<div class="text-center">
					<div class="btn-group pager_selector"></div>
				</div>        
				<!-- paging end-->
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">BLOOD </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()">
                <div class="row">
                  <div class="col-sm-6">

                    <div class="form-group">
                      <label for="courseName">Group <span class="star">*</span></label>
                      <select name="group" id="group" class="form-control2" required >
												<option value="A+">A+</option>
                            <option value="A-">A-</option>
                            <option value="AB+">AB+</option>
                            <option value="AB-">AB-</option>
                            <option value="B+">B+</option>										
                            <option value="B-">B-</option>										
                            <option value="O+">O+</option>										
                            <option value="O-">O-</option>										
												</select>				
					  					<span id="user-result"></span>
                    </div>	

										<div class="form-group">
                      <label for="name">Name<span class="star">*</span></label>
                      <input type="text" id="name" name="name" placeholder="Name" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="name">Mobile<span class="star">*</span></label>
                      <input type="text" id="mobile" name="mobile" placeholder="Mobile" class="form-control2">				
					  					<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="place">Place<span class="star">*</span></label>
                      <input type="text" id="place" name="place" placeholder="Place" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>

									</div>
								</div>
			  				<div>
            		</div>
            		<div class="modal-footer">
              		<input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            		</div>
							</form>
          	</div>
        	</div>
      	</div>
	  	</div>
<?php include("../adminFooter.php") ?>
