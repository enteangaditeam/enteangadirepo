<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_POST['group'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");
			}
		else
			{							
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$mobile=$App->convert($_REQUEST['mobile']);
				$existId=$db->existValuesId(TABLE_BLOOD_BANK," mobile='$mobile'");
				if($existId>0)
				{					
					$_SESSION['msg']="Details already Exist !!";					
					header("location:index.php");				
				}
				else
				{		
					$data['blood_group']	=	$App->convert($_REQUEST['group']);
					$data['name']			=	$App->convert($_REQUEST['name']);
					$data['place']			=	$App->convert($_REQUEST['place']);
					$data['mobile']			=	$App->convert($_REQUEST['mobile']);
					//Status is hard coded,Coz feature is not implimented now
					$data['status']			=	"Available";//$App->convert($_REQUEST['status']);
					
					$success=$db->query_insert(TABLE_BLOOD_BANK, $data);
					
					$db->close();				
				
					if($success)
					{
						$_SESSION['msg']="Blood Added Successfully";										
					}
					else
					{
						$_SESSION['msg']="Failed";							
					}	
					header("location:index.php");		
				}
			}
		break;
		
	// EDIT SECTION
	//-
	case 'edit':
		
		$fid	=	$_REQUEST['fid'];	       
		if(!$_POST['group'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
					
				$mobile=$App->convert($_REQUEST['mobile']);
				$existId=$db->existValuesId(TABLE_BLOOD_BANK," mobile='$mobile' AND ID!='$fid'");

				if($existId>0)
				{					
					$_SESSION['msg']="Details already Exist !!";					
					header("location:edit.php?id=$fid");						
					break ;
				}
				else
				{					
					$data['blood_group']	=	$App->convert($_REQUEST['group']);
					$data['name']			=	$App->convert($_REQUEST['name']);
					$data['place']			=	$App->convert($_REQUEST['place']);
					$data['mobile']			=	$App->convert($_REQUEST['mobile']);
					//Status is hard coded,Coz feature is not implimented now
					$data['status']			=	"Available";//$App->convert($_REQUEST['status']);			

					$success=$db->query_update(TABLE_BLOOD_BANK, $data, "ID='{$fid}'");					 
					$db->close(); 				
				
					if($success)
					{
						$_SESSION['msg']="Details updated successfully";																	
					}
					else
					{
						$_SESSION['msg']="Failed";									
					}	
					header("location:index.php");	
				}
									
			}
		
		break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				try
				{			
					$success= @mysql_query("DELETE FROM `".TABLE_BLOOD_BANK."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}				
				$db->close(); 
				
				
				if($success)
				{
					$_SESSION['msg']="Blood deleted successfully";									
				}
				else
				{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
				}	
				header("location:index.php");	
		break;	
}
?>