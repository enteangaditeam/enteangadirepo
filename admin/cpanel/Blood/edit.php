<?php 
	include("../adminHeader.php"); 
	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>


<?php
	if(isset($_SESSION['msg']))
	{?>
		<font color="red"><?php echo $_SESSION['msg']; ?></font><?php 
	}	
	$_SESSION['msg']='';
	$editId=$_REQUEST['id'];
	$tableEdit="SELECT * FROM ".TABLE_BLOOD_BANK." WHERE ID='$editId'";
	$editField=mysql_query($tableEdit);
	$editRow=mysql_fetch_array($editField);
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="index.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">Blood </h4>
            </div>
            <div class="modal-body clearfix">
				<form method="post" action="do.php?op=edit" class="form1" enctype="multipart/form-data" onsubmit="return valid()">
				<input type="hidden" name="fid" id="fid" value="<?php echo $editId ?>">
			             
                <div class="row">
                 	<div class="col-sm-6">
                    	<div class="form-group">
                      		<label for="group">Group <span class="star">*</span></label>
                      		<select name="group" id="group" class="form-control2" required >							
														<option value="A+" <?php if($editRow['blood_group']== "A+"){?> selected="selected"<?php }?>>A+</option>
                            <option value="A-" <?php if($editRow['blood_group']== "A-"){?> selected="selected"<?php }?>>A-</option>
                            <option value="AB+" <?php if($editRow['blood_group']== "AB+"){?> selected="selected"<?php }?>>AB+</option>
                            <option value="AB-" <?php if($editRow['blood_group']== "AB-"){?> selected="selected"<?php }?>>AB-</option>
                            <option value="B+" <?php if($editRow['blood_group']== "B+"){?> selected="selected"<?php }?>>B+</option>										
                            <option value="B-" <?php if($editRow['blood_group']== "B-"){?> selected="selected"<?php }?>>B-</option>										
                            <option value="O+" <?php if($editRow['blood_group']== "O+"){?> selected="selected"<?php }?>>O+</option>										
                            <option value="O-" <?php if($editRow['blood_group']== "O-"){?> selected="selected"<?php }?>>O-</option>										
													</select>				
					  							<span id="user-result"></span>
                    	</div>

											<div class="form-group">
												<label for="name">Name<span class="star">*</span></label>
												<input type="text" id="name" name="name" value="<?php echo $editRow['name'];?>" placeholder="Name" class="form-control2" required>				
												<span id="user-result"></span>
                    	</div>

										<div class="form-group">
												<label for="name">Mobile<span class="star">*</span></label>
												<input type="text" id="mobile" value="<?php echo $editRow['mobile'];?>" placeholder="Mobile" name="mobile" class="form-control2" required>				
												<span id="user-result"></span>
                    </div>

										<div class="form-group">
												<label for="place">Place<span class="star">*</span></label>
												<input type="text" id="place" value="<?php echo $editRow['place'];?>" placeholder="Place" name="place" class="form-control2" required>				
												<span id="user-result"></span>
                    </div>

					</div>					 																								
            	</div>			
	 			<div>
            	</div>
            	<div class="modal-footer">
              		<input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            	</div>
				</form>
          	</div>
        </div>
      </div>
      <!-- Modal1 cls -->            
  </div>
<?php include("../adminFooter.php") ?>
