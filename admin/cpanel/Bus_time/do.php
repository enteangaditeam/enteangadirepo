<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_POST['bus_time'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");
			}
		else
			{							
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
						
				$data['bus_name']			=	$App->convert($_REQUEST['bus_name']);
				$data['bus_time']			=	$App->convert($_REQUEST['bus_time']);
				$data['location_from']		=	$App->convert($_REQUEST['location_from']);
				$data['location_to']		=	$App->convert($_REQUEST['location_to']);
				$data['location_via']		=	$App->convert($_REQUEST['location_via']);
				
				//Status is hard coded,Coz feature is not implimented now
				$data['status']				=	1;				
				
				$success=$db->query_insert(TABLE_BUS_TIME, $data);
				$db->close();				
				
				if($success)
				{
					$_SESSION['msg']="Bus time Added Successfully";										
				}
				else
				{
					$_SESSION['msg']="Failed";							
				}	
				header("location:index.php");	
			}
		break;
		
	// EDIT SECTION
	//-
	case 'edit':
		
		$fid	=	$_REQUEST['fid'];	       
		if(!$_POST['bus_time'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
					
									
				$data['bus_name']			=	$App->convert($_REQUEST['bus_name']);
				$data['bus_time']			=	$App->convert($_REQUEST['bus_time']);
				$data['location_from']		=	$App->convert($_REQUEST['location_from']);
				$data['location_to']		=	$App->convert($_REQUEST['location_to']);
				$data['location_via']		=	$App->convert($_REQUEST['location_via']);
				
				//Status is hard coded,Coz feature is not implimented now
				$data['status']			=	"Available";//$App->convert($_REQUEST['status']);		

					$success=$db->query_update(TABLE_BUS_TIME, $data, "ID='{$fid}'");					 
					$db->close(); 				
				
					if($success)
					{
						$_SESSION['msg']="Details updated successfully";																	
					}
					else
					{
						$_SESSION['msg']="Failed";									
					}	
					header("location:index.php");	
				
									
			}
		
		break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				try
				{			
					$success= @mysql_query("DELETE FROM `".TABLE_BUS_TIME."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}				
				$db->close(); 
				
				
				if($success)
				{
					$_SESSION['msg']="Bus time deleted successfully";									
				}
				else
				{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
				}	
				header("location:index.php");	
		break;	
}
?>