<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_POST['location'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");
			}
		else
			{							
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$location=$App->convert($_REQUEST['location']);
				$existId=$db->existValuesId(TABLE_LOCATION," location='$location'");
				if($existId>0)
				{					
					$_SESSION['msg']="Location already Exist !!";					
					header("location:index.php");				
				}
				else
				{		
					$data['location']			=	$App->convert($_REQUEST['location']);				
					
					$success=$db->query_insert(TABLE_LOCATION, $data);
					$db->close();				
				
					if($success)
					{
						$_SESSION['msg']="Location Added Successfully";										
					}
					else
					{
						$_SESSION['msg']="Failed";							
					}	
					header("location:index.php");		
				}
			}
		break;
		
	// EDIT SECTION
	//-
	case 'edit':
		
		$fid	=	$_REQUEST['fid'];	       
		if(!$_POST['location'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
					
				$location=$App->convert($_REQUEST['location']);
				$existId=$db->existValuesId(TABLE_LOCATION," location='$location' AND ID!='$fid'");

				if($existId>0)
				{					
					$_SESSION['msg']="Location already Exist !!";					
					header("location:edit.php?id=$fid");						
					break ;
				}
				else
				{					
					$data['location']			=	$App->convert($_REQUEST['location']);					
					$success=$db->query_update(TABLE_LOCATION, $data, "ID='{$fid}'");					 
					$db->close(); 				
				
					if($success)
					{
						$_SESSION['msg']="Location updated successfully";																	
					}
					else
					{
						$_SESSION['msg']="Failed";									
					}	
					header("location:index.php");	
				}
									
			}
		
		break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				try
				{			
					$success= @mysql_query("DELETE FROM `".TABLE_LOCATION."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this Location is used some where else";				            
				}				
				$db->close(); 
				
				
				if($success)
				{
					$_SESSION['msg']="Location deleted successfully";									
				}
				else
				{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
				}	
				header("location:index.php");	
		break;	
}
?>