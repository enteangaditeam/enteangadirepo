<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
    header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

$xtnArray = array("jpg", "png", "gif", "JPG", "jpeg", "JPEG");
switch($optype)
{
    //delete_event
    // NEW SECTION
    case 'new':

        if(!$_POST['name'])
        {
            $_SESSION['msg']="Error, Invalid Details!";
            header("location:index.php");
        }
        else
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success=0;
            $imageValidity                  = 1;
            $isViewable                     = 0;
            if (isset($_POST['isViewable'])) {
                $isViewable                 =   1;
            }
            $data['name']			        =	$App->convert($_REQUEST['name']);
            $data['isViewable']			    =	$App->convert($isViewable);
            $data['description']			=	$App->convert($_REQUEST['description']);
            $data['venu']				    =	$App->convert($_REQUEST['venue']);
            //$data['time_and_date']				=	date_create_from_format('d-m-Y H:i', $_REQUEST['time_date']);
            $eventDate = explode(' ', $_REQUEST['time_date']);
            $data['time_and_date']          = $App->dbformat_date_db($eventDate[0]). ' '.$App->dbformat_time($eventDate[1]);
            $data['guest']				    =	$App->convert($_REQUEST['guest']);
            $data['organizer']				=	$App->convert($_REQUEST['organizer']);
            $data['sponsor']				=	$App->convert($_REQUEST['sponsor']);
            $data['coordinator']				=	$App->convert($_REQUEST['coordinator']);
            $data['contact_no']				=	$App->convert($_REQUEST['contact_no']);
            $data['status']				    =	1;
            
            if ($_FILES['image']['name'] != "") {
                $path_info1 = pathinfo($_FILES["image"]["name"]);
                $ext1	 	=	$path_info1["extension"];
                $result 	= date("YmdHis");
                $newName 	= $result.".".$ext1;
                $img 		="";
                if (!$App->validateImages($ext1)) {
                    $imageValidity = 0;
                } else {
                    if(move_uploaded_file($_FILES["image"]["tmp_name"],"../../Images/events/".basename($newName)))
                    {
                        $img="Images/events/" . $newName;
                    }
                    $data['image_url']			=	$App->convert($img);
                }
            } else {
                $data['image_url'] = '';
            }
            if ($imageValidity) {
                $success=$db->query_insert(TABLE_EVENT, $data);
                $catArray 	=	$_REQUEST['locations'];
                if(!empty($catArray))
                {
                    for($i=0;$i<count($catArray);$i++)
                    {
                        $data1['event_id']	=	$success;
                        $data1['location_id']=	$catArray[$i];
                        $db->query_insert(TABLE_EVENT_LOCATION,$data1);
                    }
                }
                $db->close();

                if($success)
                {
                    $_SESSION['msg']="Event Added Successfully";
                }
                else
                {
                    $_SESSION['msg']="Failed";
                }
            } else {
                $_SESSION['msg']="Please upload a valid image";
            }
            header("location:index.php");

        }
        break;

    // EDIT SECTION
    //-
    case 'edit':

        $fid	=	$_REQUEST['fid'];
        if(!$_POST['name'])
        {
            $_SESSION['msg']="Error, Invalid Details!";
            header("location:edit.php?id=$fid");
        }
        else
        {
            $isViewable                     = 0;
            if (isset($_POST['isViewable'])) {
                $isViewable                 =   1;
            }
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success=0;
            $imageValidity = 1;
            $data['name']			=	$App->convert($_REQUEST['name']);
            $data['description']			=	$App->convert($_REQUEST['description']);
            $data['venu']				=	$App->convert($_REQUEST['venue']);
            //$data['time_and_date']				=	date_create_from_format('d-m-Y H:i', $_REQUEST['time_date']);
            $eventDate = explode(' ', $_REQUEST['time_date']);
            $data['time_and_date'] = $App->dbformat_date_db($eventDate[0]). ' '.$App->dbformat_time($eventDate[1]);
            $data['guest']				=	$App->convert($_REQUEST['guest']);
            $data['organizer']				=	$App->convert($_REQUEST['organizer']);
            $data['sponsor']				=	$App->convert($_REQUEST['sponsor']);
            $data['coordinator']				=	$App->convert($_REQUEST['coordinator']);
            $data['contact_no']				=	$App->convert($_REQUEST['contact_no']);
            $data['status']				    =	1;
            $data['isViewable']			    =	$App->convert($isViewable);
            if ($_FILES['image']['name'] != "") {
                $path_info1 = pathinfo($_FILES["image"]["name"]);
                $ext1	 	=	$path_info1["extension"];
                if (!$App->validateImages($ext1)) {
                    $imageValidity = 0;
                } else {
                    $imgRes = mysql_query("SELECT image_url FROM `".TABLE_EVENT."` WHERE ID = ".$fid);
                    $imgRow = mysql_fetch_array($imgRes);
                    if ($imgRow['image_url']) {
                        $image = '../../'.$imgRow['image_url'];
                        if (file_exists($image))
                        {
                            unlink($image);
                        }
                    }

                    $result 	= date("YmdHis");
                    $newName 	= $result.".".$ext1;
                    $img 		="";
                    if(move_uploaded_file($_FILES["image"]["tmp_name"],"../../Images/events/".basename($newName)))
                    {
                        $img="Images/events/" . $newName;
                    }
                    $data['image_url']			=	$App->convert($img);
                }

            }
            if ($imageValidity) {
                $success=$db->query_update(TABLE_EVENT, $data, "ID='{$fid}'");
                //	$db->close();
                $qry = "DELETE FROM ".TABLE_EVENT_LOCATION." where event_id=$fid";
                $resultQ = $db->query($qry);

                $catArray 	=	$_REQUEST['locations'];
                //echo $catArray ;
                if(!empty($catArray))
                {
                    for($i=0;$i<count($catArray);$i++)
                    {
                        $data1['event_id']	=	$fid;
                        $data1['location_id']=	$catArray[$i];
                        $db->query_insert(TABLE_EVENT_LOCATION,$data1);
                    }
                }


                $db->close();

                if($success)
                {
                    $_SESSION['msg']="Event updated successfully";
                }
                else
                {
                    $_SESSION['msg']="Failed";
                    header("location:edit.php?id=$fid");
                    return;
                }
            } else {
                $_SESSION['msg']="Please upload a valid image";
                 header("location:edit.php?id=$fid");
                 return;
            }

            header("location:index.php");

        }

        break;

    // DELETE SECTION
    //-
    case 'delete':

        $id	=	$_REQUEST['id'];
        $success=0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();

        $res=mysql_query("select image_url from ".TABLE_EVENT." where ID='{$id}'");
        $row=mysql_fetch_array($res);
        if ($row['image_url']) {
            $image = '../../'.$row['image_url'];
            if (file_exists($image))
            {
                unlink($image);
            }
        }

        $success= @mysql_query("DELETE FROM `".TABLE_EVENT_LOCATION."` WHERE event_id='{$id}'");
        $success= @mysql_query("DELETE FROM `".TABLE_EVENT."` WHERE ID='{$id}'");

        $db->close();


        if($success)
        {
            $_SESSION['msg']="Event deleted successfully";
        }
        else
        {
            $_SESSION['msg']="You can't Delete. ";
        }
        header("location:index.php");
        break;
    case 'approve':
        if(!$_REQUEST['id'])
        {
            $_SESSION['msg']="Error, Invalid Details!";
            header("location:approve.php");
        } else {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $fid = $_REQUEST['id'];
            $success = 0;
            $data['status'] = 1;
            $success=$db->query_update(TABLE_EVENT, $data, "ID='{$fid}'");
            if($success)
            {
                $_SESSION['msg']="Event approved successfully";
            }
            else
            {
                $_SESSION['msg']="Something went wrong!";
            }
            header("location:approve.php");
            break;
        }
}
?>