<?php
include("../adminHeader.php");
if($_SESSION['LogID']=="")
{
    header("location:../../logout.php");
}
$type		=	$_SESSION['LogType'];

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
?>
<script>
    function delete_type()
    {
        var del=confirm("Do you Want to Delete ?");
        if(del==true)
        {
            window.submit();
        }
        else
        {
            return false;
        }
    }
</script>

<?php
if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }
$_SESSION['msg']='';
?>

<div class="col-md-10 col-sm-8 rightarea">
    <div class="row">
        <div class="col-sm-8">
            <div class="clearfix">
                <h2 class="q-title">Events</h2>
                <a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD NEW</a>
            </div>
        </div>
        <div class="col-sm-4">
            <form method="post">
                <div class="input-group">
                    <input type="text" class="form-control"  name="search" placeholder="Name" value="<?php echo @$_REQUEST['search'] ?>">
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> </div>
            </form>
        </div>
    </div>
    <?php
    $cond="1";
    if(@$_REQUEST['search'])
    {
        $cond=$cond." and ".TABLE_EVENT.".name like'%".$_REQUEST['search']."%'";
    }

    ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="tablearea table-responsive">
                <table id="rowspan" class="tablesorter table view_limitter pagination_table" >
                    <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Description</th>
                        <th>Venue</th>
                        <th>Time &amp; Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $selAllQuery="SELECT * FROM ".TABLE_EVENT." WHERE status = 1 and $cond ORDER BY ID ASC";

                    $selectAll= $db->query($selAllQuery);
                    $number=mysql_num_rows($selectAll);
                    $rowsPerPage = 0;
                    if($number==0)
                    {
                        ?>
                        <tr>
                            <td align="center" colspan="7">
                                There is no events in the list.
                            </td>
                        </tr>
                        <?php
                    }
                    else
                    {
                        /*********************** for pagination ******************************/
                            $rowsPerPage = ROWS_PER_PAGE;
                        if(isset($_GET['page']))
                        {
                            $pageNum = $_GET['page'];
                        }
                        else
                        {
                            $pageNum =1;
                        }
                        $offset = ($pageNum - 1) * $rowsPerPage;
                        $select1=$db->query($selAllQuery." limit $offset, $rowsPerPage");
                        $i=$offset+1;
                        //use '$select1' for fetching
                        /*************************** for pagination **************************/
                        while($row=mysql_fetch_array($select1))
                        {
                            $tableId=$row['ID'];
                            $eventDate = explode(' ', $row['time_and_date']);
                            $eventDate = substr(($App->dbformat_date_db_with_hyphen($eventDate[0]). ' '.$App->dbformat_time($eventDate[1])), 0, -3);
                            $tableId = $row['ID'];
                            $locQry = "SELECT ".TABLE_LOCATION.".location FROM ".TABLE_EVENT_LOCATION."	LEFT JOIN ".TABLE_LOCATION." ON ".TABLE_LOCATION.".ID = ".TABLE_EVENT_LOCATION.".location_id WHERE ".TABLE_EVENT_LOCATION.".event_id =$tableId";
                            $locResult = $db->query($locQry);
                            $locArray = array();
                            while($locRow = mysql_fetch_array($locResult))
                            {
                                array_push($locArray,$locRow['location']);
                            }
                            $locString	=	implode(",",$locArray);
                            $isUser = false;
                            if ($row['user_id']) {
                                $isUser = true;
                                $user_id = $row['user_id'];
                                $userRes = mysql_query("SELECT first_name, last_name, mobile, email FROM `".TABLE_USER."` WHERE ID = ".$user_id);
                                $userRow = mysql_fetch_array($userRes);
                            }
                            ?>
                            <tr>
                                <td><?php echo $i++;?>
                                    <div class="adno-dtls">
                                        <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> |
                                        <a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a> |
                                        <a href="#" data-toggle="modal" data-target="#event_view_<?= $tableId; ?>">View</a>
                                    </div>

                                    <!-- Pop up -->
                                    <div  class="modal fade" id="event_view_<?= $tableId; ?>" tabindex="-1" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div role="tabpanel" class="tabarea2">

                                                    <!-- Nav tabs -->
                                                    <ul class="nav nav-tabs" role="tablist">
                                                        <li role="presentation" class="active"> <a href="#personal<?php echo $tableId; ?>" aria-controls="personal" role="tab" data-toggle="tab">Event Details</a> </li>
                                                    </ul>

                                                    <!-- Tab panes -->
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
                                                            <table class="table nobg">
                                                                <tbody style="background-color:#FFFFFF">

                                                                <tr>
                                                                    <td>Name</td>
                                                                    <td> : </td>
                                                                    <td><?php  echo $row['name']; ?></td>
                                                                </tr>
                                                                <?php
                                                                if ($row['image_url']) {
                                                                    ?>
                                                                    <tr>
                                                                        <td>Photo</td>
                                                                        <td> : </td>
                                                                        <td>

                                                                            <div style="max-width: 100px;"><img src="../../<?= $row['image_url']; ?>" alt="No Image" class="img-responsive" /></div>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                ?>

                                                                <tr>
                                                                    <td>Description</td>
                                                                    <td> : </td>
                                                                    <td><?php  echo $row['description']; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Venue</td>
                                                                    <td> : </td>
                                                                    <td><?php  echo $row['venu']; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Time &amp; Date</td>
                                                                    <td> : </td>
                                                                    <td><?= $eventDate; ?></td>
                                                                </tr>
                                                                <?php
                                                                if (count($locArray) > 0) {
                                                                    ?>
                                                                    <tr>
                                                                        <td>Locations</td>
                                                                        <td> : </td>
                                                                        <td> <?= $locString; ?></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                if ($row['guest']) {
                                                                    ?>
                                                                    <tr>
                                                                        <td>Guest</td>
                                                                        <td> : </td>
                                                                        <td><?php  echo $row['guest']; ?></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                if ($row['organizer']) {
                                                                    ?>
                                                                    <tr>
                                                                        <td>Organizer</td>
                                                                        <td> : </td>
                                                                        <td><?php  echo $row['organizer']; ?></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                if ($row['sponsor']) {
                                                                    ?>
                                                                    <tr>
                                                                        <td>Sponsor</td>
                                                                        <td> : </td>
                                                                        <td><?php  echo $row['sponsor']; ?></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                if ($row['coordinator']) {
                                                                    ?>
                                                                    <tr>
                                                                        <td>Coordinator</td>
                                                                        <td> : </td>
                                                                        <td><?php  echo $row['coordinator']; ?></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                if ($row['contact_no']) {
                                                                    ?>
                                                                    <tr>
                                                                        <td>Contact No</td>
                                                                        <td> : </td>
                                                                        <td><?php  echo $row['contact_no']; ?></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                ?>
                                                                </tbody>
                                                            </table>
                                                            <?php
                                                            if ($isUser) {
                                                                ?>
                                                                <h3>User Details</h3>
                                                                <table class="table nobg">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>Name</td>
                                                                        <td>:</td>
                                                                        <td><?= $userRow['first_name']. ' ' .$userRow['last_name']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Mobile</td>
                                                                        <td>:</td>
                                                                        <td><?= $userRow['mobile']; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Email</td>
                                                                        <td>:</td>
                                                                        <td><?= $userRow['email']; ?></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Pop up -->
                                </td>
                                <td><?= $row['name']; ?></td>
                                <td><?php if ($row['image_url']) { ?> <img src="../../<?= $row['image_url']; ?>" alt="<?= $row['name']; ?>" width="80" height="80"/> <?php } ?></td>
                                <td><?= $row['description']; ?></td>
                                <td><?= $row['venu']; ?></td>
                                <td style="min-width: 180px;"><?= $eventDate; ?></td>
                            </tr>
                        <?php }
                    }?>
                    </tbody>
                </table>

            </div>
            <!-- **********************************************************************  -->
            <?php
            if($number>$rowsPerPage)
            {
                ?>
                <br />
                <div style="text-align: center";>
                    <div class="pagerSC" align="center" style="display: inline-block;">
                        <?php

                        $query   =  $db->query($selAllQuery);
                        $numrows = mysql_num_rows($query);
                        $maxPage = ceil($numrows/$rowsPerPage);
                        $self = $_SERVER['PHP_SELF'];
                        $nav  = '';
                        if ($pageNum - 5 < 1) {
                            $pagemin = 1;
                        } else {
                            $pagemin = $pageNum - 5;
                        };
                        if ($pageNum + 5 > $maxPage) {
                            $pagemax = $maxPage;
                        } else {
                            $pagemax = $pageNum + 5;
                        };

                        $search=@$_REQUEST['search'];
                        for($page = $pagemin; $page <= $pagemax; $page++)
                        {
                            if ($page == $pageNum)
                            {
                                $nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
                            }
                            else
                            {
                                if(@$search)
                                {
                                    $nav .= " <a href=\"$self?page=$page&search=$search\">$page</a> ";
                                }
                                else
                                {
                                    $nav .= " <a href=\"$self?page=$page\">$page</a> ";
                                }
                            }
                        }
                        ?>
                        <?php
                        if ($pageNum > 1)
                        {
                            $page  = $pageNum - 1;
                            if(@$search)
                            {
                                $prev  = " <a href=\"$self?page=$page&search=$search\">Prev</a> ";
                                $first = " <a href=\"$self?page=1&search=$search\">First</a> ";
                            }
                            else
                            {
                                $prev  = " <a href=\"$self?page=$page\">Prev</a> ";
                                $first = " <a href=\"$self?page=1\">First</a> ";
                            }
                        }
                        else
                        {
                            $prev  = '&nbsp;';
                            $first = '&nbsp;';
                        }

                        if ($pageNum < $maxPage)
                        {
                            $page = $pageNum + 1;
                            if(@$search)
                            {
                                $next = " <a href=\"$self?page=$page&search=$search\">Next</a> ";
                                $last = " <a href=\"$self?page=$maxPage&search=$search\">Last</a> ";
                            }
                            else
                            {
                                $next = " <a href=\"$self?page=$page\">Next</a> ";
                                $last = " <a href=\"$self?page=$maxPage\">Last</a> ";
                            }
                        }
                        else
                        {
                            $next = '&nbsp;';
                            $last = '&nbsp;';
                        }
                        echo $first . $prev . $nav . $next . $last;
                        ?>
                        <div style="clear: left;"></div>
                    </div>
                </div>
                <?php
            }
            ?>

            <!-- ******************************************************************-->



        </div>
    </div>
</div>

<!-- Modal1 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Events</h4>
            </div>
            <div class="modal-body clearfix">
                <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">Name:<span class="star">*</span></label>
                                <input type="text" id="name" name="name" class="form-control2" value=""  required placeholder="Name" />
                            </div>
                            <div class="form-group">
                                <label for="image">Image:</label>
                                <input type="file" id="image" class="show_progress" name="image" value="" />
                            </div>
                            <div class="form-group">
                                <label for="description">Description:<span class="star">*</span></label>
                                <textarea style="min-height: 100px;" id="description" name="description"  required placeholder="Description" class="form-control2"></textarea>
                            </div>
                             
                            <div class="form-group">
                                <label for="venue">Venue:<span class="star">*</span></label>
                                <input type="text" id="venue" name="venue" class="form-control2" value=""  required placeholder="Venue" />
                            </div>
                            <div class="form-group">
                                <label for="time_date">Time &amp; Date:<span class="star">*</span></label>
                                <input type="text" id="time_date" name="time_date" class="form-control2" value=""  required placeholder="Time and Date" />
                            </div>
                            <div class="form-group">
                                <label for="guest">Guest:</label>
                                <input type="text" id="guest" name="guest" class="form-control2" placeholder="Guest" />
                            </div>
                            <div class="form-group">
                                <label for="organizer">Organizer:</label>
                                <input type="text" id="organizer" name="organizer" class="form-control2" value="" placeholder="Organizer" />
                            </div>
                            <div class="form-group">
                                <label for="sponsor">Sponsor:</label>
                                <input type="text" id="sponsor" name="sponsor" class="form-control2" value="" placeholder="Sponsor" />
                            </div>
                            <div class="form-group">
                                <label for="coordinator">Coordinator:</label>
                                <input type="text" id="coordinator" name="coordinator" class="form-control2" value="" placeholder="Coordinator" />
                            </div>
                            <div class="form-group">
                                <label for="contact_no">Contact No:</label>
                                <input type="text" id="contact_no" name="contact_no" class="form-control2" value="" placeholder="Contact No" />
                            </div>
                           <div class="form-group">
                                <input type="checkbox" name="isViewable" id="isViewable">
                                <label for="contact_no">Show in Home</label>
                            </div>
                            <div class="form-group">
                                <label for="Location">Location</label>
                                <ul class="category_combo_list list-unstyled" style="display: block;">
                                    <?php
                                    $selAllQuery="SELECT * FROM ".TABLE_LOCATION." ORDER BY location ASC";
                                    $selectAll= $db->query($selAllQuery);
                                    while($row=mysql_fetch_array($selectAll))
                                    { ?>
                                        <li><input type="checkbox" name="locations[]" <?php if($row['ID']==1){ ?>checked="true" <?php } ?> id="breaking" value="<?= $row['ID'];?>">
                                            <label><?= $row['location']; ?></label></li>
                                    <?php	}
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<!-- Detailed view for events -->
<!-- Modal1 -->
<div class="modal fade" id="event_view" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Events - Detailed View</h4>
            </div>
            <div class="modal-body clearfix">

            </div>
        </div>
    </div>
</div>
<?php include("../adminFooter.php") ?>
