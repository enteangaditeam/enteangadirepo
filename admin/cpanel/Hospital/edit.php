<?php 
	include("../adminHeader.php"); 
	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>


<?php
	if(isset($_SESSION['msg']))
	{?>
		<font color="red"><?php echo $_SESSION['msg']; ?></font><?php 
	}	
	$_SESSION['msg']='';
	$editId=$_REQUEST['id'];
	$tableEdit="SELECT * FROM ".TABLE_HOSPITAL." WHERE ID='$editId'";	
	$editField=mysql_query($tableEdit);
	$editRow=mysql_fetch_array($editField);
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="index.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">Hospital </h4>
            </div>
            <div class="modal-body clearfix">
				<form method="post" action="do.php?op=edit" class="form1" enctype="multipart/form-data" onsubmit="return valid()">
							<input type="hidden" name="fid" id="fid" value="<?php echo $editId ?>">
			             
                <div class="row">
                 	<div class="col-sm-6">

											<div class="form-group">
                      <label for="category">Category <span class="star">*</span></label>
                      <?php 
											?>
											<select name="category" id="group" class="form-control2" required >
											<?php
												$categoryTypes="select ID,category from ".TABLE_HOSPITAL_CATEGORY."";
												$res2=mysql_query($categoryTypes);													
												while($row=mysql_fetch_array($res2))
												{?>	
													<option value="<?php echo $row['ID']?>" <?php if($editRow['category_id']== $row['ID']){?> selected="selected"<?php }?>><?php echo $row['category']?></option>
												<?php 									
												}?>	            																						
											</select>				
					  					<span id="user-result"></span>
                    </div>	
										<div class="form-group">
                      <label for="emergency_name">Hospital Name<span class="star">*</span></label>
                      <input type="text" id="hospital_name" value="<?php echo $editRow['hospital_name'];?>" placeholder="Name" title="Name" name="hospital_name" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="contact_number">Contact Number<span class="star">*</span></label>
                      <input type="text" id="contact_number" value="<?php echo $editRow['contact_number'];?>" placeholder="Contact Number" title="Contact Number"  name="contact_number" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>
									  <div class="form-group">
                      <label for="Description">Address<span class="star"></span></label>
                      <textarea id="address" name="address" class="form-control2" placeholder="Address" ><?php echo $editRow['address'];?></textarea>				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
                      <label for="Photo">Photo</label>
                      <input type="file" name="photo"  id="image_url"  >		  				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
											<?php 
											if($editRow['image_url'])
											{
												$path="../../".$editRow['image_url']; ?>
												<div style="max-width: 255px;"><img src="<?php echo $path ?>" alt="No Image" id="thumb" class="img-responsive" /></div>
											<?php }else{?>
											<div style="max-width: 255px;"><img src="../../img/student.png" alt="No Image"  class="img-responsive" /></div>
											<?php } ?>
										</div>	

								</div>					 																								
            	</div>			
	 			<div>
            	</div>
            	<div class="modal-footer">
              		<input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            	</div>
				</form>
          	</div>
        </div>
      </div>
      <!-- Modal1 cls -->            
  </div>
<?php include("../adminFooter.php") ?>
