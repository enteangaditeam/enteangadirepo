<?php 
	include("../adminHeader.php");
	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}
	$type		=	$_SESSION['LogType'];

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>
<script>
	function delete_type()
	{
		var del=confirm("Do you Want to Delete ?");
		if(del==true)
		{
			window.submit();
		}
		else
		{
			return false;
		}
	}
	// ajax for loading photo

	function loadMorePhoto(sid,hospital)
	{
		var xmlhttp;
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("department"+sid).innerHTML=xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","department.php?hospitalId="+sid+"&hospital="+hospital,true);
		xmlhttp.send();
	}

	function loadDoctor(sid,hospital)
	{
		var xmlhttp;
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("doctor"+sid).innerHTML=xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","doctor.php?hospitalId="+sid+"&hospital="+hospital,true);
		xmlhttp.send();
	}

		// ajax for loading Priority
	function PriorityLoad(sid,prio,shop)
	{
		var xmlhttp;
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("tabPriority"+sid).innerHTML=xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","priority.php?shopId="+sid+"&priority="+prio+"&shop="+shop,true);
		xmlhttp.send();
	}	

		// ajax for loading Priority
	function PriorityLoad2(sid,prio,shop)
	{
		var xmlhttp;
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("tabPriority2"+sid).innerHTML=xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","priorityDoctor.php?shopId2="+sid+"&priority2="+prio+"&shop2="+shop,true);
		xmlhttp.send();
	}	
</script>
<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>
<?php
	//Select all category names
	$categoryTypes="select ID,category from ".TABLE_HOSPITAL_CATEGORY."";
	$res2=mysql_query($categoryTypes);
?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">HOSPITAL</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD NEW</a> 
				</div>
          </div>
          <div class="col-sm-4">
            <form method="post">
						  <div class="input-group">
										<input type="text" class="form-control"  name="search" placeholder="Hospital/Category" value="<?php echo @$_REQUEST['search'] ?>">
								<span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> </div>
            </form>
          </div>
        </div>
 <?php	
$cond="1";
if(@$_REQUEST['search'])
{
		$cond=$cond." and ".TABLE_HOSPITAL.".hospital_name like '%".$_REQUEST['search']."%' OR  ".TABLE_HOSPITAL_CATEGORY.".category LIKE  '%".$_REQUEST['search']."%'";	
}

?>
        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table id="rowspan" class="tablesorter table view_limitter pagination_table" >
                <thead>
                  <tr >
										<th>Sl No</th>               
										<th>Category</th>
										<th>Hospital Name</th>
										<th>Phone</th>	
										<th>Priority</th>					
										<th></th>										
										<th></th>
										<th></th>																											
                  </tr>
                </thead>
                <tbody>
								<?php 	
									 	$rowsPerPage = ROWS_PER_PAGE;															
										$selAllQuery="SELECT ".TABLE_HOSPITAL.".ID,".TABLE_HOSPITAL.".priority,".TABLE_HOSPITAL.".hospital_name, 
										".TABLE_HOSPITAL.".address,".TABLE_HOSPITAL.".contact_number,    
										".TABLE_HOSPITAL.".image_url, ".TABLE_HOSPITAL_CATEGORY.".category
										 FROM ".TABLE_HOSPITAL." JOIN ".TABLE_HOSPITAL_CATEGORY." ON ".TABLE_HOSPITAL_CATEGORY.".ID=".TABLE_HOSPITAL.".category_id WHERE $cond ORDER BY ".TABLE_HOSPITAL.".ID ASC";
																
										$selectAll= $db->query($selAllQuery);
										$number=mysql_num_rows($selectAll);
										if($number==0)
										{
										?>
											<tr>
												<td align="center" colspan="8">
													There is no details in list.
												</td>
											</tr>
										<?php
										}
										else
										{
											/*********************** for pagination ******************************/												
											if(isset($_GET['page']))
											{
												$pageNum = $_GET['page'];
											}
											else
											{
												$pageNum =1;
											}
											$offset = ($pageNum - 1) * $rowsPerPage;
											$select1=$db->query($selAllQuery." limit $offset, $rowsPerPage");
											$i=$offset+1;
											//use '$select1' for fetching
											/*************************** for pagination **************************/	
											while($row=mysql_fetch_array($select1))
											{	
												$tableId=$row['ID'];
												$name=$row['hospital_name'];																								
												?>
												<tr>
												
												<td><?php echo $i++;?>
															<div class="adno-dtls"> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> |
																		<a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a> |
																		<a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>"> VIEW</a>
															</div>																					  						
													<!-- Modal3 -->
																	<div  class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
																	<div class="modal-dialog">
																		<div class="modal-content"> 																																					
																		<div role="tabpanel" class="tabarea2"> 
																			
																			<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Hospital Details</h4>
            </div>
																			
																			<!-- Tab panes -->
																			<div class="tab-content">
																			<div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
																				<table class="table nobg">
																				<tbody style="background-color:#FFFFFF">
															
																					<tr>
																						<td>Category</td>
																						<td> : </td>
																						<td><?php  echo $row['category']; ?></td>
																					</tr>
																					<tr>
																						<td>Name</td>
																						<td> : </td>
																						<td><?php  echo $row['hospital_name']; ?></td>
																					</tr>
																					<tr>
																						<td>Contact No</td>
																						<td> : </td>
																						<td><?php  echo $row['contact_number']; ?></td>
																					</tr>
																						<tr>
																						<td>Address</td>
																						<td> : </td>
																						<td><?php  echo $row['address']; ?></td>
																					</tr>
																					<tr>
																						<td>Photo</td>
																						<td> : </td>
																						<td><?php if($row['image_url']){
																									$path="../../".$row['image_url']; ?>
																									<div style="max-width: 100px;"><img src="<?php echo $path ?>" alt="No Image" class="img-responsive" /></div>
																									<?php } ?>
																						</td>
																					</tr>																																																													
																				</tbody>
																				</table>
																			</div>
																													
																			</div>
																		</div>
																		</div>
																	</div>
																	</div>
																	<!-- Modal3 cls --> 																				  						
													</td>
													<td><?php echo $row['category']; ?></td>
													<td><?php echo $row['hospital_name']; ?></td>
													<td><?php echo $row['contact_number']; ?></td>
													<td><?= $row['priority']; $pr=$row['priority']; $name=$row['hospital_name']; ?></td>	
													<td style="width: 108px;" id="priority<?php echo $tableId; ?>" ><a href="#" onclick="PriorityLoad('<?php echo $tableId;?>','<?php echo $pr;?>','<?php echo $name;?>')" data-toggle="modal" data-target="#myModal7<?php echo $tableId; ?>" class="viewbtn">Priority</a>
													<!-- Modal7 priority -->
														<div class="modal fade" id="myModal7<?php echo $tableId; ?>" tabindex="-1" role="dialog">
															<div class="modal-dialog">
																<div class="modal-content">
																	<div role="tabpanel" class="tabarea2">
																		<div class="modal-header">
																			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																			<h4 class="modal-title">Hospital Priority</h4>
																		</div>
																		<!-- Tab panes -->
																		<div class="tab-content" id="tabPriority<?php echo $tableId; ?>">																			
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!-- Modal6 priority cls -->
													<td style="width: 108px;" id="photo<?php echo $tableId; ?>" ><a href="#" onclick="loadMorePhoto('<?php echo $tableId;?>','<?php echo $name;?>')" data-toggle="modal" data-target="#myModal5<?php echo $tableId; ?>" class="viewbtn">Department</a>													
														<!-- Modal3 photo -->
														<div class="modal fade" id="myModal5<?php echo $tableId; ?>" tabindex="-1" role="dialog">
															<div class="modal-dialog">
																<div class="modal-content">
																	<div role="tabpanel" class="tabarea2">
																		<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Department</h4>
            </div>
																		<!-- Tab panes -->
																		<div class="tab-content" id="department<?php echo $tableId; ?>">
																			
																	</div>
																</div>
															</div>
														</div>
													</div>
														<!-- Modal3 photo cls -->
													</td>
													<td style="width: 108px;" id="photo<?php echo $tableId; ?>" ><a href="#" onclick="loadDoctor('<?php echo $tableId;?>','<?php echo $name;?>')" data-toggle="modal" data-target="#myModal6<?php echo $tableId; ?>" class="viewbtn">Doctor</a>													
														<!-- Modal3 photo -->
														<div class="modal fade" id="myModal6<?php echo $tableId; ?>" tabindex="-1" role="dialog">
															<div class="modal-dialog" style="width: 900px;">
																<div class="modal-content">
																	<div role="tabpanel" class="tabarea2">
																		<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Doctors</h4>
            </div>
																		<!-- Tab panes -->
																		<div class="tab-content" id="doctor<?php echo $tableId; ?>">
																			
																	</div>
																</div>
															</div>
														</div>
													</div>
														<!-- Modal3 photo cls -->
													</td>				                   	 	
												</tr>
										<?php }
									}?>                  
                </tbody>
              </table>
			  	
            </div>
		<!-- **********************************************************************  -->
									<?php 
										if($number>$rowsPerPage)
										{
										?>	
										<br />	
										<div style="text-align: center";>
											<div class="pagerSC" align="center" style="display: inline-block;">
										<?php
										
										$query   =  $db->query($selAllQuery);
										$numrows = mysql_num_rows($query);
										$maxPage = ceil($numrows/$rowsPerPage);
										$self = $_SERVER['PHP_SELF'];
										$nav  = '';
										if ($pageNum - 5 < 1) {
										$pagemin = 1;
										} else {
										$pagemin = $pageNum - 5;
										};
										if ($pageNum + 5 > $maxPage) {
										$pagemax = $maxPage;
										} else {
										$pagemax = $pageNum + 5;
										};
										
										$search=@$_REQUEST['search'];
										for($page = $pagemin; $page <= $pagemax; $page++)
										{
											if ($page == $pageNum)
											{
												$nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
											}
											else
											{
												if(@$search)
													{
													$nav .= " <a href=\"$self?page=$page&search=$search\">$page</a> ";
												}
												else
												{
													$nav .= " <a href=\"$self?page=$page\">$page</a> ";
												}
											}
										}
										?>
										<?php
										if ($pageNum > 1)
										{
											$page  = $pageNum - 1;
											if(@$search)
											{
												$prev  = " <a href=\"$self?page=$page&search=$search\">Prev</a> ";
												$first = " <a href=\"$self?page=1&search=$search\">First</a> ";
											}
											else
											{
												$prev  = " <a href=\"$self?page=$page\">Prev</a> ";
												$first = " <a href=\"$self?page=1\">First</a> ";
											}
										}
										else
										{
											$prev  = '&nbsp;';
											$first = '&nbsp;';
										}
										
										if ($pageNum < $maxPage)
										{
											$page = $pageNum + 1;
											if(@$search)
											{
													$next = " <a href=\"$self?page=$page&search=$search\">Next</a> ";
													$last = " <a href=\"$self?page=$maxPage&search=$search\">Last</a> ";
											}
											else
											{
													$next = " <a href=\"$self?page=$page\">Next</a> ";
													$last = " <a href=\"$self?page=$maxPage\">Last</a> ";
											}
										}
										else
										{
											$next = '&nbsp;';
											$last = '&nbsp;';
										}
										echo $first . $prev . $nav . $next . $last;
										?>
										<div style="clear: left;"></div>
										</div>	 
									</div>
									<?php
									}
													?>
											
										<!-- ******************************************************************-->
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Hospital </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-sm-6">

                    <div class="form-group">
                      <label for="category">Category <span class="star">*</span></label>
                      
											<select name="category" id="group" class="form-control2" required >
											<?php
											$categoryTypes="select ID,category from ".TABLE_HOSPITAL_CATEGORY."";
											$res2=mysql_query($categoryTypes);	
											
											while($row=mysql_fetch_array($res2))
											{?>	
												<option value="<?php echo $row['ID']?>"><?php echo $row['category']?></option>
											<?php 									
											}?>	            			
											</select>	
					  					<span id="user-result"></span>
                    </div>	
										<div class="form-group">
                      <label for="emergency_name">Hospital Name<span class="star">*</span></label>
                      <input type="text" id="hospital_name" name="hospital_name" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="contact_number">Contact Number<span class="star">*</span></label>
                      <input type="text" id="contact_number" name="contact_number" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
                      <label for="Description">Address<span class="star"></span></label>
                      <textarea id="address" name="address" class="form-control2" ></textarea>				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
                      						<label for="Photo">Photo<span class="star"></span></label>
                      						<input type="file" name="photo" id="image_url" >		  				
					  						<span id="user-result"></span>
                    					</div>
										<div class="form-group">
											<img id="thumb" width="255" height="265" src="../../img/student.png" alt="your image" />
										</div>	

									</div>
								</div>
			  				<div>
            		</div>
            		<div class="modal-footer">
              		<input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            		</div>
							</form>
          	</div>
        	</div>
      	</div>
	  	</div>
<?php include("../adminFooter.php") ?>
