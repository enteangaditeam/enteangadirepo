<?php
	require("../../config/config.inc.php"); 
	require("../../config/Database.class.php");
	require("../../config/Application.class.php");

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();

	$tableId 	= 	$_REQUEST['hospitalId'];	
	$hosName 	=	$_REQUEST['hospital'];

?>

	<div role="tabpanel" class="tab-pane active" id="photo<?php echo $tableId; ?>">
		<form action="do.php?op=doctor" class="form1" method="post" onsubmit="return valid()" >
			<input type="hidden" name="hospitalId" id="hospitalId" value="<?php echo $tableId ?>">               								
			<div class="row">
				<div class="col-sm-6">
					
					<div class="form-group">		                      						
						<label for="emergency_name">Department Name<span class="star">*</span></label>						                      
						<select name="department" id="group" class="form-control2" required >
							<?php
							$categoryTypes="select ID,department from ".TABLE_HOSPITAL_DEPARTMENT." where hospital_id='$tableId'";
							$res2=mysql_query($categoryTypes);							
							while($row=mysql_fetch_array($res2))
							{?>	
								<option value="<?php echo $row['ID']?>"><?php echo $row['department']?></option>
							<?php 									
							}?>	            			
						</select>																	
					  	<span id="user-result"></span>                    	                     
					</div>
					<div class="form-group">		                      						
						<label for="emergency_name">Doctor<span class="star">*</span></label>
						<input type="text" id="doctor" name="doctor" class="form-control2" required>				
					  	<span id="user-result"></span>                    	                     
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">		                      						
						<label for="emergency_name">Booking Number<span class="star"></span></label>
						<input type="text" id="booking" name="booking" class="form-control2" >				
					  	<span id="user-result"></span>                    	                     
					</div>	
					<div class="form-group">		                      						
						<label for="emergency_name">Worked Before</label>
						<textarea name="worked_before" id="worked_before" class="form-control2"></textarea>                    	                     
					</div>				
				</div> 	                  
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label for="sun">Sunday<span class="star"></span></label>
						<input type="text" id="sun" name="sun" class="form-control2 timepicker5" placeholder="09.00 AM " >
						<input type="text" id="sunt" name="sunt" class="form-control2 timepicker5" placeholder="04.30 PM" >
					</div>
					<div class="form-group">
						<label for="sun">Wednesday<span class="star"></span></label>
						<input type="text" id="wed" name="wed" class="form-control2 timepicker5" placeholder="09.00 AM" >
						<input type="text" id="wedt" name="wedt" class="form-control2 timepicker5" placeholder="04.30 PM" >
					</div>
					<div class="form-group">
						<label for="sun">Saturday<span class="star"></span></label>
						<input type="text" id="sat" name="sat" class="form-control2 timepicker5" placeholder="09.00 AM" >
						<input type="text" id="satt" name="satt" class="form-control2 timepicker5" placeholder="04.30 PM" >
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label for="sun">Monday<span class="star"></span></label>
						<input type="text" id="mon" name="mon" class="form-control2 timepicker5" placeholder="09.00 AM" >
						<input type="text" id="mont" name="mont" class="form-control2 timepicker5" placeholder="04.30 PM" >
					</div>
					<div class="form-group">
						<label for="sun">Thursday<span class="star"></span></label>
						<input type="text" id="thur" name="thur" class="form-control2 timepicker5" placeholder="09.00 AM " >
						<input type="text" id="thurt" name="thurt" class="form-control2 timepicker5" placeholder="04.30 PM" >
					</div>
				</div>
				<div class="col-sm-4">
				<div class="form-group">
						<label for="sun">Tuesday<span class="star"></span></label>
						<input type="text" id="tue" name="tue" class="form-control2" placeholder="09.00 AM" >
						<input type="text" id="tuet" name="tuet" class="form-control2" placeholder="04.30 PM" >
					</div>
					<div class="form-group">
						<label for="sun">Friday<span class="star"></span></label>
						<input type="text" id="fri" name="fri" class="form-control2" placeholder="09.00 AM" >
						<input type="text" id="frit" name="frit" class="form-control2" placeholder="04.30 PM" >
					</div>
				</div>
			</div>              
			<div class="modal-footer">
				<input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
			</div>			          
		</form>
		
		<div class="tablearea table-responsive">
			<table class="table table_pop" id="headerTable2" >
			<thead style="background-color:#CCC;">
				<tr>
					<th>Sl No</th>
					<th>Department</th>
					<th>Doctor</th>
					<th>Booking No</th>
					<th>Worked before</th> 	
					<th>Priority</th>					
					<th></th>			                           
				</tr>
			</thead>
			<tbody >                             
				<?php							
				$selAllQuery3="select `".TABLE_DOCTOR."`.worked_before,`".TABLE_DOCTOR."`.priority,`".TABLE_DOCTOR."`.ID ,`".TABLE_DOCTOR."`.doctor_name ,
				`".TABLE_DOCTOR."`.sun,`".TABLE_DOCTOR."`.mon,`".TABLE_DOCTOR."`.tue,`".TABLE_DOCTOR."`.wed,`".TABLE_DOCTOR."`.thur,`".TABLE_DOCTOR."`.fri,`".TABLE_DOCTOR."`.sat,`".TABLE_DOCTOR."`.booking_number,`".TABLE_HOSPITAL_DEPARTMENT."`.department	from 
				`".TABLE_DOCTOR."`,`".TABLE_HOSPITAL_DEPARTMENT."`	
				where `".TABLE_DOCTOR."`.hospital_id='$tableId' and `".TABLE_DOCTOR."`.department=`".TABLE_HOSPITAL_DEPARTMENT."`.ID 	order by ID desc";
				//echo $selAllQuery;
				$selectAll3= $db->query($selAllQuery3);
				$num3=mysql_num_rows($selectAll3);							

				if($num3==0)
				{
					?>
					<tr>
						<td colspan="6" align="center">No data found</td>
					</tr>
					<?php
				}
				else
				{	
					$j=0;														
					while($row3=mysql_fetch_array($selectAll3))
					{
						$tabphotoId=$row3['ID']; 								
						?>
						<tr>
							<td><?php echo ++$j;?>									
								<div class="adno-dtls"><a href="do.php?deleteId=<?php echo $tabphotoId; ?>&op=delDoctor" class="delete" onclick="return delete_type();">DELETE</a> </div>
							</td> 
							<td>
								<?php echo $row3['department']; ?>
							</td>
							<td>
								<?php echo $row3['doctor_name']; ?>
							</td>
						
							<td>
								<?php echo $row3['booking_number']; ?>
							</td>
							<td>
								<?php echo $row3['worked_before']; ?>
							</td> 	
							<td><?= $row['priority']; $pr=$row3['priority']; $name=$row3['doctor_name']; ?></td>	
							<td style="width: 108px;" id="priority<?php echo $tabphotoId; ?>" ><a href="#" onclick="PriorityLoad2('<?php echo $tabphotoId;?>','<?php echo $pr;?>','<?php echo $name;?>')" data-toggle="modal" data-target="#myModal8<?php echo $tabphotoId; ?>" class="viewbtn">Priority</a>
							<!-- Modal8 priority -->
								<div class="modal fade" id="myModal8<?php echo $tabphotoId; ?>" tabindex="-1" role="dialog">
									<div class="modal-dialog">
										<div class="modal-content">
											<div role="tabpanel" class="tabarea2">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title">Doctor Priority</h4>
												</div>
												<!-- Tab panes -->
												<div class="tab-content" id="tabPriority2<?php echo $tabphotoId; ?>">																			
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- Modal6 priority cls -->						                                                             
						</tr>
						<?php					
					}								
				}
				?>

			</tbody>

			</table>
			
		</div>
	</div>