<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_POST['hospital_name'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");
			}
		else
			{							
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;	
						
				$data['category_id']	=	$App->convert($_REQUEST['category']);
				$data['hospital_name']	=	$App->convert($_REQUEST['hospital_name']);
				$data['contact_number']	=	$App->convert($_REQUEST['contact_number']);
				$data['address']		=	$App->convert($_REQUEST['address']);
				$data['status']			= 1; 
	
				$img 		="";

				if($_FILES["photo"]["name"]){
					
					$file_name 		 				= 	pathinfo($_FILES["photo"]["name"]);
					$file_ext	 					=	$file_name["extension"];
					$rename 						= 	date("YmdHis");
					$new_file_name 					= 	$rename.".".$file_ext;
					if ($App->validateImages($file_ext)) {	
						
						if (!file_exists('../../Images/hospital')) {
							mkdir('../../Images/hospital', 0777, true);
							copy('http://www.pickit3d.com/sites/default/files/default.jpg', '../../Images/hospital/default_image.jpg');
						}
						
						if(move_uploaded_file($_FILES["photo"]["tmp_name"],"../../Images/hospital/".basename($new_file_name))){
							$img="Images/hospital/" . $new_file_name;		
						}
						else{
							$_SESSION['msg']="Opps...! Category creation failed";
							header("location:index.php");		
							return;	
						}
					}
					else{
						$_SESSION['msg']="Please upload a valid Image";
						header("location:index.php");		
						return;	
					} 
				}
												
				$data['image_url']			=	$App->convert($img);

				$success=$db->query_insert(TABLE_HOSPITAL, $data);
				$db->close();				
		
				if($success)
				{
					$_SESSION['msg']="Hospital added Successfully";										
				}
				else
				{
					$_SESSION['msg']="Failed";							
				}	
				header("location:index.php");		
			}
		break;
		
	// EDIT SECTION
	//-
	case 'edit':
		
		$fid	=	$_REQUEST['fid'];	       
		if(!$_POST['hospital_name'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
					
								
				$data['category_id']	=	$App->convert($_REQUEST['category']);
				$data['hospital_name']	=	$App->convert($_REQUEST['hospital_name']);
				$data['contact_number']	=	$App->convert($_REQUEST['contact_number']);
				$data['address']		=	$App->convert($_REQUEST['address']);
				$data['status']			= 1; 
				
				
				if($_FILES["photo"]["name"]){
					
					$file_name 		 				= 	pathinfo($_FILES["photo"]["name"]);
					$file_ext	 					=	$file_name["extension"];
					$rename 						= 	date("YmdHis");
					$new_file_name 					= 	$rename.".".$file_ext;
					if ($App->validateImages($file_ext)) {	
						
						if (!file_exists('../../Images/hospital')) {
							mkdir('../../Images/hospital', 0777, true);
							copy('http://www.pickit3d.com/sites/default/files/default.jpg', '../../Images/hospital/default_image.jpg');
						}
						
						if(move_uploaded_file($_FILES["photo"]["tmp_name"],"../../Images/hospital/".basename($new_file_name))){
							$img="Images/hospital/" . $new_file_name;
							$data['image_url']			=	$App->convert($img);

							//Delete Previous image
							$res=mysql_query("select image_url from ".TABLE_HOSPITAL." where ID='{$fid}'");
							$row=mysql_fetch_array($res);
							$image = '../../'.$row['image_url'];				
						}
						else{
							$_SESSION['msg']="Opps...! Category updation failed";
							header("location:edit.php?id=$fid");	
							return;	
						}
					}
					else{
						$_SESSION['msg']="Please upload a valid Image";
						header("location:edit.php?id=$fid");	
						return;	
					} 
				}
		

				$success=$db->query_update(TABLE_HOSPITAL, $data, "ID='{$fid}'");					 
				$db->close(); 				
				
				if($success)
				{
					if ($image) {
						if (file_exists($image)){
							unlink($image);
						}
					}
					$_SESSION['msg']="Hospital updated successfully";																	
				}
				else
				{
					$_SESSION['msg']="Failed";									
				}	
				header("location:index.php");	
				
									
			}
		
		break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				try
				{
					$res=mysql_query("select image_url from ".TABLE_HOSPITAL." where ID='{$id}'");
					$row=mysql_fetch_array($res);
					$photo="../../".$row['image_url'];	

					$success= @mysql_query("DELETE FROM `".TABLE_DOCTOR."` WHERE hospital_id='{$id}'");
					$success= @mysql_query("DELETE FROM `".TABLE_HOSPITAL_DEPARTMENT."` WHERE hospital_id='{$id}'");		
					$success= @mysql_query("DELETE FROM `".TABLE_HOSPITAL."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}				
				$db->close(); 
				
				
				if($success)
				{
					if (file_exists($photo)) 	
					{
						unlink($photo);					
					}
					$_SESSION['msg']="Hospital deleted successfully";									
				}
				else
				{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
				}	
				header("location:index.php");	
		break;
	// PHOTO UPLOAD
	case 'department':				
			    	
			if(!$_REQUEST['hospitalId'])
				{
					$_SESSION['msg']="Error, Invalid Details!";					
					header("location:index.php");	
				}
			else
				{				
					$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
					$db->connect();
					$success=0;
													
					$shopId		=	$App->convert($_REQUEST['hospitalId']);				
										
					$data['department']			=	$App->convert($_REQUEST['department']);		
					$data['hospital_id']		=	$shopId;						
					
					$success1=$db->query_insert(TABLE_HOSPITAL_DEPARTMENT,$data);											
					$db->close();												
					if($success1)
					{							
						$_SESSION['msg']="Department Added Successfully";										
					}
					else
					{						
						$_SESSION['msg']="Failed";											
					}
					header("location:index.php");
				}
																					
			break;	
// DELETE SHOP PHOTO
	case 'delPhoto':		
				$deleteId	=	$_REQUEST['deleteId'];
				//$shopId		=	$_REQUEST['sid'];
				$success1	=	0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
								
				try
				{ 										
					$success1= @mysql_query("DELETE FROM `".TABLE_HOSPITAL_DEPARTMENT."` WHERE ID='{$deleteId}'");																	     
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete this";				            
				}											
				$db->close(); 
				if($success1)
				{
					$_SESSION['msg']="Department deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete this";										
				}	
				header("location:index.php");					
		break;		
	// Doctor
	case 'doctor':				
			    	
			if(!$_REQUEST['hospitalId'])
				{
					$_SESSION['msg']="Error, Invalid Details!";					
					header("location:index.php");	
				}
			else
				{				
					$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
					$db->connect();
					$success=0;
													
					$shopId		=	$App->convert($_REQUEST['hospitalId']);				
										
					$data['department']			=	$App->convert($_REQUEST['department']);		
					$data['hospital_id']		=	$shopId;
					$data['doctor_name']		=	$App->convert($_REQUEST['doctor']);		
					$data['booking_number']		=	$App->convert($_REQUEST['booking']);		
					$data['status']				=	1;
					$data['worked_before']		=	$App->convert($_REQUEST['worked_before']);
					
					if (($_REQUEST["sun"])=="" ||($_REQUEST["sunt"])==""){
						$data['sun']			=	"Nt Avble";
					} 
					else{
						$sun					=	$App->convert($_REQUEST['sun']);
						$sunt					=	$App->convert($_REQUEST['sunt']);
						$data['sun']			=	$sun.'-'.$sunt;
					}

					if (($_REQUEST["mon"])=="" || ($_REQUEST["mont"])==""){
						$data['mon']			=	"Nt Avble";
					}
					else{
						$mon					=	$App->convert($_REQUEST['mon']);
						$mont					=	$App->convert($_REQUEST['mont']);
						$data['mon']			=	$mont.'-'.$mont;
					}
					
					if (($_REQUEST["tue"])=="" || ($_REQUEST["tuet"])==""){
						$data['tue']			=	"Nt Avble";
					}
					else{
						$tue					=	$App->convert($_REQUEST['tue']);
						$tuet					=	$App->convert($_REQUEST['tuet']);
						$data['tue']			=	$tue.'-'.$tuet;
					}	
					
					if (($_REQUEST["wed"])=="" || ($_REQUEST["wedt"])==""){
						$data['wed'] 					= 	"Nt Avble";
					}
					else{
						$wed					=	$App->convert($_REQUEST['wed']);
						$wedt					=	$App->convert($_REQUEST['wedt']);
						$data['wed']			=	$wed.'-'.$wedt;
					}	
					
					if (($_REQUEST["thur"])=="" || ($_REQUEST["thurt"])==""){
						$data['thur'] 			= 	"Nt Avble";
					}
					else{
						$thur					=	$App->convert($_REQUEST['thur']);
						$thurt					=	$App->convert($_REQUEST['thurt']);
						$data['thur']			=	$thur.'-'.$thurt;	
					}
					
					if (($_REQUEST["fri"])=="" || ($_REQUEST["frit"])==""){
						$data['fri'] 				= 	"Nt Avble";
						
					}	
					else{
						$fri						=	$App->convert($_REQUEST['fri']);
						$frit						=	$App->convert($_REQUEST['frit']);
						$data['fri']				=	$fri.'-'.$frit;
					}
					


					if (($_REQUEST["sat"])=="" || ($_REQUEST["sat"])==""){
						$data['sat']				=	"Nt Avble";
					}
					else{
						$sat						=	$App->convert($_REQUEST['sat']);
						$satt						=	$App->convert($_REQUEST['satt']);
						$data['sat']				=	$sat.'-'.$satt;
					}
					
					$success1=$db->query_insert(TABLE_DOCTOR,$data);											
					$db->close();									
					if($success1)
					{							
						$_SESSION['msg']="Doctor Added Successfully";										
					}
					else
					{						
						$_SESSION['msg']="Failed";											
					}
					
					header("location:index.php");
				}
																					
			break;	
// DELETE Doctor
	case 'delDoctor':		
				$deleteId	=	$_REQUEST['deleteId'];
				//$shopId		=	$_REQUEST['sid'];
				$success1	=	0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
								
				try
				{ 										
					$success1= @mysql_query("DELETE FROM `".TABLE_DOCTOR."` WHERE ID='{$deleteId}'");																	     
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete this";				            
				}											
				$db->close(); 
				if($success1)
				{
					$_SESSION['msg']="Doctor deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete this";										
				}	
				header("location:index.php");					
		break;	
			// Priority
	case 'priority':		
		$editId	=	$_REQUEST['shopId']; 
			    	
		if(!$_REQUEST['shopId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();

				$data['priority']	=   $App->convert($_REQUEST['priority']);
				
				$success1=$db->query_update(TABLE_HOSPITAL,$data," ID='{$editId}'");

				$db->close();
					
				if($success1)
				{							
					$_SESSION['msg']="Hospital Priority Updated Successfully";										
				}
				else{
					$_SESSION['msg']="Failed";
				}
				
				header("location:index.php");																
			}	
			break;		
			// Priority
	case 'priority2':		
		$editId	=	$_REQUEST['shopId2']; 
			    	
		if(!$_REQUEST['shopId2'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();

				$data['priority']	=   $App->convert($_REQUEST['priority']);
				
				$success1=$db->query_update(TABLE_DOCTOR,$data," ID='{$editId}'");

				$db->close();
					
				if($success1)
				{							
					$_SESSION['msg']="Doctor Priority Updated Successfully";										
				}
				else{
					$_SESSION['msg']="Failed";
				}
				
				header("location:index.php");																
			}	
			break;					
}
?>