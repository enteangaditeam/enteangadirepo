<?php
	require("../../config/config.inc.php"); 
	require("../../config/Database.class.php");
	require("../../config/Application.class.php");

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();


	$tableId2 	= 	$_REQUEST['shopId2'];
	$priority	=	$_REQUEST['priority2'];	
	$shopName 	=	$_REQUEST['shop2'];

?>

	<div role="tabpanel" class="tab-pane active" id="photo<?php echo $tableId2; ?>">
		<form action="do.php?op=priority2" class="form1" method="post" onsubmit="return valid()" enctype="multipart/form-data">
			<input type="hidden" name="shopId2" id="shopId2" value="<?php echo $tableId2 ?>">               								
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="shopName"><?= $shopName; ?></span></label>		                                  
					</div>
					<div class="form-group">		                      
						<label for="photo">Priority<span class="valid">*</span></label>
						<input type="number" min="1" step="1" name="priority"  id="priority" class="form-control2" required value="<?=$priority?>" >		                     
					</div>					
				</div> 	                  
			</div>              
			<div class="modal-footer">
				<input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
			</div>			          
		</form>
				
	</div>