<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_POST['group'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");
			}
		else
			{							
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$registration_number=$App->convert($_REQUEST['registration_number']);
				$existId=$db->existValuesId(TABLE_TAXI," registration_number='$registration_number'");
				if($existId>0)
				{					
					$_SESSION['msg']="Details already Exist !!";					
					header("location:index.php");				
				}
				else
				{	
					$img                            =   "";

					$data['category_id']			=	$App->convert($_REQUEST['group']);
					$data['driver_name']			=	$App->convert($_REQUEST['driver_name']);
					$data['taxi_name']				=	$App->convert($_REQUEST['taxi_name']);
					$data['registration_number']	=	$App->convert($_REQUEST['registration_number']);
					$data['contact_number']			=	$App->convert($_REQUEST['contact_number']);
					$data['license_number']			=	$App->convert($_REQUEST['license_number']);
					$data['badge_number']			=	$App->convert($_REQUEST['badge_number']);
					$data['place']					=	$App->convert($_REQUEST['place']);

					//Status is hard coded,Coz feature is not implimented now
					$data['status']					=	1;//$App->convert($_REQUEST['status']);
					
					if($_FILES["image_url"]["name"])
					{
						$file_name 		 				= 	pathinfo($_FILES["image_url"]["name"]);
						$file_ext	 					=	$file_name["extension"];
						$allowd_ext 					= 	array("jpeg","jpg","png");
						$rename 						= 	date("YmdHis");
						$new_file_name 					= 	$rename.".".$file_ext;

						if(in_array($file_ext,$allowd_ext) ) 	
						{
							if(move_uploaded_file($_FILES["image_url"]["tmp_name"],"../../Images/taxi/".basename($new_file_name)))
							{
								$img="Images/taxi/" . $new_file_name;
								$data['image_url']			=	$App->convert($img);
							}	
						}
						else
						{
							$db->close();
							$_SESSION['msg']="Invalid image";
							header("location:index.php");	
							return;
						}
					}	
					else
					{
						$data['image_url']				=	$App->convert($img);
					}				
					$success=$db->query_insert(TABLE_TAXI, $data);
					$db->close();				
				
					if($success)
					{
						$_SESSION['msg']="Taxi Added Successfully";										
					}
					else
					{
						$_SESSION['msg']="Failed";							
					}	
					header("location:index.php");		
				}
			}
		break;
		
	// EDIT SECTION
	//-
	case 'edit':
		
		$fid	=	$_REQUEST['fid'];	       
		if(!$_POST['group'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$registration_number=$App->convert($_REQUEST['registration_number']);
				$existId=$db->existValuesId(TABLE_TAXI," registration_number='$registration_number' AND ID!='$fid'");

				if($existId>0)
				{					
					$_SESSION['msg']="Details already Exist !!";					
					header("location:edit.php?id=$fid");						
					break ;
				}
				else
				{	
					$res=mysql_query("SELECT image_url from ".TABLE_TAXI." where ID='{$fid}'");
        			$row=mysql_fetch_array($res);
					$isImageUploaded 				= 0;	
				
					$img                            =   "";

					$data['category_id']			=	$App->convert($_REQUEST['group']);
					$data['driver_name']			=	$App->convert($_REQUEST['driver_name']);
					$data['taxi_name']				=	$App->convert($_REQUEST['taxi_name']);
					$data['registration_number']	=	$App->convert($_REQUEST['registration_number']);
					$data['contact_number']			=	$App->convert($_REQUEST['contact_number']);
					$data['license_number']			=	$App->convert($_REQUEST['license_number']);
					$data['badge_number']			=	$App->convert($_REQUEST['badge_number']);
					$data['place']					=	$App->convert($_REQUEST['place']);

					//Status is hard coded,Coz feature is not implimented now
					$data['status']					=	1;//$App->convert($_REQUEST['status']);
					

					if($_FILES["image_url"]["name"])
					{
						$file_name 		 				= 	pathinfo($_FILES["image_url"]["name"]);
						$file_ext	 					=	$file_name["extension"];
						$allowd_ext 					= 	array("jpeg","jpg","png");
						$rename 						= 	date("YmdHis");
						$new_file_name 					= 	$rename.".".$file_ext;
						$isValidImage 					=	$App->validateImages($file_ext);
						if($isValidImage) 	
						{
							if(move_uploaded_file($_FILES["image_url"]["tmp_name"],"../../Images/taxi/".basename($new_file_name)))
							{
								$img="Images/taxi/" . $new_file_name;
								$data['image_url']	=	$App->convert($img);
								$isImageUploaded 	=	1;
							}	
						}
						else
						{
							$db->close();
							$_SESSION['msg']="Invalid image";
							header("location:index.php");	
							return;
						}
					}
					$success=$db->query_update(TABLE_TAXI, $data,"ID='{$fid}'");
					$db->close(); 				
					if($success)
					{
						if ($row['image_url'] && $isImageUploaded ) 
						{
            				$image = '../../'.$row['image_url'];
            				if (file_exists($image))
							{
								unlink($image);
							}
						}
						$_SESSION['msg']="Details updated successfully";																	
					}
					else
					{
						$_SESSION['msg']="Failed";									
					}	
					header("location:index.php");	
				}
									
			}
		
		break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				try
				{	
					$res=mysql_query("SELECT image_url from ".TABLE_TAXI." where ID='{$id}'");
        			$row=mysql_fetch_array($res);		
					$success= @mysql_query("DELETE FROM `".TABLE_TAXI."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}				
				$db->close(); 
				
				
				if($success)
				{
					if ($row['image_url']) {
            			$image = '../../'.$row['image_url'];
            			if (file_exists($image))
						{
							unlink($image);
						}
					}
					$_SESSION['msg']="Taxi deleted successfully";									
				}
				else
				{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
				}	
				header("location:index.php");	
		break;	

				// Priority
	case 'priority':		
		$editId	=	$_REQUEST['shopId']; 
			    	
		if(!$_REQUEST['shopId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();

				$data['priority']	=   $App->convert($_REQUEST['priority']);
				
				$success1=$db->query_update(TABLE_TAXI,$data," ID='{$editId}'");

				$db->close();
					
				if($success1)
				{							
					$_SESSION['msg']="Taxi Priority Updated Successfully";										
				}
				else{
					$_SESSION['msg']="Failed";
				}
				
				header("location:index.php");																
			}	
			break;	
}
?>