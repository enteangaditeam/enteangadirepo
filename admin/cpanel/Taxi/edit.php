<?php 
	include("../adminHeader.php"); 
	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>


<?php
	if(isset($_SESSION['msg']))
	{?>
		<font color="red"><?php echo $_SESSION['msg']; ?></font><?php 
	}	
	$_SESSION['msg']='';
	$editId=$_REQUEST['id'];
	$tableEdit="SELECT * FROM ".TABLE_TAXI." WHERE ID='$editId'";
	$editField=mysql_query($tableEdit);
	$editRow=mysql_fetch_array($editField);
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="index.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">TAXI DETAILS</h4>
            </div>
            <div class="modal-body clearfix">
				<form method="post" action="do.php?op=edit" class="form1" enctype="multipart/form-data" onsubmit="return valid()">
				<input type="hidden" name="fid" id="fid" value="<?php echo $editId ?>">
			             
                <div class="row">
                 	<div class="col-sm-6">
                    	<div class="form-group">
                      		<label for="group">Category <span class="star">*</span></label>
                      		<select name="group" id="group" class="form-control2" required >
													<?php	
													$categoryTypes="select ID,category from ".TABLE_TAXI_CATEGORY."";
													$res2=mysql_query($categoryTypes);	
											
													while($row=mysql_fetch_array($res2))
													{?>	
														<option value="<?php echo $row['ID']?>" <?php if($editRow['category_id']== $row['ID']){?> selected="selected"<?php }?>><?php echo $row['category']?></option>
													<?php 									
													}?>										
													</select>				
					  							<span id="user-result"></span>
                    	</div>

											<div class="form-group">
                      <label for="driver_name">Driver name<span class="star">*</span></label>
                      <input type="text" id="driver_name" value="<?php echo $editRow['driver_name'];?>" placeholder="Driver name" name="driver_name" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="taxi_name">Taxi name<span class="star">*</span></label>
                      <input type="text" id="taxi_name" value="<?php echo $editRow['taxi_name'];?>"  name="taxi_name" placeholder="Taxi name" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
                      <label for="mobile">Contact Number<span class="star">*</span></label>
                      <input type="text" id="mobile" value="<?php echo $editRow['contact_number'];?>" name="contact_number" placeholder="Contact number" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="registration_number">Registration No<span class="star"></span></label>
                      <input type="registration_number" value="<?php echo $editRow['registration_number'];?>" id="registration_number" placeholder="Registration No" name="registration_number" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
                      <label for="license_number">Licence No<span class="star">*</span></label>
                      <input type="text" id="license_number" value="<?php echo $editRow['license_number'];?>" placeholder="License No" name="license_number" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>
										
										<div class="form-group">
                      <label for="badge_number">Badge No<span class="star"></span></label>
                      <input type="text" id="badge_number" value="<?php echo $editRow['badge_number'];?>" placeholder="Badge number" name="badge_number" class="form-control2">				
					  					<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="place">Place <span class="star">*</span></label>
                      <input type="text" id="place" value="<?php echo $editRow['place'];?>" placeholder="Place" name="place" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
                      <label for="image_url">Image</label>
                      <input type="file" id="image_url" name="image_url" class="form-control2">				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
											<?php 
											if($editRow['image_url'])
											{
												$path="../../".$editRow['image_url']; ?>
												<div style="max-width: 100px;"><img src="<?php echo $path ?>" alt="No Image" id="thumb" class="img-responsive" /></div>
											<?php }else{?>
											<div style="max-width: 100px;"><img src="../../img/student.png" alt="No Image"  class="img-responsive" /></div>
											<?php } ?>
										</div>

					</div>					 																								
            	</div>			
	 			<div>
            	</div>
            	<div class="modal-footer">
              		<input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            	</div>
				</form>
          	</div>
        </div>
      </div>
      <!-- Modal1 cls -->            
  </div>
<?php include("../adminFooter.php") ?>
