<?php 
	include("../adminHeader.php");
	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}
	$type		=	$_SESSION['LogType'];

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>
<script>
	function delete_type()
	{
		var del=confirm("Do you Want to Delete ?");
		if(del==true)
		{
			window.submit();
		}
		else
		{
			return false;
		}
	}
	
	// ajax for loading Priority
	function PriorityLoad(sid,prio,shop)
	{
		var xmlhttp;
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("tabPriority"+sid).innerHTML=xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","priority.php?shopId="+sid+"&priority="+prio+"&shop="+shop,true);
		xmlhttp.send();
	}	
</script>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-4"> 
          		<div class="clearfix">
					<h2 class="q-title">TAXI</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD NEW</a> 
				</div>
          </div>
          <div class="col-sm-8">
            <form method="post">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="form-control"  name="driver_name" placeholder="Driver name" value="<?php echo @$_REQUEST['driver_name'] ?>">
									</div>
								</div>
								<div class="col-md-6">
              		<div class="input-group">
									<?php
										//Select all category names
										$categoryTypes="select ID,category from ".TABLE_TAXI_CATEGORY."";
										$res2=mysql_query($categoryTypes);
									?>
									<select name="group" id="group" class="form-control" required style="height:48px" >							
										<option value="All">All</option>
										<?php
												while($row=mysql_fetch_array($res2))
												{?>	
													<option value="<?php echo $row['ID']?>"><?php echo $row['category']?></option>
												<?php 									
												}?>	            			
											</select>           						
									</select>             
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> 
								</div>
								</div>
							</row>
            </form>
          </div>
        </div>
 <?php	
$cond="1";
if(@$_REQUEST['group'])
{
	if($_POST['group']=="All")
	{
		$cond=$cond." and ".TABLE_TAXI.".driver_name like'%".$_POST['driver_name']."%'";
	}
	else
	{
		$cond=$cond." and ".TABLE_TAXI.".driver_name like'%".$_POST['driver_name']."%' AND ".TABLE_TAXI.".category_id=".$_POST['group']."";
	}
	
}

?>
</div>
        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table id="rowspan" class="tablesorter table view_limitter pagination_table" >
                <thead>
                  <tr >
										<th>Sl No</th>               
										<th>Category</th>
										<th>Driver Name</th>
										<th>Contact Number</th>	
										<th>Place</th>
										<th>Priority</th>					
										<th></th>																											
                  </tr>
                </thead>
                <tbody>
								<?php 
										$rowsPerPage=0;															
										$selAllQuery="SELECT ".TABLE_TAXI.".ID,".TABLE_TAXI.".priority ,".TABLE_TAXI.".badge_number,".TABLE_TAXI.".contact_number,".TABLE_TAXI.".driver_name,".TABLE_TAXI.".image_url ,".TABLE_TAXI.".license_number,".TABLE_TAXI.".place,".TABLE_TAXI.".registration_number,".TABLE_TAXI.".taxi_name,".TABLE_TAXI.".status,".TABLE_TAXI_CATEGORY.".category FROM ".TABLE_TAXI." JOIN ".TABLE_TAXI_CATEGORY." ON ".TABLE_TAXI_CATEGORY.".ID=".TABLE_TAXI.".category_id WHERE $cond ORDER BY ".TABLE_TAXI.".ID ASC";
																
										$selectAll= $db->query($selAllQuery);
										$number=mysql_num_rows($selectAll);
										if($number==0)
										{
										?>
											<tr>
												<td align="center" colspan="7">
													There is no details in list.
												</td>
											</tr>
										<?php
										}
										else
										{
											/*********************** for pagination ******************************/
												$rowsPerPage = ROWS_PER_PAGE;
												if(isset($_GET['page']))
												{
													$pageNum = $_GET['page'];
												}
												else
												{
													$pageNum =1;
												}
												$offset = ($pageNum - 1) * $rowsPerPage;
												$select1=$db->query($selAllQuery." limit $offset, $rowsPerPage");
												$i=$offset+1;
												//use '$select1' for fetching
												/*************************** for pagination **************************/			


											//$i=1;
											while($row=mysql_fetch_array($select1))
											{	
												$tableId=$row['ID'];	?>
												<tr>
												<td><?php echo $i++;?>
															<div class="adno-dtls"> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> |
																		<a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a> |
																		<a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>"> VIEW</a>
															</div>
															<!-- Modal3 -->
																	<div  class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
																	<div class="modal-dialog">
																		<div class="modal-content"> 																																					
																		<div role="tabpanel" class="tabarea2"> 
																			
																			<div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Taxi Details </h4>
            </div>
																			
																			<!-- Tab panes -->
																			<div class="tab-content">
																			<div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
																				<table class="table nobg">
																				<tbody style="background-color:#FFFFFF">
															
																					<tr>
																						<td>Taxi name</td>
																						<td> : </td>
																						<td><?php  echo $row['taxi_name']; ?></td>
																					</tr>
																					<tr>
																						<td>Driver name</td>
																						<td> : </td>
																						<td><?php  echo $row['driver_name']; ?></td>
																					</tr>
																					<tr>
																						<td>Contact</td>
																						<td> : </td>
																						<td><?php  echo $row['contact_number']; ?></td>
																					</tr>
																					<tr>
																						<td>Registration no</td>
																						<td> : </td>
																						<td><?php  echo $row['registration_number']; ?></td>
																					</tr>
																					<tr>
																						<td>License no</td>
																						<td> : </td>
																						<td><?php  echo $row['license_number']; ?></td>
																					</tr>
																					<tr>
																						<td>Badge no</td>
																						<td> : </td>
																						<td><?php  echo $row['badge_number']; ?></td>
																					</tr>
																					<tr>
																						<td>Place</td>
																						<td> : </td>
																						<td><?php  echo $row['place']; ?></td>
																					</tr>
																					
																					<tr>
																						<td>Photo</td>
																						<td> : </td>
																						<td><?php if($row['image_url']){
																									$path="../../".$row['image_url']; ?>
																									<div style="max-width: 100px;"><img src="<?php echo $path ?>" alt="No Image" class="img-responsive" /></div>
																									<?php }else{?>
																									<div style="max-width: 100px;"><img src="../../img/student.png" alt="No Image" class="img-responsive" /></div>
																									<?php } ?>
																						</td>
																					</tr>																					
																				</tbody>
																				</table>
																			</div>
																													
																			</div>
																		</div>
																		</div>
																	</div>
																	</div>
																	<!-- Modal3 cls --> 																					  						
													</td>
													<td><?php echo $row['category']; ?></td>
													<td><?php echo $row['driver_name']; ?></td>
													<td><?php echo $row['contact_number']; ?></td>
													<td><?php echo $row['place']; ?></td>	
													<td><?= $row['priority']; $pr=$row['priority']; $name=$row['driver_name']; ?></td>	
													<td style="width: 108px;" id="priority<?php echo $tableId; ?>" ><a href="#" onclick="PriorityLoad('<?php echo $tableId;?>','<?php echo $pr;?>','<?php echo $name;?>')" data-toggle="modal" data-target="#myModal6<?php echo $tableId; ?>" class="viewbtn">Priority</a>
													<!-- Modal6 priority -->
														<div class="modal fade" id="myModal6<?php echo $tableId; ?>" tabindex="-1" role="dialog">
															<div class="modal-dialog">
																<div class="modal-content">
																	<div role="tabpanel" class="tabarea2">
																		<div class="modal-header">
																			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																			<h4 class="modal-title">Taxi Priority</h4>
																		</div>
																		<!-- Tab panes -->
																		<div class="tab-content" id="tabPriority<?php echo $tableId; ?>">																			
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!-- Modal6 priority cls -->				                   	 	
												</tr>
										<?php }
									}?>                  
                </tbody>
              </table>
			  	
            </div>
						<?php 
										if($number>$rowsPerPage)
										{
										?>	
										<br />	
										<div style="text-align: center";>
											<div class="pagerSC" align="center" style="display: inline-block;">
										<?php
										
										$query   =  $db->query($selAllQuery);
										$numrows = mysql_num_rows($query);
										$maxPage = ceil($numrows/$rowsPerPage);
										$self = $_SERVER['PHP_SELF'];
										$nav  = '';
										if ($pageNum - 5 < 1) {
										$pagemin = 1;
										} else {
										$pagemin = $pageNum - 5;
										};
										if ($pageNum + 5 > $maxPage) {
										$pagemax = $maxPage;
										} else {
										$pagemax = $pageNum + 5;
										};
										
										$search=@$_REQUEST['search'];
										for($page = $pagemin; $page <= $pagemax; $page++)
										{
											if ($page == $pageNum)
											{
												$nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
											}
											else
											{
												if(@$search)
													{
													$nav .= " <a href=\"$self?page=$page&search=$search\">$page</a> ";
												}
												else
												{
													$nav .= " <a href=\"$self?page=$page\">$page</a> ";
												}
											}
										}
										?>
										<?php
										if ($pageNum > 1)
										{
											$page  = $pageNum - 1;
											if(@$search)
											{
												$prev  = " <a href=\"$self?page=$page&search=$search\">Prev</a> ";
												$first = " <a href=\"$self?page=1&search=$search\">First</a> ";
											}
											else
											{
												$prev  = " <a href=\"$self?page=$page\">Prev</a> ";
												$first = " <a href=\"$self?page=1\">First</a> ";
											}
										}
										else
										{
											$prev  = '&nbsp;';
											$first = '&nbsp;';
										}
										
										if ($pageNum < $maxPage)
										{
											$page = $pageNum + 1;
											if(@$search)
											{
													$next = " <a href=\"$self?page=$page&search=$search\">Next</a> ";
													$last = " <a href=\"$self?page=$maxPage&search=$search\">Last</a> ";
											}
											else
											{
													$next = " <a href=\"$self?page=$page\">Next</a> ";
													$last = " <a href=\"$self?page=$maxPage\">Last</a> ";
											}
										}
										else
										{
											$next = '&nbsp;';
											$last = '&nbsp;';
										}
										echo $first . $prev . $nav . $next . $last;
										?>
										<div style="clear: left;"></div>
										</div>	 
									</div>
									<?php
									}
									?>

			<!-- paging -->		
				<div style="clear:both;"></div>
				<div class="text-center">
					<div class="btn-group pager_selector"></div>
				</div>        
				<!-- paging end-->
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Taxi Details </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" enctype = "multipart/form-data" method="post" onsubmit="return valid()">
                <div class="row">
                  <div class="col-sm-6">

                    <div class="form-group">
                      <label for="courseName">Category <span class="star">*</span></label>
                      <select name="group" id="group" class="form-control2" required >	
												<?php
												$res2=mysql_query($categoryTypes);
												while($row=mysql_fetch_array($res2))
												{?>	
													<option value="<?php echo $row['ID']?>"><?php echo $row['category']?></option>
												<?php 									
												}?>	 									
												</select>				
					  					<span id="user-result"></span>
                    </div>	

										<div class="form-group">
                      <label for="driver_name">Driver name<span class="star">*</span></label>
                      <input type="text" id="driver_name" placeholder="Driver name" name="driver_name" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="name">Taxi name<span class="star">*</span></label>
                      <input type="text" id="taxi_name" name="taxi_name" placeholder="Taxi name" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
                      <label for="name">Contact Number<span class="star">*</span></label>
                      <input type="text" id="mobile" name="contact_number" placeholder="Contact number" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="place">Registration No<span class="star">*</span></label>
                      <input type="registration_number" id="registration_number" placeholder="Registration No" name="registration_number" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
                      <label for="place">Licence No<span class="star">*</span></label>
                      <input type="text" id="license_number" placeholder="License No" name="license_number" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>
										
										<div class="form-group">
                      <label for="place">Badge No</label>
                      <input type="text" id="badge_number" placeholder="Badge number" name="badge_number" class="form-control2">				
					  					<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="place">Place<span class="star">*</span></label>
                      <input type="text" id="place" placeholder="Place" name="place" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
                      <label for="place">Image<span class="star"></span></label>
                      <input type="file" id="image_url" name="image_url" class="form-control2">				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
											<img id="thumb" width="100" height="100" src="../../img/student.png" alt="your image" />
										</div>
									</div>
								</div>
			  				<div>
            		</div>
            		<div class="modal-footer">
              		<input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            		</div>
							</form>
          	</div>
        	</div>
      	</div>
	  	</div>
    </div>
<?php include("../adminFooter.php") ?>
