<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_POST['description'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");
			}
		else
			{							
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				$success=0;																	

				$allowed 	=  	array('jpg', 'JPG', 'jpeg', 'JPEG');	
				$result 	= 	date("YmdHis");

				$nimage2	=	$_FILES['photo2']['tmp_name'];
				$path_info2 =	pathinfo($_FILES["photo2"]["name"]);
				$ext2 		=	$path_info2["extension"];
	
				$upload_file2=$result.'_1.'.$ext2;

				if(in_array($ext2,$allowed)) 	
				{
					copy($nimage2,"../../Images/Ad/$upload_file2");
					$size = getimagesize("../../Images/Ad/$upload_file2");
					$w = $size[0];
					$h = $size[1];
					$normalwidth=$w/360;
					$normalthum=$w/480;
					$orginalwidth=$w/$normalwidth;
					$orginalhegiht=$h/$normalwidth;
					$test="$nimage2";
					$test=substr($test,'-3');
					if($w>380)
					{					  
						cropImage($orginalwidth, $orginalhegiht, "../../Images/Ad/$upload_file2", 'jpg', "../../Images/Ad/$upload_file2");					   
					}
					
					else
					{
						copy($nimage2,"../../Images/Ad/$upload_file2");
					}
					$data['Image_url2']				=	$App->convert("Images/Ad/$upload_file2");
                	$data['description']			=	$App->convert($_REQUEST['description']);
					$success=$db->query_insert(TABLE_AD, $data);
					$db->close(); 
				}
				else
				{
					$_SESSION['msg']= $ext2." Images are not allowed";
					header("location:index.php");
					return;
				}											
				
				if($success)
				{
					$_SESSION['msg']="Advertisement Added Successfully";										
				}
				else
				{
					$_SESSION['msg']="Failed";							
				}	
				header("location:index.php");		
		
			}
		break;
			
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				try
				{	
					$res=mysql_query("select image_url2 from ".TABLE_AD." where ID='{$id}'");
					$row=mysql_fetch_array($res);
					//$photo1="../../".$row['image_url1'];	
					$photo2="../../".$row['image_url2'];	
					
					if (file_exists($photo2)) 	
					{
						unlink($photo2);					
					} 			
					$success= @mysql_query("DELETE FROM `".TABLE_AD."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this Advertisement is used some where else";				            
				}				
				$db->close(); 
				
				
				if($success)
				{
					$_SESSION['msg']="Advertisement deleted successfully";									
				}
				else
				{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
				}	
				header("location:index.php");	
		break;	

		// Priority
	case 'priority':		
		$editId	=	$_REQUEST['shopId']; 
			    	
		if(!$_REQUEST['shopId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();

				$data['priority']	=   $App->convert($_REQUEST['priority']);
				
				$success1=$db->query_update(TABLE_AD,$data," ID='{$editId}'");

				$db->close();
					
				if($success1)
				{							
					$_SESSION['msg']="Ad Priority Updated Successfully";										
				}
				else{
					$_SESSION['msg']="Failed";
				}
				
				header("location:index.php");																
			}	
			break;	
}

function cropImage($w,$h,$filename,$stype,$dest)
{
	
	$filename = $filename;
	$width = $w;
	$height = $h;
	//echo $stype;die;
	switch($stype) 
	{
			case 'gif':
			$simg = imagecreatefromgif($filename);
			break;
			case 'jpg':
			$simg = imagecreatefromjpeg($filename);			
			break;
			case 'png':
			$simg = imagecreatefrompng($filename);
			break;
			
	}
	
	header('Content-type: image/jpeg');
	list($width_orig, $height_orig) = getimagesize($filename);
	$ratio_orig = $width_orig/$height_orig;
	if ($width/$height > $ratio_orig) 
	{
	   $width = $height*$ratio_orig;
	} 
	else 
	{
	   $height = $width/$ratio_orig;
	}	
	
	$image_p = imagecreatetruecolor($width, $height);	
	imagecopyresampled($image_p, $simg, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
	imagejpeg($image_p,$dest, 100);
}
?>