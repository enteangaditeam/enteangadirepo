<?php 
	include("../adminHeader.php");
	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}
	$type		=	$_SESSION['LogType'];

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>
<script>
	function delete_type()
	{
		var del=confirm("Do you Want to Delete ?");
		if(del==true)
		{
			window.submit();
		}
		else
		{
			return false;
		}
	}
	function loadMorePhoto(sid,pics,shop)
	{
		var xmlhttp;
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("photoDiv"+sid).innerHTML=xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","photo.php?shopId="+sid+"&pics="+pics+"&shop="+shop,true);
		xmlhttp.send();
	}
</script>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">HOTSPOT</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD NEW</a> 
				</div>
          </div>
          <div class="col-sm-4">
            <form method="post">
              <div class="input-group">
			  				<input type="text" class="form-control"  name="search" placeholder="Hotspot Name" value="<?php echo @$_REQUEST['search'] ?>">             
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> </div>
            </form>
          </div>
        </div>
 <?php	
$cond="1";
if(@$_REQUEST['search'])
{
	$cond=$cond." and ".TABLE_HOTSPOT.".hotspot_name like'%".$_REQUEST['search']."%'";
}

?>
        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
             <table id="rowspan" class="tablesorter table view_limitter pagination_table" >
                <thead>
                  <tr >
										<th>Sl No</th>               
										<th>Hotspot Name</th>
										<th>Place</th>	
										<th>Image</th>																											
                  </tr>
                </thead>
                <tbody>
								<?php 															
										$selCount="(SELECT COUNT(".TABLE_HOTSPOT_PHOTO.".ID) as numPic	FROM ".TABLE_HOTSPOT_PHOTO.",".TABLE_HOTSPOT." where ".TABLE_HOTSPOT_PHOTO.".hotspot_id=".TABLE_HOTSPOT.".ID )";
										$selCount= $db->query($selCount);
										while($row=mysql_fetch_array($selCount))
										{
											$numPics 		= $row['numPic'];
										}
										
										$selAllQuery="SELECT * FROM ".TABLE_HOTSPOT." WHERE status=1 and $cond ORDER BY ID ASC";
										$rowsPerPage = ROWS_PER_PAGE;						
										$selectAll= $db->query($selAllQuery);
										$number=mysql_num_rows($selectAll);
										if($number==0)
										{
										?>
											<tr>
												<td align="center" colspan="4">
													There is no details in list.
												</td>
											</tr>
										<?php
										}
										else
										{
											/*********************** for pagination ******************************/												
											if(isset($_GET['page']))
											{
												$pageNum = $_GET['page'];
											}
											else
											{
												$pageNum =1;
											}
											$offset = ($pageNum - 1) * $rowsPerPage;
											$select1=$db->query($selAllQuery." limit $offset, $rowsPerPage");
											$i=$offset+1;
											//use '$select1' for fetching
											/*************************** for pagination **************************/											
											while($row=mysql_fetch_array($select1))
											{	
												$tableId=$row['ID'];	
												
												?>
												<tr>
												<td><?php echo $i++;?>
															<div class="adno-dtls"> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> |
																		<a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a> |
																		<a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>"> VIEW</a>
															</div>	
																<!-- Modal3 -->
																	<div  class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
																	<div class="modal-dialog">
																		<div class="modal-content"> 																																					
																		<div role="tabpanel" class="tabarea2"> 
																			
																		<div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Hotspot Details</h4>
            </div>
																			
																			<!-- Tab panes -->
																			<div class="tab-content">
																			<div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
																				<table class="table nobg">
																				<tbody style="background-color:#FFFFFF">
															
																					<tr>
																						<td>Hotspot Name</td>
																						<td> : </td>
																						<td><?php  echo $row['hotspot_name']; ?></td>
																					</tr>
																					<tr>
																						<td>Description</td>
																						<td> : </td>
																						<td><?php  echo $row['description']; ?></td>
																					</tr>
																					<tr>
																						<td>Place</td>
																						<td> : </td>	
																						<td><?php  echo $row['place']; ?></td>
																					</tr>
																					
																					
																																										
																				</tbody>
																				</table>
																			</div>
																													
																			</div>
																		</div>
																		</div>
																	</div>
																	</div>
																	<!-- Modal3 cls --> 																				  						
													</td>
													<td><?php echo $row['hotspot_name']; ?></td>
													<td><?php echo $row['place']; ?></td>
                                                    <td style="width: 108px;" id="photo<?php echo $tableId; ?>" ><a href="#" onclick="loadMorePhoto('<?php echo $tableId;?>','<?php echo $numPics;?>','<?php echo $row['hotspot_name'];?>')" data-toggle="modal" data-target="#myModal5<?php echo $tableId; ?>" class="viewbtn">Photo</a>
													<!-- Modal3 photo -->
														<div class="modal fade" id="myModal5<?php echo $tableId; ?>" tabindex="-1" role="dialog">
															<div class="modal-dialog">
																<div class="modal-content">
																	<div role="tabpanel" class="tabarea2">
																		<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Shop Photos</h4>
            </div>
																		<!-- Tab panes -->
																		<div class="tab-content" id="photoDiv<?php echo $tableId; ?>">
																			
																	</div>
																</div>
															</div>
														</div>
													</div>
														<!-- Modal3 photo cls -->
													</td>
												</tr>
										<?php }
									}?>                  
								</tbody>
							</table>
								
							</div>
								<!-- **********************************************************************  -->
									<?php 
										if($number>$rowsPerPage)
										{
										?>	
										<br />	
										<div style="text-align: center";>
											<div class="pagerSC" align="center" style="display: inline-block;">
										<?php
										
										$query   =  $db->query($selAllQuery);
										$numrows = mysql_num_rows($query);
										$maxPage = ceil($numrows/$rowsPerPage);
										$self = $_SERVER['PHP_SELF'];
										$nav  = '';
										if ($pageNum - 5 < 1) {
										$pagemin = 1;
										} else {
										$pagemin = $pageNum - 5;
										};
										if ($pageNum + 5 > $maxPage) {
										$pagemax = $maxPage;
										} else {
										$pagemax = $pageNum + 5;
										};
										
										$search=@$_REQUEST['search'];
										for($page = $pagemin; $page <= $pagemax; $page++)
										{
											if ($page == $pageNum)
											{
												$nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
											}
											else
											{
												if(@$search)
													{
													$nav .= " <a href=\"$self?page=$page&search=$search\">$page</a> ";
												}
												else
												{
													$nav .= " <a href=\"$self?page=$page\">$page</a> ";
												}
											}
										}
										?>
										<?php
										if ($pageNum > 1)
										{
											$page  = $pageNum - 1;
											if(@$search)
											{
												$prev  = " <a href=\"$self?page=$page&search=$search\">Prev</a> ";
												$first = " <a href=\"$self?page=1&search=$search\">First</a> ";
											}
											else
											{
												$prev  = " <a href=\"$self?page=$page\">Prev</a> ";
												$first = " <a href=\"$self?page=1\">First</a> ";
											}
										}
										else
										{
											$prev  = '&nbsp;';
											$first = '&nbsp;';
										}
										
										if ($pageNum < $maxPage)
										{
											$page = $pageNum + 1;
											if(@$search)
											{
													$next = " <a href=\"$self?page=$page&search=$search\">Next</a> ";
													$last = " <a href=\"$self?page=$maxPage&search=$search\">Last</a> ";
											}
											else
											{
													$next = " <a href=\"$self?page=$page\">Next</a> ";
													$last = " <a href=\"$self?page=$maxPage\">Last</a> ";
											}
										}
										else
										{
											$next = '&nbsp;';
											$last = '&nbsp;';
										}
										echo $first . $prev . $nav . $next . $last;
										?>
										<div style="clear: left;"></div>
										</div>	 
									</div>
									<?php
									}
													?>
											
										<!-- ******************************************************************-->
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add Hotspot </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-sm-12">

                    <div class="form-group">
                      <label for="hotspot_name">Hotspot Name<span class="star">*</span></label>
                      <input type="text" id="hotspot_name" name="hotspot_name" class="form-control2" value=""  required placeholder="Hotspot Name" title="Hotspot Name" />				
					  					<span id="user-result"></span>
                    </div>	

					<div class="form-group">
                      <label for="Description">Description<span class="star">*</span></label>
                      <textarea id="description" name="description" class="form-control2" placeholder="Description" required></textarea>				
					  					<span id="user-result"></span>
                    </div>
					<div class="form-group">
                      <label for="Place">Place<span class="star">*</span></label>
                      <input id="place" name="place" class="form-control2" placeholder="Place" required>				
					  					<span id="user-result"></span>
                    </div>
											
					<div class="form-group">
																	<input type="checkbox" name="isViewable" id="isViewable">
																	<label for="contact_no">Show in Home</label>
																</div>
											
				</div>
				
								</div>
			  				<div>
            		</div>
            		<div class="modal-footer">
              		<input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            		</div>
							</form>
          	</div>
        	</div>
      	</div>
	  	</div>
<?php include("../adminFooter.php") ?>