<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_POST['hotspot_name'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");
			}
		else
			{							
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;			
				$isViewable                     = 0;
				if (isset($_POST['isViewable'])) {
					$isViewable                 =   1;
				}	
				$data['hotspot_name']			=	$App->convert($_REQUEST['hotspot_name']);
				$data['description']			=	$App->convert($_REQUEST['description']);
				$data['place']					=	$App->convert($_REQUEST['place']);
				$data['isViewable']				=	$App->convert($isViewable);
			

			
				$data['status']				=	1;
				
				$success=$db->query_insert(TABLE_HOTSPOT, $data);
					
				
				$db->close();				
			
				if($success)
				{
					$_SESSION['msg']="Hotspot Added Successfully";										
				}
				else
				{
					$_SESSION['msg']="Failed";							
				}	
				header("location:index.php");		
				
			}
		break;
		
	// EDIT SECTION
	//-
	case 'edit':
		
			$hot_id	=	$_REQUEST['hot_id'];	       
			if(!$_POST['hotspot_name'])
				{				
					$_SESSION['msg']="Error, Invalid Details!";					
					header("location:edit.php?id=$hot_id");	
				}
			else
				{				
					$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
					$db->connect();
					$success=0;
					$isViewable                     = 0;
					if (isset($_POST['isViewable'])) {
						$isViewable                 =   1;
					}		
				   $data['hotspot_name']			=	$App->convert($_REQUEST['hotspot_name']);
				   $data['description']			    =	$App->convert($_REQUEST['description']);
				   $data['place']			        =	$App->convert($_REQUEST['place']);
				   $data['isViewable']					=	$App->convert($isViewable);
					
					$data['status']				=	1;				
					$image_name = $_FILES['photo']['name'];
					if($image_name)
                    {
							
						$ext = pathinfo($image_name, PATHINFO_EXTENSION);
						$next	=	date("ymdhis");
						
							if(move_uploaded_file($_FILES["photo"]["tmp_name"],"../../Images/hotspot/".basename($next.'.'.$ext)))
								{
									$img="Images/hotspot/" . $next.'.'.$ext;		
									$data['image_url']			=	$App->convert($img);
									$tableEditQry	=  "SELECT *						  
										FROM ".TABLE_HOTSPOT."						  
										WHERE ".TABLE_HOTSPOT.".ID='$hot_id'";
					
									$tableEdit 	=	mysql_query($tableEditQry);
									$editRow	=	mysql_fetch_array($tableEdit); 
									$delete_image = $editRow['image_url'];
									$file= ("../../".$delete_image);
 									unlink($file);
								}								
								
							
					
					}
					
					$success=$db->query_update(TABLE_HOTSPOT,$data," ID='{$hot_id}'");				 
					//$db->close(); 				
				    

					//$qry = "DELETE FROM ".TABLE_NEWS_LOCATION." where news_id=$fid";
					//$resultQ = $db->query($qry);

			
					$db->close();	

					if($success)
					{
						$_SESSION['msg']="Details updated successfully";																	
					}
					else
					{
						$_SESSION['msg']="Failed";									
					}	
					header("location:index.php");						
										
				}
			
			break;
	// PHOTO UPLOAD
	case 'photo':				
			    	
			if(!$_REQUEST['shopId'])
				{
					$_SESSION['msg']="Error, Invalid Details!";					
					header("location:index.php");	
				}
			else
				{				
					$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
					$db->connect();
					$success=0;
													
					$shopId		=	$App->convert($_REQUEST['shopId']);				
					
					// upload file
					$date		= 	date("YmdHis");
					$path_info  = 	pathinfo($_FILES["photo"]["name"]);
					$ext	 	=	$path_info["extension"];	
					$allowed 	=  	array('bmp','jpg');		// allowed image extensions					
					$newName 	= 	$date.".".$ext;

					if ($App->validateImages($ext))	
					{
						$img='';	
						if(move_uploaded_file($_FILES["photo"]["tmp_name"],"../../Images/hotspot/".basename($newName)))
						{
							$img="Images/hotspot/" . $newName;		
						}
						$data['hotspot_id']			=	$shopId;		
						$data['image_url']			=	$App->convert($img);						
						
						$success1=$db->query_insert(TABLE_HOTSPOT_PHOTO,$data);	
						$db->close();								
					}
					
					if($success1)
					{							
						$_SESSION['msg']="Hot spot Photo Added Successfully";										
					}
					else
					{
						if(!in_array($ext,$allowed) ) 
						{
							$_SESSION['msg']="You have uploaded an invalid image file";
						}
						else
						{
							$_SESSION['msg']="Failed";	
						}						
					}
					header("location:index.php");
				}
																					
			break;		
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				

				$res=mysql_query("select image_url from ".TABLE_HOTSPOT." where ID='{$id}'");
				$row=mysql_fetch_array($res);
				$photo="../../".$row['image_url'];	
				if (file_exists($photo)) 	
				{
					unlink($photo);					
				} 					

			
				$success= @mysql_query("DELETE FROM `".TABLE_HOTSPOT."` WHERE ID='{$id}'");				      
			  					
				$db->close(); 
				
				
				if($success)
				{
					$_SESSION['msg']="Hotspot deleted successfully";									
				}
				else
				{
					$_SESSION['msg']="You can't Delete. ";									
				}	
				header("location:index.php");	
		break;
		// DELETE SHOP PHOTO
	case 'delPhoto':		
				$deleteId	=	$_REQUEST['deleteId'];
				$shopId		=	$_REQUEST['sid'];
				$success1	=	0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
								
				try
				{ 										
					$res=mysql_query("SELECT image_url from ".TABLE_HOTSPOT_PHOTO." where ID='{$deleteId}'");
					$row=mysql_fetch_array($res);
					$photo="../../".$row['image_url'];	
					$success1= @mysql_query("DELETE FROM `".TABLE_HOTSPOT_PHOTO."` WHERE ID='{$deleteId}'");
					if($success1)					
					{																																	
						if (file_exists($photo)) 	
						{
						unlink($photo);					
						} 						
					}														     
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete this";				            
				}											
				$db->close(); 
				if($success1)
				{
					$_SESSION['msg']="Shop Photo deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete this";										
				}	
				header("location:index.php");					
		break;	
}
?>