<?php 
	include("../adminHeader.php"); 
	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>


<?php
	if(isset($_SESSION['msg']))
	{?>
		<font color="red"><?php echo $_SESSION['msg']; ?></font><?php 
	}	
	$_SESSION['msg']='';
	$editId=$_REQUEST['id'];
	$tableEdit="SELECT * FROM ".TABLE_OFFER." WHERE ID='$editId'";
	$editField=mysql_query($tableEdit);
	$editRow=mysql_fetch_array($editField);
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="index.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">Offer Details </h4>
            </div>
            <div class="modal-body clearfix">
				<form method="post" action="do.php?op=edit" class="form1" enctype="multipart/form-data" onsubmit="return valid()">
				<input type="hidden" name="fid" id="fid" value="<?php echo $editId ?>">
			             
                <div class="row">
                 	<div class="col-sm-6">

											<div class="form-group">
                      <label for="driver_name">Offer Name<span class="star">*</span></label>
                      <input type="text" id="driver_name" value="<?php echo $editRow['name'];?>" placeholder="Offer Name" name="name" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="name">Description</label>
                      <textarea name="description" id="description" placeholder="Description" class="form-control2" cols="30" rows="10"><?php echo $editRow['description'];?></textarea>			
					  					<span id="user-result"></span>
                    </div>
											<div class="form-group">
                                <input type="checkbox" <?php if($editRow['isViewable']){ echo "checked" ;}?> name="isViewable" id="isViewable">
                                <label for="contact_no">Show in Home</label>
                            </div>
										<div class="form-group">
                      <label for="place">Image</label>
                      <input type="file" id="image_url" name="image_url" class="form-control2">				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
											<?php 
											if($editRow['image_url'])
											{
												$path="../../".$editRow['image_url']; ?>
												<div style="max-width: 100px;"><img id="thumb" src="<?php echo $path ?>" alt="No Image" class="img-responsive" /></div>
											<?php }else{?>
											<div style="max-width: 100px;"><img id="thumb" src="../../img/student.png" alt="No Image" class="img-responsive" /></div>
											<?php } ?>
										</div>

					</div>					 																								
            	</div>			
	 			<div>
            	</div>
            	<div class="modal-footer">
              		<input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            	</div>
				</form>
          	</div>
        </div>
      </div>
      <!-- Modal1 cls -->            
  </div>
<?php include("../adminFooter.php") ?>
