<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_POST['name'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");
			}
		else
			{							
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$isViewable                     = 0;
				if (isset($_POST['isViewable'])) {
					$isViewable                 =   1;
				}
				$name=$App->convert($_REQUEST['name']);
				$description=$App->convert($_REQUEST['description']);

				$img                            =   "";	
				$data['name']					=	$App->convert($_REQUEST['name']);
				$data['description']			=	$App->convert($_REQUEST['description']);
				$data['isViewable']		=	$App->convert($isViewable);
					
				//Status is hard coded,Coz feature is not implimented now
				$data['status']					=	1;
				
				if($_FILES["image_url"]["name"])
				{
					$file_name 		 				= 	pathinfo($_FILES["image_url"]["name"]);
					$file_ext	 					=	$file_name["extension"];
					$rename 						= 	date("YmdHis");
					$new_file_name 					= 	$rename.".".$file_ext;
						
					if ($App->validateImages($file_ext)) {
						if (!file_exists('../../Images/Offers')) {
							mkdir('../../Images/Offers', 0777, true);
							copy('http://www.pickit3d.com/sites/default/files/default.jpg', '../../Images/Offers/default_image.jpg');
						}
						
						if(move_uploaded_file($_FILES["image_url"]["tmp_name"],"../../Images/Offers/".basename($new_file_name))){
							$img="Images/Offers/" . $new_file_name;
							$data['image_url']			=	$App->convert($img);
						}
						else{
							$_SESSION['msg']="Opps...! Category creation failed";
							header("location:index.php");		
							return;	
						}
					}
					else{
						$_SESSION['msg']="Please upload a valid image";
						header("location:index.php");		
						return;
					}
				}
				
				$data['image_url']			=	$App->convert($img);
				$success=$db->query_insert(TABLE_OFFER, $data);
				
				$db->close();
				if($success)
				{
					$_SESSION['msg']="Offer Added Successfully";										
				}
				else
				{
					$_SESSION['msg']="Failed";							
				}	
				header("location:index.php");		
			}
			break;
		
	// EDIT SECTION
	//-
	case 'edit':
		
		$fid	=	$_REQUEST['fid'];	       
		if(!$_POST['name'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=".$fid);	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$isViewable                     = 0;
				if (isset($_POST['isViewable'])) {
					$isViewable                 =   1;
				}
				$imgRes = mysql_query("SELECT image_url FROM `".TABLE_OFFER."` WHERE ID = ".$fid);
            	$imgRow = mysql_fetch_array($imgRes);
            	$old_img = $imgRow['image_url'];

				$file_name 		 				= 	pathinfo($_FILES["image_url"]["name"]);
				$file_ext	 					=	$file_name["extension"];
				$allowd_ext 					= 	array("jpeg","jpg","png");
				$rename 						= 	date("YmdHis");
				$new_file_name 					= 	$rename.".".$file_ext;
				$img                            =   "";

				$data['name']					=	$App->convert($_REQUEST['name']);
				$data['description']			=	$App->convert($_REQUEST['description']);
				$data['isViewable']				=	$App->convert($isViewable);
					//Status is hard coded,Coz feature is not implimented now
				$data['status']					=	1;
					
				if(($_FILES["image_url"]["name"]))
				{
					if($App->validateImages($file_ext) ) 	
					{
						if (!file_exists('../../Images/Offers')) {
							mkdir('../../Images/Offers', 0777, true);
							copy('http://www.pickit3d.com/sites/default/files/default.jpg', '../../Images/Offers/default_image.jpg');
						}
						if(move_uploaded_file($_FILES["image_url"]["tmp_name"],"../../Images/Offers/".basename($new_file_name)))
						{
							$img="Images/Offers/" . $new_file_name;
							$data['image_url']	=	$App->convert($img);
						}
						else
						{
							$_SESSION['msg']="Error while uploading image.!!";
							header("location:edit.php?id=".$fid);		
							return;	
						}	
					}
					else
					{
						$_SESSION['msg']="Please upload valid image";
						header("location:edit.php?id=".$fid);
						return;
					}	
				}
				$success=$db->query_update(TABLE_OFFER, $data,"ID='{$fid}'");
				$db->close();
				if($success)
				{
					if ($old_img) {
                        $image = '../../'.$old_img;
                        if (file_exists($image)){
                        	unlink($image);
                    	}
                    }
					$_SESSION['msg']="Details updated successfully";																	
				}
				else
				{
					$_SESSION['msg']="Failed";									
				}	
				header("location:index.php");	
			}
		break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				try
				{			
					$imgRes = mysql_query("SELECT image_url FROM `".TABLE_OFFER."` WHERE ID = ".$id);
            		$imgRow = mysql_fetch_array($imgRes);
            		$old_img = $imgRow['image_url'];

					$success= @mysql_query("DELETE FROM `".TABLE_OFFER."` WHERE ID='{$id}'");			      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}				
				$db->close(); 
				
				
				if($success)
				{
					if ($old_img) {
                    	$image = '../../'.$old_img;
                        if (file_exists($image))
                        {
                        	unlink($image);
                        }
                    }
					$_SESSION['msg']="Offer deleted successfully";									
				}
				else
				{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
				}	
				header("location:index.php");	
		break;	
			// Priority
	case 'priority':		
		$editId	=	$_REQUEST['shopId']; 
			    	
		if(!$_REQUEST['shopId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();

				$data['priority']	=   $App->convert($_REQUEST['priority']);
				
				$success1=$db->query_update(TABLE_OFFER,$data," ID='{$editId}'");

				$db->close();
					
				if($success1)
				{							
					$_SESSION['msg']="Offer Priority Updated Successfully";										
				}
				else{
					$_SESSION['msg']="Failed";
				}
				
				header("location:index.php");																
			}	
			break;	
}
?>