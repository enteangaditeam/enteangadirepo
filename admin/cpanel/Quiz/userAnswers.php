<?php
include("../adminHeader.php");
if($_SESSION['LogID']=="")
{
    header("location:../../logout.php");
}
$type		=	$_SESSION['LogType'];

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
?>
<script>
    function delete_type()
    {
        var del=confirm("Do you Want to Delete ?");
        if(del==true)
        {
            window.submit();
        }
        else
        {
            return false;
        }
    }
</script>

<?php
if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }
$_SESSION['msg']='';
?>

<div class="col-md-10 col-sm-8 rightarea">
    <div class="row">
        <div class="col-sm-4">
            <div class="clearfix">
                <h2 class="q-title">User Answers</h2>
            </div>
        </div>
        <div class="col-sm-8">
            <form method="post" class="form-inline text-right">
                <div class="input-group" style="margin-right: 15px;">
                    <input name="correct_status" value="1" id="correct_status" type="checkbox" <?php if (@$_REQUEST['correct_status']) { ?>checked<?php } ?>>
                    <label for="correct_status">Show only correct answers</label>
                </div>
                <div class="input-group">
                    <input type="text" class="form-control"  name="search" placeholder="Name" value="<?php echo @$_REQUEST['search'] ?>">
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> </div>
            </form>
        </div>
    </div>
    <?php
    $cond="1";
    if(@$_REQUEST['search'])
    {
        $cond=$cond." AND quest.quiz_name LIKE '%".$_REQUEST['search']."%'";
    }
    ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="tablearea table-responsive">
                <table class="table" >
                    <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>User Name</th>
                        <th>Contact No</th>
                        <th>Email</th>
                        <th>Profile Photo</th>
                        <th>Quiz Name</th>
                        <th>Question</th>
                        <th>Answer</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //$selAllQuery="SELECT * FROM ".TABLE_QUIZ_QUESTION." WHERE $cond ORDER BY ID ASC";
                    $selAllQuery="SELECT usr.ID as usrID, usr.first_name, usr.last_name, usr.mobile, usr.email, usr.image_url, usrAns.status as winStatus, quest.ID as questID, quest.quiz_name, quest.question, opt.ID as optID, opt.options FROM `".TABLE_USER."` usr, `".TABLE_QUIZ_USER_ANSWER."` usrAns, `".TABLE_QUIZ_QUESTION."` quest, `".TABLE_QUIZ_OPTION."` opt WHERE (usr.ID = usrAns.user_id) AND (usrAns.question_id = quest.ID) AND (usrAns.option_id = opt.ID)  AND $cond ORDER BY usr.ID ASC";

                    $selectAll= $db->query($selAllQuery);
                    $number=mysql_num_rows($selectAll);
                    $rowsPerPage = 0;
                    if($number==0)
                    {
                        ?>
                        <tr>
                            <td align="center" colspan="9">
                                There is no details in the list.
                            </td>
                        </tr>
                        <?php
                    }
                    else
                    {
                        /*********************** for pagination ******************************/
                        $rowsPerPage = ROWS_PER_PAGE;
                        if(isset($_GET['page']))
                        {
                            $pageNum = $_GET['page'];
                        }
                        else
                        {
                            $pageNum =1;
                        }
                        $offset = ($pageNum - 1) * $rowsPerPage;
                        $select1=$db->query($selAllQuery." limit $offset, $rowsPerPage");
                        $i=$offset+1;
                        //use '$select1' for fetching
                        /*************************** for pagination **************************/
                        while($row=mysql_fetch_array($select1))
                        {
                            $tableId=$row['questID'];
                            $questId = $row['questID'];

                            $opId = $row['optID'];
                            $opRes = mysql_query("SELECT * FROM `".TABLE_QUIZ_QUESTION_ANSWER."` WHERE question_id = ".$questId." AND option_id = ".$opId);
                            if (mysql_num_rows($opRes) > 0) {
                                $answerTdHClass = "text-success";
                                $filterByCorrectAnswer = 1;
                            } else {
                                $answerTdHClass = "text-danger";
                                $filterByCorrectAnswer = 0;
                            }
                            if (!@$_REQUEST['correct_status']) {
                                ?>
                                <tr>
                                    <td><?php echo $i++;?>
                                        <!--<div class="adno-dtls">
                                        <a href="edit.php?id=<?php /*echo $tableId*/?>">EDIT</a> |
                                        <a href="do.php?id=<?php /*echo $tableId; */?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a>
                                    </div>-->
                                    </td>
                                    <td><?= $row['first_name'].' '.$row['last_name']; ?></td>
                                    <td><?= $row['mobile']; ?></td>
                                    <td><?= $row['email']; ?></td>
                                    <td><?php if ($row['image_url']) { ?> <img src="../../<?= $row['image_url']; ?>" width="90" height="90" alt="<?= $row['first_name'].' '.$row['last_name']; ?>"/> <?php } ?></td>
                                    <td><?= $row['quiz_name']; ?></td>
                                    <td><?= $row['question']; ?></td>
                                    <td>
                                        <h5 class="<?= $answerTdHClass; ?>"><?= $row['options']; ?></h5>
                                    </td>
                                    <td>
                                        <?php
                                        $status = $row['winStatus'];
                                        if ($status == 1) {
                                            ?>
                                            <h5 class="text-success">Winner</h5>
                                            <?php
                                        } else {
                                            $winnerCheck = 0;
                                            $winnerCheck = $db->existValuesId(TABLE_QUIZ_USER_ANSWER, "question_id = ". $questId ." AND status = 1");
                                            if ($winnerCheck) {
                                                ?>
                                                <h5 class="text-danger">Already selected winner.</h5>
                                                <?php
                                            } else if (!$winnerCheck && $filterByCorrectAnswer) {
                                                ?>
                                                <a href="do.php?uId=<?= $row['usrID']; ?>&qId=<?= $questId; ?>&op=winner" class="btn btn-success">Choose as winner</a>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            } else {
                                if ($filterByCorrectAnswer) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i++;?>
                                            <!--<div class="adno-dtls">
                                        <a href="edit.php?id=<?php /*echo $tableId*/?>">EDIT</a> |
                                        <a href="do.php?id=<?php /*echo $tableId; */?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a>
                                    </div>-->
                                        </td>
                                        <td><?= $row['first_name'].' '.$row['last_name']; ?></td>
                                        <td><?= $row['mobile']; ?></td>
                                        <td><?= $row['email']; ?></td>
                                        <td><?php if ($row['image_url']) { ?> <img src="../../<?= $row['image_url']; ?>" width="90" height="90" alt="<?= $row['first_name'].' '.$row['last_name']; ?>"/> <?php } ?></td>
                                        <td><?= $row['quiz_name']; ?></td>
                                        <td><?= $row['question']; ?></td>
                                        <td>
                                            <h5 class="<?= $answerTdHClass; ?>"><?= $row['options']; ?></h5>
                                        </td>
                                        <td>
                                            <?php
                                            $status = $row['winStatus'];
                                            if ($status == 1) {
                                                ?>
                                                <h5 class="text-success">Winner</h5>
                                                <?php
                                            } else {
                                                $winnerCheck = 0;
                                                $winnerCheck = $db->existValuesId(TABLE_QUIZ_USER_ANSWER, "question_id = ". $questId ." AND status = 1");
                                                if ($winnerCheck) {
                                                    ?>
                                                    <h5 class="text-danger">Already selected winner.</h5>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <a href="do.php?uId=<?= $row['usrID']; ?>&qId=<?= $questId; ?>&op=winner" class="btn btn-success">Choose as winner</a>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                        }
                    }?>
                    </tbody>
                </table>

            </div>
            <!-- **********************************************************************  -->
            <?php
            if($number>$rowsPerPage)
            {
                ?>
                <br />
                <div style="text-align: center";>
                    <div class="pagerSC" align="center" style="display: inline-block;">
                        <?php

                        $query   =  $db->query($selAllQuery);
                        $numrows = mysql_num_rows($query);
                        $maxPage = ceil($numrows/$rowsPerPage);
                        $self = $_SERVER['PHP_SELF'];
                        $nav  = '';
                        if ($pageNum - 5 < 1) {
                            $pagemin = 1;
                        } else {
                            $pagemin = $pageNum - 5;
                        };
                        if ($pageNum + 5 > $maxPage) {
                            $pagemax = $maxPage;
                        } else {
                            $pagemax = $pageNum + 5;
                        };

                        $search=@$_REQUEST['search'];
                        for($page = $pagemin; $page <= $pagemax; $page++)
                        {
                            if ($page == $pageNum)
                            {
                                $nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
                            }
                            else
                            {
                                if(@$search)
                                {
                                    $nav .= " <a href=\"$self?page=$page&search=$search\">$page</a> ";
                                }
                                else
                                {
                                    $nav .= " <a href=\"$self?page=$page\">$page</a> ";
                                }
                            }
                        }
                        ?>
                        <?php
                        if ($pageNum > 1)
                        {
                            $page  = $pageNum - 1;
                            if(@$search)
                            {
                                $prev  = " <a href=\"$self?page=$page&search=$search\">Prev</a> ";
                                $first = " <a href=\"$self?page=1&search=$search\">First</a> ";
                            }
                            else
                            {
                                $prev  = " <a href=\"$self?page=$page\">Prev</a> ";
                                $first = " <a href=\"$self?page=1\">First</a> ";
                            }
                        }
                        else
                        {
                            $prev  = '&nbsp;';
                            $first = '&nbsp;';
                        }

                        if ($pageNum < $maxPage)
                        {
                            $page = $pageNum + 1;
                            if(@$search)
                            {
                                $next = " <a href=\"$self?page=$page&search=$search\">Next</a> ";
                                $last = " <a href=\"$self?page=$maxPage&search=$search\">Last</a> ";
                            }
                            else
                            {
                                $next = " <a href=\"$self?page=$page\">Next</a> ";
                                $last = " <a href=\"$self?page=$maxPage\">Last</a> ";
                            }
                        }
                        else
                        {
                            $next = '&nbsp;';
                            $last = '&nbsp;';
                        }
                        echo $first . $prev . $nav . $next . $last;
                        ?>
                        <div style="clear: left;"></div>
                    </div>
                </div>
                <?php
            }
            ?>

            <!-- ******************************************************************-->



        </div>
    </div>
</div>



<?php include("../adminFooter.php") ?>
