<?php
include("../adminHeader.php");
if($_SESSION['LogID']=="")
{
    header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
?>


<?php
if(isset($_SESSION['msg']))
{?>
    <font color="red"><?php echo $_SESSION['msg']; ?></font><?php
}
$_SESSION['msg']='';
$editId=$_REQUEST['id'];
$tableEdit="SELECT * FROM ".TABLE_EVENT." WHERE ID='$editId'";
$editField=mysql_query($tableEdit);
$editRow=mysql_fetch_array($editField);
$eventDate = explode(' ', $editRow['time_and_date']);
$eventDate = substr(($App->dbformat_date_db_with_hyphen($eventDate[0]). ' '.$App->dbformat_time($eventDate[1])), 0, -3);

//Location query
$qry = "SELECT location_id FROM ".TABLE_EVENT_LOCATION."   WHERE event_id =$editId";

$locResult = $db->query($qry);
$sLocation = array();
while($locFetch = mysql_fetch_array($locResult))
{
    array_push($sLocation,$locFetch['location_id']);
}
?>


<!-- Modal1 -->
<div >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a class="close" href="index.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title">Edit Event Details</h4>
            </div>
            <div class="modal-body clearfix">
                <form method="post" action="do.php?op=edit" class="form1" enctype="multipart/form-data" onsubmit="return valid()">
                    <input type="hidden" name="fid" id="fid" value="<?php echo $editId ?>">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">Name:<span class="star">*</span></label>
                                <input type="text" id="name" name="name" class="form-control2" value="<?= $editRow['name']; ?>"  required placeholder="Name" />
                            </div>
                            <div class="form-group">
                                <label for="image">Change Image:</label>
                                <input type="file" id="image" class="show_progress" name="image" value="" />
                                <img class="image_upload_preview" src="../../<?= $editRow['image_url']; ?>" alt=""/>
                            </div>
                            <div class="form-group">
                                <label for="description">Description:<span class="star">*</span></label>
                                <textarea style="min-height: 100px;" id="description" name="description"  required placeholder="Description" class="form-control2"><?= $editRow['description']; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="venue">Venue:<span class="star">*</span></label>
                                <input type="text" id="venue" name="venue" class="form-control2" value="<?= $editRow['venu']; ?>"  required placeholder="Venue" />
                            </div>
                            <div class="form-group">
                                <label for="time_date">Time &amp; Date:<span class="star">*</span></label>
                                <input type="text" id="time_date" name="time_date" class="form-control2" value="<?= $eventDate; ?>"  required placeholder="Time and Date" />
                            </div>
                            <div class="form-group">
                                <label for="guest">Guest:</label>
                                <input type="text" id="guest" name="guest" class="form-control2" placeholder="Guest" value="<?= $editRow['guest']; ?>" />
                            </div>
                            <div class="form-group">
                                <label for="organizer">Organizer:</label>
                                <input type="text" id="organizer" name="organizer" class="form-control2" value="<?= $editRow['organizer']; ?>" placeholder="Organizer" />
                            </div>
                            <div class="form-group">
                                <label for="sponsor">Sponsor:</label>
                                <input type="text" id="sponsor" name="sponsor" class="form-control2" value="<?= $editRow['sponsor']; ?>" placeholder="Sponsor" />
                            </div>
                            <div class="form-group">
                                <label for="coordinator">Coordinator:</label>
                                <input type="text" id="coordinator" name="coordinator" class="form-control2" value="<?= $editRow['coordinator'] ?>" placeholder="Coordinator" />
                            </div>

                            <div class="form-group">
                                <label for="Location">Location<span class="star">*</span></label>
                                <ul class="category_combo_list list-unstyled" style="display: block;">
                                    <?php
                                    $selAllQuery="SELECT * FROM ".TABLE_LOCATION." ORDER BY location ASC";
                                    $selectAll= $db->query($selAllQuery);
                                    while($row=mysql_fetch_array($selectAll))
                                    { ?>
                                        <li><input type="checkbox" name="locations[]" value="<?= $row['ID'];?>" <?php if(in_array( $row['ID'],$sLocation)){ echo "checked" ;}?>>
                                            <label><?= $row['location']; ?></label></li>
                                    <?php	}
                                    ?>
                                </ul>
                            </div>

                        </div>

                        </div>
                    </div>
                    <div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal1 cls -->
</div>
<?php include("../adminFooter.php") ?>
