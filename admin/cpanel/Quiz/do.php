<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
    header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
    // NEW SECTION
    case 'new':

        if(!$_POST['quiz_name'])
        {
            $_SESSION['msg']="Error, Invalid Details!";
            header("location:index.php");
        }
        else
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success=0;
            $data['quiz_name'] = $App->convert($_REQUEST['quiz_name']);
            $data['question'] = $App->convert($_REQUEST['question']);
            $data['exp_date'] = $App->dbformat_date_db_with_hyphen($_REQUEST['exp_date']);
            $data['pub_date'] = $App->dbformat_date_db_with_hyphen($_REQUEST['pub_date']);
            $optionCount = $_REQUEST['option_count'];
            $success = $db->query_insert(TABLE_QUIZ_QUESTION, $data);
            if ($success) {
                $optSuccess = 0;
                for ($i = 1; $i <= $optionCount; $i++) {
                    $optData['question_id'] = $success;
                    $optData['options'] = $App->convert($_REQUEST['option_'.$i]);
                    $optSuccess = $db->query_insert(TABLE_QUIZ_OPTION, $optData);
                    if ($optSuccess && $_REQUEST['correct'] == $i) {
                        $ansSuccess = 0;
                        $ansData = array("question_id" => $success, "option_id" => $optSuccess);
                        $ansSuccess = $db->query_insert(TABLE_QUIZ_QUESTION_ANSWER, $ansData);
                    }
                }
            }
            if ($success && $optSuccess && $ansSuccess) {
                $_SESSION['msg']="Quiz Added Successfully";
            } else {
                $_SESSION['msg']="Invalid data! Please Try Again.";
            }
            header("location:index.php");

        }
        break;

    // EDIT SECTION
    //-
    case 'edit':

        $fid	=	$_REQUEST['fid'];
        if(!$_POST['name'])
        {
            $_SESSION['msg']="Error, Invalid Details!";
            header("location:edit.php?id=$fid");
        }
        else
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success=0;
            $imageValidity = 1;
            $data['name']			=	$App->convert($_REQUEST['name']);
            $data['description']			=	$App->convert($_REQUEST['description']);
            $data['venu']				=	$App->convert($_REQUEST['venue']);
            //$data['time_and_date']				=	date_create_from_format('d-m-Y H:i', $_REQUEST['time_date']);
            $eventDate = explode(' ', $_REQUEST['time_date']);
            $data['time_and_date'] = $App->dbformat_date_db($eventDate[0]). ' '.$App->dbformat_time($eventDate[1]);
            $data['guest']				=	$App->convert($_REQUEST['guest']);
            $data['organizer']				=	$App->convert($_REQUEST['organizer']);
            $data['sponsor']				=	$App->convert($_REQUEST['sponsor']);
            $data['coordinator']				=	$App->convert($_REQUEST['coordinator']);
            $data['status']				=	1;
            if ($_FILES['image']['name'] != "") {
                $path_info1 = pathinfo($_FILES["image"]["name"]);
                $ext1	 	=	$path_info1["extension"];
                if (!$App->validateImages($ext1)) {
                    $imageValidity = 0;
                } else {
                    $imgRes = mysql_query("SELECT image_url FROM `".TABLE_EVENT."` WHERE ID = ".$fid);
                    $imgRow = mysql_fetch_array($imgRes);
                    if ($imgRow['image_url']) {
                        $image = '../../'.$imgRow['image_url'];
                        if (file_exists($image))
                        {
                            unlink($image);
                        }
                    }

                    $result 	= date("YmdHis");
                    $newName 	= $result.".".$ext1;
                    $img 		="";
                    if(move_uploaded_file($_FILES["image"]["tmp_name"],"../../images/events/".basename($newName)))
                    {
                        $img="images/events/" . $newName;
                    }
                    $data['image_url']			=	$App->convert($img);
                }

            }
            if ($imageValidity) {
                $success=$db->query_update(TABLE_EVENT, $data, "ID='{$fid}'");
                //	$db->close();
                $qry = "DELETE FROM ".TABLE_EVENT_LOCATION." where event_id=$fid";
                $resultQ = $db->query($qry);

                $catArray 	=	$_REQUEST['locations'];
                //echo $catArray ;
                if(!empty($catArray))
                {
                    for($i=0;$i<count($catArray);$i++)
                    {
                        $data1['event_id']	=	$fid;
                        $data1['location_id']=	$catArray[$i];
                        $db->query_insert(TABLE_EVENT_LOCATION,$data1);
                    }
                }


                $db->close();

                if($success)
                {
                    $_SESSION['msg']="Event updated successfully";
                }
                else
                {
                    $_SESSION['msg']="Failed";
                }
            } else {
                $_SESSION['msg']="Please upload a valid image";
            }

            header("location:index.php");

        }

        break;

    // DELETE SECTION
    //-
    case 'delete':

        $id	=	$_REQUEST['id'];
        $success=0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();

        $success= @mysql_query("DELETE FROM `".TABLE_QUIZ_USER_ANSWER."` WHERE question_id='{$id}'");
        $success= @mysql_query("DELETE FROM `".TABLE_QUIZ_QUESTION_ANSWER."` WHERE question_id='{$id}'");
        $success= @mysql_query("DELETE FROM `".TABLE_QUIZ_OPTION."` WHERE question_id='{$id}'");
        $success= @mysql_query("DELETE FROM `".TABLE_QUIZ_QUESTION."` WHERE ID='{$id}'");
        $db->close();
        if($success)
        {
            $_SESSION['msg']="Quiz deleted successfully";
        }
        else
        {
            $_SESSION['msg']="Something went wrong!";
        }
        header("location:index.php");
        break;
    case 'winner':
        if (!$_REQUEST['uId'] || !$_REQUEST['qId']) {
            $_SESSION['msg']="Something went wrong!";
        } else {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $uId = $_REQUEST['uId'];
            $qId = $_REQUEST['qId'];
            $success=0;
            $data['status'] = 1;
            $success = $db->query_update(TABLE_QUIZ_USER_ANSWER, $data, "question_id='{$qId}' AND user_id='{$uId}'");
            if ($success) {
                $_SESSION['msg']="Winner selected successfully";
            } else {
                $_SESSION['msg']="Something went wrong!";
            }
        }
        header("location:userAnswers.php");
        break;
}
?>