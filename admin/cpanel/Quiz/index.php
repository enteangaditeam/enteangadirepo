<?php
include("../adminHeader.php");
if($_SESSION['LogID']=="")
{
    header("location:../../logout.php");
}
$type		=	$_SESSION['LogType'];

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
?>
<script>
    function delete_type()
    {
        var del=confirm("Do you Want to Delete ?");
        if(del==true)
        {
            window.submit();
        }
        else
        {
            return false;
        }
    }
</script>

<?php
if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }
$_SESSION['msg']='';
?>

<div class="col-md-10 col-sm-8 rightarea">
    <div class="row">
        <div class="col-sm-8">
            <div class="clearfix">
                <h2 class="q-title">Quiz</h2>
                <a href="#" class="addnew"  data-toggle="modal" data-target="#quiz_question"> <span class="plus">+</span> ADD NEW</a>
            </div>
        </div>
        <div class="col-sm-4">
            <form method="post">
                <div class="input-group">
                    <input type="text" class="form-control"  name="search" placeholder="Name" value="<?php echo @$_REQUEST['search'] ?>">
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> </div>
            </form>
        </div>
    </div>
    <?php
    $cond="1";
    if(@$_REQUEST['search'])
    {
        $cond=$cond." and ".TABLE_QUIZ_QUESTION.".quiz_name like'%".$_REQUEST['search']."%'";
    }

    ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="tablearea table-responsive">
                <table id="rowspan" class="tablesorter table view_limitter pagination_table" >
                    <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Quiz Name</th>
                        <th>Question</th>
                        <th>Answers</th>
                        <th>Publish Date</th>
                        <th>Expiry Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $selAllQuery="SELECT * FROM ".TABLE_QUIZ_QUESTION." WHERE $cond ORDER BY ID ASC";

                    $selectAll= $db->query($selAllQuery);
                    $number=mysql_num_rows($selectAll);
                    $rowsPerPage = 0;
                    if($number==0)
                    {
                        ?>
                        <tr>
                            <td align="center" colspan="6">
                                There is no questions in the list.
                            </td>
                        </tr>
                        <?php
                    }
                    else
                    {
                        /*********************** for pagination ******************************/
                            $rowsPerPage = ROWS_PER_PAGE;
                        if(isset($_GET['page']))
                        {
                            $pageNum = $_GET['page'];
                        }
                        else
                        {
                            $pageNum =1;
                        }
                        $offset = ($pageNum - 1) * $rowsPerPage;
                        $select1=$db->query($selAllQuery." limit $offset, $rowsPerPage");
                        $i=$offset+1;
                        //use '$select1' for fetching
                        /*************************** for pagination **************************/
                        while($row=mysql_fetch_array($select1))
                        {
                            $tableId=$row['ID'];
                            ?>
                            <tr>
                                <td><?php echo $i++;?>
                                    <div class="adno-dtls">
                                        <!--<a href="edit.php?id=<?php /*echo $tableId*/?>">EDIT</a> |-->
                                        <a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a>
                                    </div>
                                </td>
                                <td><?= $row['quiz_name']; ?></td>
                                <td><?= $row['question']; ?></td>
                                <td>
                                    <?php
                                    $opRes = mysql_query("SELECT * FROM `".TABLE_QUIZ_OPTION."` WHERE question_id = ".$tableId);
                                    if (mysql_num_rows($opRes) > 0) {
                                        ?>
                                        <ul>
                                        <?php
                                        while ($opRow = mysql_fetch_array($opRes)) {
                                            $opId = $opRow['ID'];
                                            $correct = false;
                                            $correctRes = mysql_query("SELECT * FROM `".TABLE_QUIZ_QUESTION_ANSWER."` WHERE question_id = ".$tableId." AND option_id = ".$opId);
                                            if (mysql_num_rows($correctRes) > 0) {
                                                $correct = true;
                                            }
                                            ?>
                                            <li class="<?php if ($correct) { ?>text-success<?php } else { ?>text-danger<?php } ?>"><?= $opRow['options'] ?></li>
                                            <?php
                                        }
                                        ?>
                                        </ul>
                                        <?php
                                    }

                                    ?>
                                </td>
                                <td><?= $App->dbformat_date_db_with_hyphen($row['pub_date']); ?></td>
                                <td><?= $App->dbformat_date_db_with_hyphen($row['exp_date']); ?></td>
                            </tr>
                        <?php }
                    }?>
                    </tbody>
                </table>

            </div>
            <!-- **********************************************************************  -->
            <?php
            if($number>$rowsPerPage)
            {
                ?>
                <br />
                <div style="text-align: center";>
                    <div class="pagerSC" align="center" style="display: inline-block;">
                        <?php

                        $query   =  $db->query($selAllQuery);
                        $numrows = mysql_num_rows($query);
                        $maxPage = ceil($numrows/$rowsPerPage);
                        $self = $_SERVER['PHP_SELF'];
                        $nav  = '';
                        if ($pageNum - 5 < 1) {
                            $pagemin = 1;
                        } else {
                            $pagemin = $pageNum - 5;
                        };
                        if ($pageNum + 5 > $maxPage) {
                            $pagemax = $maxPage;
                        } else {
                            $pagemax = $pageNum + 5;
                        };

                        $search=@$_REQUEST['search'];
                        for($page = $pagemin; $page <= $pagemax; $page++)
                        {
                            if ($page == $pageNum)
                            {
                                $nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
                            }
                            else
                            {
                                if(@$search)
                                {
                                    $nav .= " <a href=\"$self?page=$page&search=$search\">$page</a> ";
                                }
                                else
                                {
                                    $nav .= " <a href=\"$self?page=$page\">$page</a> ";
                                }
                            }
                        }
                        ?>
                        <?php
                        if ($pageNum > 1)
                        {
                            $page  = $pageNum - 1;
                            if(@$search)
                            {
                                $prev  = " <a href=\"$self?page=$page&search=$search\">Prev</a> ";
                                $first = " <a href=\"$self?page=1&search=$search\">First</a> ";
                            }
                            else
                            {
                                $prev  = " <a href=\"$self?page=$page\">Prev</a> ";
                                $first = " <a href=\"$self?page=1\">First</a> ";
                            }
                        }
                        else
                        {
                            $prev  = '&nbsp;';
                            $first = '&nbsp;';
                        }

                        if ($pageNum < $maxPage)
                        {
                            $page = $pageNum + 1;
                            if(@$search)
                            {
                                $next = " <a href=\"$self?page=$page&search=$search\">Next</a> ";
                                $last = " <a href=\"$self?page=$maxPage&search=$search\">Last</a> ";
                            }
                            else
                            {
                                $next = " <a href=\"$self?page=$page\">Next</a> ";
                                $last = " <a href=\"$self?page=$maxPage\">Last</a> ";
                            }
                        }
                        else
                        {
                            $next = '&nbsp;';
                            $last = '&nbsp;';
                        }
                        echo $first . $prev . $nav . $next . $last;
                        ?>
                        <div style="clear: left;"></div>
                    </div>
                </div>
                <?php
            }
            ?>

            <!-- ******************************************************************-->



        </div>
    </div>
</div>

<!-- Modal1 -->
<div class="modal fade" id="quiz_question" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Question</h4>
            </div>
            <div class="modal-body clearfix">
                <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="quiz_name">Quiz Name:<span class="star">*</span></label>
                                <input type="text" id="quiz_name" name="quiz_name" class="form-control2" value=""  required placeholder="Quiz name" />
                            </div>
                            <div class="form-group">
                                <label for="question">Question:<span class="star">*</span></label>
                                <input type="text" id="question" name="question" class="form-control2" value=""  required placeholder="Question" />
                            </div>
                            <div class="form-group">
                                <label for="exp_date_pick">Expiry Date:<span class="star">*</span></label>
                                <input type="text" autocomplete="off" id="exp_date_pick" name="exp_date" class="form-control2 date_pick" value=""  required placeholder="Expiry Date" />
                            </div>
                            <div class="form-group">
                                <label for="pub_date_pick">Publish Date:<span class="star">*</span></label>
                                <input type="text" autocomplete="off" id="pub_date_pick" name="pub_date" class="form-control2 date_pick" value=""  required placeholder="Publish Date" />
                            </div>
                            <div class="form-group">
                                <label>Options:<span class="star">*</span></label>
                                <div class="option_wrap table-responsive">
                                    <table id="opt_table" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th colspan="3" class="text-right">
                                                    <button class="btn btn-success" type="button" id="add_quiz_option">Add Option</button>
                                                    <input type="hidden" name="option_count" value="2">
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>Correct Option</th>
                                                <th>Option</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><input type="radio" name="correct" value="1" required></td>
                                            <td><input type="text" name="option_1" class="form-control2" required /></td>
                                            <td><a href="#" data-action="delete" class="btn btn-danger">Delete</a></td>
                                        </tr>
                                        <tr>
                                            <td><input type="radio" name="correct" value="2"></td>
                                            <td><input type="text" name="option_2" class="form-control2" required /></td>
                                            <td><a href="#" data-action="delete" class="btn btn-danger">Delete</a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<!-- Detailed view for events -->
<!-- Modal1 -->
<div class="modal fade" id="event_view" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Events - Detailed View</h4>
            </div>
            <div class="modal-body clearfix">

            </div>
        </div>
    </div>
</div>
<?php include("../adminFooter.php") ?>
