  <div id="footer">
    <div class="container">
      <div class="powerd">
        <p>Powered By </p>
        <div class="powerdimg"><a href="http://www.bodhiinfo.com/" target="_blank"><img src="../../img/bodhi.png"  alt=""/></a></div>
      </div>
    </div>
  </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="../../js/bootstrap.min.js"></script>
<script src="../../xdsoft_date/jquery.datetimepicker.full.min.js"></script>
<script src="../../js/bootstrap-datepicker.min.js"></script>

<script src="../../js/validation.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.timepicker/0.2.6/js/bootstrap-timepicker.min.js"></script>
<script src="../../js/main.js"></script> 
</body>
</html>