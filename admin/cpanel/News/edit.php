<?php 
	include("../adminHeader.php"); 
	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>


<?php
	if(isset($_SESSION['msg']))
	{?>
		<font color="red"><?php echo $_SESSION['msg']; ?></font><?php 
	}	
	$_SESSION['msg']='';
	$editId=$_REQUEST['id'];
	$tableEdit="SELECT * FROM ".TABLE_NEWS." WHERE ID='$editId'";
	$editField=mysql_query($tableEdit);
	$editRow=mysql_fetch_array($editField);

	//Location query 
	$qry = "SELECT location_id FROM ".TABLE_NEWS_LOCATION."   WHERE news_id =$editId";
								
	$locResult = $db->query($qry);
	$sLocation = array();
	while($locFetch = mysql_fetch_array($locResult))
	{	 		
	 		array_push($sLocation,$locFetch['location_id']);
	}	
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="index.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">News </h4>
            </div>
            <div class="modal-body clearfix">
				<form method="post" action="do.php?op=edit" class="form1" enctype="multipart/form-data" onsubmit="return valid()">
				<input type="hidden" name="fid" id="fid" value="<?php echo $editId ?>">			             									 

                <div class="row">
                 	<div class="col-sm-6">
                    	<div class="form-group">
                      <label for="Heading">Heading<span class="star">*</span></label>
                      <input type="text" id="heading" name="heading" class="form-control2" value="<?php echo $editRow['heading'];?>"  required placeholder="Heading" title="Heading" />				
					  					<span id="user-result"></span>
                    </div>	

										<div class="form-group">
                      <label for="Description">Description<span class="star">*</span></label>
                      <textarea id="description" name="description" class="form-control2"  placeholder="Description" required><?php echo $editRow['content'];?></textarea>				
					  					<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="Date">Date<span class="star">*</span></label>
                      <input type="text" id="newsDate" name="newsDate" class="form-control2 datepicker" value="<?php echo $App->dbformat_date_db($editRow['date']);?>" required readonly value="<?php echo date("d/m/Y");?>"  >				
					  					<span id="user-result"></span>
                    </div>	

										<div class="form-group">
                      <label for="Photo">Photo</label>
                      <input type="file" id="Photo" class="show_progress" name="photo" value="" />	  				
					  <img class="image_upload_preview" src="../../<?= $editRow['photo_url']; ?>" alt=""/>
                    </div>

										<div class="form-group">
                      <label for="breaking">Is this Breaking News</label>
                      <input type="checkbox" name="breaking" id="breaking" <?php if( $editRow['breaking']==1){?> checked <?php } ?> >			
					  					<span id="user-result"></span>
                    </div>	

										<div class="form-group">
                      <label for="Location">Location</label>
											<ul class="category_combo_list list-unstyled" style="display: block;">
												<?php 
														$selAllQuery="SELECT * FROM ".TABLE_LOCATION." ORDER BY location ASC";
														$selectAll= $db->query($selAllQuery);
														while($row=mysql_fetch_array($selectAll))
														{ ?>
															<li><input type="checkbox" name="locations[]" id="locations" value="<?= $row['ID'];?>" <?php if(in_array( $row['ID'],$sLocation)){ echo "checked" ;}?>>  
                        			<label><?= $row['location']; ?></label></li>
													<?php	}	
												?>
												</ul>					  																                     
                    </div>		

									</div>
								</div>
			  				<div>
            		</div>
            	<div class="modal-footer">
              		<input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            	</div>
				</form>
          	</div>
        </div>
      </div>
      <!-- Modal1 cls -->            
  </div>
<?php include("../adminFooter.php") ?>
