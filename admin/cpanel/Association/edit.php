<?php 
	include("../adminHeader.php"); 
	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>


<?php
	if(isset($_SESSION['msg']))
	{?>
		<font color="red"><?php echo $_SESSION['msg']; ?></font><?php 
	}	
	$_SESSION['msg']='';
	$editId=$_REQUEST['id'];
	$tableEdit="SELECT * FROM ".TABLE_ASSOCIATION." WHERE ID='$editId'";
	$editField=mysql_query($tableEdit);
	$editRow=mysql_fetch_array($editField);
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="index.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">ASSociation Details </h4>
            </div>
            <div class="modal-body clearfix">
				<form method="post" action="do.php?op=edit" class="form1" enctype="multipart/form-data" onsubmit="return valid()">
				<input type="hidden" name="fid" id="fid" value="<?php echo $editId ?>">
			             
                <div class="row">
                 	<div class="col-sm-6">
                    	<div class="form-group">
                      <label for="location">Location<span class="star">*</span></label>
											<select name="location" id="location" class="form-control2" required >
											<?php
											$location="SELECT ID,location FROM ".TABLE_LOCATION."";
											$res2=mysql_query($location);	
											
											while($row=mysql_fetch_array($res2))
											{?>	
													<option value="<?php echo $row['ID']?>" <?php if($editRow['location_id']== $row['ID']){?> selected="selected"<?php }?>><?php echo $row['location']?></option>
											<?php 									
											}?>	            			
											</select>
										</div>
										<div class="form-group">
                      <label for="association_type">association Category<span class="star">*</span></label>
											<select name="association_type" id="association_type" class="form-control2" required >
											<?php
											$location="SELECT ID,association_type FROM ".TABLE_ASSOCIATION_TYPE."";
											$res2=mysql_query($location);	
											
											while($row=mysql_fetch_array($res2))
											{?>	
													<option value="<?php echo $row['ID']?>" <?php if($editRow['association_type_id']== $row['ID']){?> selected="selected"<?php }?>><?php echo $row['association_type']?></option>
											<?php 									
											}?>	            			
											</select>
										</div>	

                    <div class="form-group">
                      <label for="Heading">Name<span class="star">*</span></label>
                      <input type="text" id="name" value="<?php echo $editRow['name'];?>" placeholder="name" title="Association name" name="name" class="form-control2" value=""  required placeholder="Association Name" title="Heading" />				
					  					<span id="user-result"></span>
                    </div>	

										<div class="form-group">
                      <label for="concern_person_name">Concern person<span class="star">*</span></label>
                      <input type="text" id="concern_person_name" value="<?php echo $editRow['concern_person_name'];?>" placeholder="concern_person_name" title="concern_person_name" name="concern_person_name" placeholder="concern person" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>
										
										<div class="form-group">
                      <label for="contact_number">Contact Number<span class="star">*</span></label>
                      <input type="text" id="contact_number"  value="<?php echo $editRow['contact_number'];?>" placeholder="contact number" title="contact_number" name="contact_number" placeholder="contact number" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="description">description<span class="star">*</span></label>
                      <textarea id="services" name="description" placeholder="description" title="Services" class="form-control2" placeholder="description" required><?php echo $editRow['description'];?></textarea>				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
                      <label for="Photo">Photo</label>
                      <input type="file" name="photo" id="image_url" >		  				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
											<?php 
											if($editRow['photo_url'])
											{
												$path="../../".$editRow['photo_url']; ?>
												<div style="max-width: 255px;"><img src="<?php echo $path ?>" alt="No Image" id="thumb" class="img-responsive" /></div>
											<?php }else{?>
											<div style="max-width: 255px;"><img src="../../img/student.png" alt="No Image"  class="img-responsive" /></div>
											<?php } ?>
										</div>
										
										<div class="form-group">
                      <label for="policy">policy<span class="star">*</span></label>
                      <textarea id="policy" title="policy" name="policy" placeholder="policy"  class="form-control2" placeholder="policy" required><?php echo $editRow['policy'];?></textarea>				
					  					<span id="user-result"></span>
                    </div>

					</div>					 																								
            	</div>			
	 			<div>
            	</div>
            	<div class="modal-footer">
              		<input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            	</div>
				</form>
          	</div>
        </div>
      </div>
      <!-- Modal1 cls -->            
  </div>
<?php include("../adminFooter.php") ?>
