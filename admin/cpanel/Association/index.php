<?php 
	include("../adminHeader.php");
	if($_SESSION['LogID']=="")
	{
		header("location:../../logout.php");
	}
	$type		=	$_SESSION['LogType'];

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
?>
<script>
	function delete_type()
	{
		var del=confirm("Do you Want to Delete ?");
		if(del==true)
		{
			window.submit();
		}
		else
		{
			return false;
		}
	}

	// ajax for loading Priority
	function PriorityLoad(sid,prio,shop)
	{
		var xmlhttp;
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("tabPriority"+sid).innerHTML=xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","priority.php?shopId="+sid+"&priority="+prio+"&shop="+shop,true);
		xmlhttp.send();
	}	
</script>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">Association</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD New</a> 
				</div>
          </div>
          <div class="col-sm-4">
            <form method="get">
              <div class="input-group">
			  				<input type="text" class="form-control"  name="name" placeholder="Association Name" value="<?php echo @$_REQUEST['office_name'] ?>">             
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> </div>
            </form>
          </div>
        </div>
 <?php	
$cond="1";
if(@$_REQUEST['name'])
{
	$cond=$cond." and ".TABLE_ASSOCIATION.".name like'%".$_REQUEST['name']."%'";
}

?>
        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table id="rowspan" class="tablesorter table view_limitter pagination_table " >
                <thead>
                  <tr >
										<th>Sl No</th>               
										<th>name</th>
										<th>concern person</th>	
										<th>contact number</th>
										<th>Location</th>	
										<th>Priority</th>					
										<th></th>																										
                  </tr>
                </thead>
                <tbody>
								<?php
								//JOIN ".TABLE_ASSOCIATION_TYPE.".ID = ".TABLE_ASSOCIATION.".association_type_id
								//													
										$selAllQuery="SELECT ".TABLE_ASSOCIATION.".ID,".TABLE_ASSOCIATION.".priority,".TABLE_ASSOCIATION.".photo_url,".TABLE_ASSOCIATION_TYPE.".association_type,".TABLE_ASSOCIATION.".description,".TABLE_ASSOCIATION.".concern_person_name,".TABLE_ASSOCIATION.".name,".TABLE_LOCATION.".location,".TABLE_ASSOCIATION.".contact_number FROM ".TABLE_ASSOCIATION." JOIN ".TABLE_LOCATION." ON ".TABLE_ASSOCIATION.".location_id =".TABLE_LOCATION.".ID JOIN ".TABLE_ASSOCIATION_TYPE." ON ".TABLE_ASSOCIATION_TYPE.".ID=".TABLE_ASSOCIATION.".association_type_id WHERE $cond ORDER BY ".TABLE_ASSOCIATION.".ID ASC";
																
										$selectAll= $db->query($selAllQuery);
										$number=mysql_num_rows($selectAll);
										if($number==0)
										{
										?>
											<tr>
												<td align="center" colspan="5">
													There is no details in list.
												</td>
											</tr>
										<?php
										}
										else
										{
										/*********************** for pagination ******************************/
												$rowsPerPage = ROWS_PER_PAGE;
												if(isset($_GET['page']))
												{
													$pageNum = $_GET['page'];
												}
												else
												{
													$pageNum =1;
												}
												$offset = ($pageNum - 1) * $rowsPerPage;
												$select1=$db->query($selAllQuery." limit $offset, $rowsPerPage");
												$i=$offset+1;
												//use '$select1' for fetching
												/*************************** for pagination **************************/
											while($row=mysql_fetch_array($select1))
											{	
												$tableId=$row['ID'];	?>
												<tr>
												<td><?php echo $i++;?>
															<div class="adno-dtls"> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> |
																		<a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a> |
																		<a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>"> VIEW</a>
															</div>
															<!-- Modal3 -->
																	<div  class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
																	<div class="modal-dialog">
																		<div class="modal-content"> 																																					
																		<div role="tabpanel" class="tabarea2"> 
																			
																			<!-- Nav tabs -->
																		<ul class="nav nav-tabs" role="tablist">									  
																			<li role="presentation" class="active"> <a href="#personal<?php echo $tableId; ?>" aria-controls="personal" role="tab" data-toggle="tab">News Details</a> </li>										
																		</ul>
																			
																			<!-- Tab panes -->
																			<div class="tab-content">
																			<div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
																				<table class="table nobg">
																				<tbody style="background-color:#FFFFFF">
															
																					<tr>
																						<td>Category</td>
																						<td> : </td>
																						<td><?php  echo $row['association_type']; ?></td>
																					</tr>
																					<tr>
																						<td>Name</td>
																						<td> : </td>
																						<td><?php  echo $row['name']; ?></td>
																					</tr>
																					<tr>
																						<td>Contact Number</td>
																						<td> : </td>
																						<td><?php  echo $row['contact_number']; ?></td>
																					</tr>
																					<tr>
																						<td>CONCERN PERSON</td>
																						<td> : </td>
																						<td><?php  echo $row['concern_person_name'] ?></td>
																					</tr>
																					
                                           <tr>
																							<td>Locations</td>
                                              <td> : </td>
                                              <td> <?php  echo $row['location'] ?></td>
                                          </tr>
                                         	
																					<tr>
																						<td>Description</td>
																						<td> : </td>
																						<td><?php echo $row['description'] ?></td>
																					</tr>

																					<tr>
																						<td>Photo</td>
																						<td> : </td>
																						<td><?php if($row['photo_url']){
																									$path="../../".$row['photo_url']; ?>
																									<div style="max-width: 100px;"><img src="<?php echo $path ?>" alt="No Image" class="img-responsive" /></div>
																									<?php }else{?>
																									<div style="max-width: 100px;"><img src="../../img/student.png" alt="No Image" class="img-responsive" /></div>
																									<?php } ?>
																						</td>
																					</tr>	
																														
																				</tbody>
																				</table>
																			</div>
																													
																			</div>
																		</div>
																		</div>
																	</div>
																	</div>
																	<!-- Modal3 cls --> 																						  						
													</td>
													<td><?php echo $row['name']; ?></td>
												<td><?php echo $row['concern_person_name']; ?></td>
													<td><?php echo $row['contact_number']; ?></td>
													<td><?php echo $row['location']; ?></td>
													<td><?= $row['priority']; $pr=$row['priority']; $name=$row['name']; ?></td>	
													<td style="width: 108px;" id="priority<?php echo $tableId; ?>" ><a href="#" onclick="PriorityLoad('<?php echo $tableId;?>','<?php echo $pr;?>','<?php echo $name;?>')" data-toggle="modal" data-target="#myModal6<?php echo $tableId; ?>" class="viewbtn">Priority</a>
													<!-- Modal6 priority -->
														<div class="modal fade" id="myModal6<?php echo $tableId; ?>" tabindex="-1" role="dialog">
															<div class="modal-dialog">
																<div class="modal-content">
																	<div role="tabpanel" class="tabarea2">
																		<div class="modal-header">
																			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																			<h4 class="modal-title">Association Priority</h4>
																		</div>
																		<!-- Tab panes -->
																		<div class="tab-content" id="tabPriority<?php echo $tableId; ?>">																			
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!-- Modal6 priority cls -->

												</tr>
										<?php }
									}?>                  
                </tbody>
              </table>
			  	
            </div>
						<!-- **********************************************************************  -->
					<!--Paging missing -->
										<!-- ******************************************************************-->
			<!-- paging -->		
				<div style="clear:both;"></div>
				<div class="text-center">
					<div class="btn-group pager_selector"></div>
				</div>        
				<!-- paging end-->
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add Association </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" enctype="multipart/form-data" class="form1" method="post" onsubmit="return valid()">
                <div class="row">
                  <div class="col-sm-6">

										<div class="form-group">
                      <label for="location">Location<span class="star">*</span></label>
											<select name="location" id="location" class="form-control2" required >
											<?php
											$categoryTypes="select ID,location from ".TABLE_LOCATION."";
											$res2=mysql_query($categoryTypes);	
											
											while($row=mysql_fetch_array($res2))
											{?>	
												<option value="<?php echo $row['ID']?>"><?php echo $row['location']?></option>
											<?php 									
											}?>	            			
											</select>
										</div>	
										<div class="form-group">
                      <label for="category">Category<span class="star">*</span></label>
											<select name="association_type_id" id="association_type_id" class="form-control2" required >
											<?php
											$categoryTypes="select ID,association_type from ".tbl_association_type."";
											$res2=mysql_query($categoryTypes);	
											
											while($row=mysql_fetch_array($res2))
											{?>	
												<option value="<?php echo $row['ID']?>"><?php echo $row['association_type']?></option>
											<?php 									
											}?>	            			
											</select>
										</div>

                    <div class="form-group">
                      <label for="Heading">Name<span class="star">*</span></label>
                      <input type="text" id="name" name="name" class="form-control2" value=""  required placeholder="Association/Party" title="Heading" />				
					  					<span id="user-result"></span>
                    </div>	

										<div class="form-group">
                      <label for="contact_number">Contact Number<span class="star">*</span></label>
                      <input type="text" id="contact_number" name="contact_number" placeholder="Contact Name" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>
										
										<div class="form-group">
                      <label for="concern_person_name">Concern Person<span class="star">*</span></label>
                      <input type="text" id="concern_person_name" name="concern_person_name" placeholder="Concern Person" class="form-control2" required>				
					  					<span id="user-result"></span>
                    </div>

										<div class="form-group">
                      <label for="Description">Description</label>
                      <textarea id="Description" name="description" class="form-control2" placeholder="description"></textarea>				
					  					<span id="user-result"></span>
                    </div>
<div class="form-group">
                      <label for="image_url">Photo</label>
                      <input type="file" name="photo" id="image_url" >		  				
					  					<span id="user-result"></span>
                    </div>
										<div class="form-group">
											<img id="thumb" width="255" height="265" src="../../img/student.png" alt="your image" />
										</div>	
										<div class="form-group">
                      <label for="policy">policy<span class="star">*</span></label>
                      <textarea id="policy" name="policy" class="form-control2" placeholder="policy" required></textarea>				
					  					<span id="user-result"></span>
                    </div>
									</div>
								</div>
			  				<div>
            		</div>
            		<div class="modal-footer">
              		<input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            		</div>
							</form>
          	</div>
        	</div>
      	</div>
	  	</div>
<?php include("../adminFooter.php") ?>
