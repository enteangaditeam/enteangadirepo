<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
	header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_POST['location'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");
			}
		else
			{							
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
						
				$data['location_id']		=	$App->convert($_REQUEST['location']);
				$data['name']			=	$App->convert($_REQUEST['name']);
				$data['concern_person_name']		=	$App->convert($_REQUEST['concern_person_name']);
				$data['contact_number']		=	$App->convert($_REQUEST['contact_number']);
                $data['association_type_id']		=	$App->convert($_REQUEST['association_type_id']);
				$data['policy']			=	$App->convert($_REQUEST['policy']);
				$data['description']		=	$App->convert($_REQUEST['description']);
				
				//Status is hard coded,Coz feature is not implimented now
				$data['status']			=	0;//$App->convert($_REQUEST['status']);
				$img 					=	"";				
				
				if($_FILES["photo"]["name"])
				{
					$path_info1 = pathinfo($_FILES["photo"]["name"]);
					$ext1	 				=	$path_info1["extension"];
					$allowed_extension 		=	array("jpg","jpeg","png");
					$result 				= 	date("YmdHis");
					$newName 	= $result.".".$ext1;
					if(in_array($ext1,$allowed_extension))
					{
						if(move_uploaded_file($_FILES["photo"]["tmp_name"],"../../Images/Association/".basename($newName)))
						{
							$img="Images/Association/" . $newName;
							$data['photo_url']			=	$App->convert($img);		
						}
					}
					else
					{
						$_SESSION['msg']="Invalid image";
						header("location:index.php");
						return;	
					}						
				
				}
				else
				{
					$data['photo_url']			=	$App->convert($img);
				}
				
				
				$success=$db->query_insert(TABLE_ASSOCIATION, $data);
				$db->close();				
				
				if($success)
				{
					$_SESSION['msg']="Association Added Successfully";										
				}
				else
				{
					$_SESSION['msg']="Failed";
					header("location:index.php");
					return;							
				}	
				header("location:index.php");	
			}
		break;
		
	// EDIT SECTION
	//-
	case 'edit':
		
		$fid	=	$_REQUEST['fid'];	       
		if(!$_POST['location'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=".$fid);	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;

				$imgRes = mysql_query("SELECT photo_url FROM `".TABLE_ASSOCIATION."` WHERE ID = ".$fid);
				$imgRow = mysql_fetch_array($imgRes);
				$photo_url = $imgRow['photo_url'];
				$isImageUploaded =0;

				$data['location_id']		=	$App->convert($_REQUEST['location']);
				$data['name']			=	$App->convert($_REQUEST['name']);
				$data['association_type_id']		=	$App->convert($_REQUEST['association_type']);
				$data['concern_person_name']		=	$App->convert($_REQUEST['concern_person_name']);
				$data['contact_number']		=	$App->convert($_REQUEST['contact_number']);	
				$data['policy']			=	$App->convert($_REQUEST['policy']);
				$data['description']		=	$App->convert($_REQUEST['description']);
				//Status is hard coded,Coz feature is not implimented now
				$data['status']			=	0;//$App->convert($_REQUEST['status']);		

				if ($_FILES['photo']['name'] != "") 
            	{
					$file_name 		 				    = 	pathinfo($_FILES["photo"]["name"]);
					$file_ext	 					    =	$file_name["extension"];
					$rename 						    = 	date("YmdHis");
					$new_file_name 					    = 	$rename.".".$file_ext;
					$isValidImage 						=	$App->validateImages($file_ext);
					if($isValidImage)
					{
						if(move_uploaded_file($_FILES["photo"]["tmp_name"],"../../Images/Association/".basename($new_file_name)))
						{
							$isImageUploaded=1;
							$img="Images/Association/" . $new_file_name;
							$data['photo_url']		    =	$App->convert($img);
						}
						else
						{
							$_SESSION['msg']            =   "Error..!!";
                        	header("location:edit.php?id=".$fid);
                        	return;
						}
					}
					else
					{
						$_SESSION['msg']               =   "Please upload a valid image";
                     	header("location:edit.php?id=".$fid);
                     	return;
					}
				}
				$success=$db->query_update(TABLE_ASSOCIATION, $data, "ID='{$fid}'");					 
				$db->close();
				if($success)
				{
					if ($photo_url && $isImageUploaded) 
					{
                    	$image = '../../'.$photo_url;
                        if (file_exists($image))
                        {
                            unlink($image);
                        }
                    }
					$_SESSION['msg']="Details updated successfully";																	
				}
				else
				{
					$_SESSION['msg']="Failed";									
				}	
				header("location:index.php");						
			}
		
		break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$imgRes = mysql_query("SELECT photo_url FROM `".TABLE_ASSOCIATION."` WHERE ID = ".$id);
				$imgRow = mysql_fetch_array($imgRes);
				$photo_url = $imgRow['photo_url'];
				
				try
				{			
					$success= @mysql_query("DELETE FROM `".TABLE_ASSOCIATION."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}				
				$db->close(); 
				
				
				if($success)
				{
					if ($photo_url) 
					{
                    	$image = '../../'.$photo_url;
                        if (file_exists($image))
                        {
                            unlink($image);
                        }
                    }
					$_SESSION['msg']="Association details deleted successfully";									
				}
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";									
				}	
				header("location:index.php");	
		break;	

			// Priority
	case 'priority':		
		$editId	=	$_REQUEST['shopId']; 
			    	
		if(!$_REQUEST['shopId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();

				$data['priority']	=   $App->convert($_REQUEST['priority']);
				
				$success1=$db->query_update(TABLE_ASSOCIATION,$data," ID='{$editId}'");

				$db->close();
					
				if($success1)
				{							
					$_SESSION['msg']="Association Priority Updated Successfully";										
				}
				else{
					$_SESSION['msg']="Failed";
				}
				
				header("location:index.php");																
			}	
			break;	
}
?>