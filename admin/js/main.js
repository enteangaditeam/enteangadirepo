$(document).ready(function() {
	//$('.leftmenu').prepend('<div class="mobmenu"></div>');
	//$('.mobmenu').hide();
    mobileMenu();

    //$(window).resize(mobileMenu);

    $('.datepicker').datepicker();
	$('#time_date').datetimepicker({
        format: 'd-m-Y H:i',
        step: 30,
        minDate: '0',
        defaultTime: '12:00'
    });
	
    // Date picker
    $('.date_pick').datetimepicker({
        format: 'd-m-Y',
        step: 30,
        minDate: '0',
        timepicker: false,
        defaultDate: new Date()
    });
	//for time picker
    $('.timepicker5').timepicker({
        template: false,
        showInputs: false,
        minuteStep: 5
    });

    // Show file upload thumbnail before submitting the form
    function imageThumb(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            $(input).nextAll('.image_upload_preview').remove();
            reader.onload = function (e) {
                //$('#image_thumb').attr('src', e.target.result);
                $(input).after('<img class="image_upload_preview" src="' + e.target.result + '" alt=""/>');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).on('change', '.show_progress', function () {
        imageThumb($(this).get(0));
        //console.log($(this).get(0));
    });
	// Removing the thumbnail while form is closed
	$('#myModal').on('hidden.bs.modal', function () {
        $('#myModal').find('.image_upload_preview').remove();
		$('#myModal').find('input.show_progress').val('');
    });
	
	// Quiz functions
    // Adding options
    $(document).on('click', '#add_quiz_option', function (e) {
        e.preventDefault();
        var quizPop = $(this).parents('#quiz_question'),
            opTable = quizPop.find('table#opt_table');
        opTable.find('tbody').append('<tr>' +
            '<td><input type="radio" name="correct" value="1"></td>' +
            '<td><input type="text" class="form-control2" required /></td>' +
            '<td><a href="#" data-action="delete" class="btn btn-danger">Delete</a></td>' +
            '</tr>');
        opTable.find('tbody > tr:last > td input[type="radio"]').val(opTable.find('tbody > tr').length);
        opTable.find('tbody > tr:last > td input[type="text"]').attr('name', 'option_' + opTable.find('tbody > tr').length);
        opTable.find('thead > tr > th input[name="option_count"]').val(opTable.find('tbody > tr').length);
    });
    // Deleting options
    $(document).on('click', '#opt_table > tbody > tr > td a[data-action="delete"]', function (e) {
        e.preventDefault();
        var delBtn = $(this),
            opTable = delBtn.closest('table'),
            opRow = delBtn.closest('tr'),
            opRowIndex = opRow.index(),
            opCount = opTable.find('tbody > tr').length;
        if (opCount <= 2) {
            alert("Minimum two options are mandatory!");
        } else {
            opRow.remove();
            opTable.find('tbody > tr:last').find('input[type="radio"]').attr('required', 'required');
            opTable.find('tbody > tr').each(function () {
                var tr = $(this);
                tr.find('input[type="radio"]').val(tr.index() + 1);
                tr.find('input[type="text"]').attr('name', 'option_' + (tr.index() + 1));
            });
            opTable.find('thead > tr > th input[name="option_count"]').val(opTable.find('tbody > tr').length);
        }
    });
});

function mobileMenu(){
	$(document).on('click', '.mobmenu', function(){
		if ($(window).width() <= 991){
			$('.leftmenu>ul').slideToggle();
		}
	});
	/*$(window).resize(function(){
		if($(window).width() <= 991){
			$('.leftmenu>ul').hide();
		} else {
			$('.leftmenu>ul').show();			
		}
	});*/
    /*if ($(window).width() <= 991){	
		 //$('.mobmenu').show();
		 //$('.leftmenu>ul').slideUp();
		 $('.mobmenu').click(function(){
		   $('.leftmenu>ul').stop().slideToggle();
		 });
	}
	else{
		//$('.mobmenu').hide();
		$('.leftmenu>ul').slideDown();
	}*/	
}
//for image thumbnil preview
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#thumb').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
$("#image_url").change(function() {
    readURL(this);
});
// script for diary
$(document).ready(function() {
	$('.diary_navigation > li > a').click(function(e) {
		e.preventDefault();
		var curBtn = $(this);
		$('.diary_navigation > li > a').not(curBtn).removeClass('active');
		curBtn.addClass('active');
		var targetDiaryBlock = $(curBtn.attr('href'));
		$('.diary_block').not(targetDiaryBlock).removeClass('active');
		targetDiaryBlock.addClass('active');
	});
});